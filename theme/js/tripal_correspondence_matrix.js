
/* 
 * File: tripal_correspondence_matrix.js
 * Obtain settings from MapViewer PHP form, call correspondence matrix draw to display selected map correspondences.
 */


(function($) {
  Drupal.behaviors.tripal_mapCorrespondenceMatrixBehavior = {
    attach: function (context, settings) {
    	
    	$('#select_fieldset_correspondence_matrix').once('select_fieldset_correspondence_matrix', function() {

    	var comparisonType = 'geneticMap';	
   		var DrupalCorrespondenceMatrixJSObj = "";
   		
   		if ( typeof Drupal.settings.mapGenomeCorrespondenceMatrixJS == "object") {
   			DrupalCorrespondenceMatrixJSObj = Drupal.settings.mapGenomeCorrespondenceMatrixJS;
   			comparisonType = 'genomeComparison';
   		}
   		else if ( typeof Drupal.settings.mapGenomesCorrespondenceMatrixJS == "object") {
    		DrupalCorrespondenceMatrixJSObj = Drupal.settings.mapGenomesCorrespondenceMatrixJS;
   			comparisonType = 'genomesComparison';
   		}
   		else {
    		DrupalCorrespondenceMatrixJSObj = Drupal.settings.mapCorrespondenceMatrixJS;
   		}
    	var rows =				DrupalCorrespondenceMatrixJSObj.rows;                   	
    	var cols =				DrupalCorrespondenceMatrixJSObj.cols;
    	var correspondences =	DrupalCorrespondenceMatrixJSObj.correspondences;
       	
        var container =  "#select_fieldset_correspondence_matrix";
    	$(container).before('<div></div><div class = "TripalMap" id ="select_fieldset_correspondence_matrix_svg" width="100%"></div>');

    	correspondenceMatrix.drawCorrespondenceMatrix(rows, cols, correspondences, comparisonType);

    });
    }
  };
})(jQuery);

