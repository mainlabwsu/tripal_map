
////////////////////////////////////////////////////////////
// Display correspondence matrix with D3.js

var dotplot = dotplot || {};

dotplot = {

	drawDotplotPlotly: function(primMapId, primMap, primLinkageGroup, primMapUnitType, secMapId, secMap, secMapUnitType, 
			secLinkageGroup, genomePageLink, corres ) {

		var genomeDisplay = false;
		if ((typeof secMapId != 'string') || (secMapId.length <= 0)) {
			// this dot plot contains genome correspondence markers
			genomeDisplay = true;			
		}

		// first obtain the correspondence datapoints
		dpHash = {};
		for (var ckey in corres) {
			//corres: {"X17_1500":[{"linkageGroup":"LGIII","position":"184.2", "feature_id": 1233554},{"linkageGroup":"LGIIIb.3","position":"74.5943", ..}],"
			if (corres.hasOwnProperty(ckey)) {
				markerCorres = corres[ckey];
				if (Object.keys(markerCorres).length >= 2 ) {
					// only use the marker if it pertains to either the primary or secondary linkage group (correspondences are for the whole map)
					var xPos = parseFloat(markerCorres[0].position);
					var yPos = parseFloat(markerCorres[1].position);
					if (!(yPos in dpHash)) {
						dpHash[yPos] = {};
						dpHash[yPos][xPos] = {};
						dpHash[yPos][xPos][ckey] = markerCorres[0];
					}
					else {
						if (!(xPos in dpHash[yPos])) {
							dpHash[yPos][xPos] = {};
							dpHash[yPos][xPos][ckey] = markerCorres[0];
						} 
						else {
							dpHash[yPos][xPos][ckey] = markerCorres[0];
						}
					}
				}
			}
		};

		// reorder the points where duplication of position occurs
		for (var y in dpHash) {
			for (var x in dpHash[y]) {
				if (Object.keys(dpHash[y][x]).length > 1) {
					// find a new position for this data point, other markers exist with the same coords.
					var count = 1;
					for (var point in dpHash[y][x]) {
						// skip the first point, leaving one in its existing position and reposition the others.
						if (count > 1) {
							dpHash = dotplot.findNewPos(dpHash, x, y, point);
						}
						count += 1;
					}
				}
			}
		}

		// add the points to the data point structure to pass to the scatter plot
		var dp = {};
		dp['x'] = [];
		dp['y'] = [];
		dp['label'] = [];
		dp['annotation'] = [];
		for (var yPos in dpHash) {
			for (var xPos in dpHash[yPos]) {
				for (var ckey in dpHash[yPos][xPos]) {
					var dp_url = Drupal.settings.baseUrl+"/tripalmap_feature/"+dpHash[yPos][xPos][ckey].feature_url;
					var x = genomeDisplay ? yPos : xPos;		
					var y = genomeDisplay ? xPos : yPos; 
					dp.x.push(x);
					dp.y.push(y);
					dp.label.push(ckey);
					dp.annotation.push({ x: x, y: y, text: "<a href = '"+dp_url+"'>{}</a>",
						showarrow: false, xanchor:'center', yanchor:'center', opacity: 0, hovertext: ckey });
				}
			}
		}
		
		var markerSize = 10;
		var trace1 = {
			x: dp.x,
			y: dp.y,
			mode: 'markers',
			type: 'scatter',
			//text: dp.label, use annotation with hover over option instead of labels, so can specify a link.
			marker: {
				size: markerSize,
				color: 'rgb(58, 118, 175)',
				line: { color: 'rgb(128, 158, 193)', width: 1},
			},
			//hoverinfo: "text", - default for hoverinfo, is to show position and text if available. If do not want position to appear, specifiy: none.
			cliponaxis: false,
		};

		var data = [ trace1];

		var svgWidth = 600;
		var svgHeight = 600;
		var fontSize = 12;

		var maxLineLenX = Math.floor(svgWidth/fontSize);
		var xAxisMapTitle = dotplot.formatAxisName(primMap, maxLineLenX);
		var xAxisLgTitle = tripalMap.lGNameReduction(primLinkageGroup, primMap);

		var maxLineLenY = Math.floor(svgHeight/fontSize);
		var yAxisMapTitle = dotplot.formatAxisName(secMap, maxLineLenY);
		var yAxisLgTitle = tripalMap.lGNameReduction(secLinkageGroup, secMap);
		
		var xAxisUnit = " "+primMapUnitType;
		var yAxisUnit = genomeDisplay ? " bp" : " "+secMapUnitType ;
		
		var xTitle =  "<a href = '"+Drupal.settings.baseUrl+ "/mapviewer/" + primMapId + "/" 
			+ encodeURI(primLinkageGroup.replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "'>"
			+ xAxisMapTitle + "<br>" + xAxisLgTitle + "</a>";
		
		var yTitle = genomeDisplay ? "<a href = '"+Drupal.settings.baseUrl + genomePageLink + "'>" + yAxisMapTitle + "<br>" + yAxisLgTitle + "</a>" : 
			"<a href = '"+Drupal.settings.baseUrl+ "/mapviewer/" + secMapId + "/" 
			+ encodeURI(secLinkageGroup.replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "'>"
			+ yAxisMapTitle + "<br>" + yAxisLgTitle + "</a>";
		
		var layout = {
			
			width: svgWidth, 
	        height: svgHeight, 
			xaxis: {
				range: [0, (Math.max(...dp.x) + markerSize)],
				title: xTitle,
				layer: "below traces", 
				ticksuffix: xAxisUnit,
				showticksuffix: "last",

			},

			yaxis: {
				range: [0, (Math.max(...dp.y) + markerSize)],
				title: yTitle,
				layer: "below traces",
				ticksuffix: yAxisUnit,
				showticksuffix: "last",

			},
			hovermode:'closest',
			showlegend: false,
			annotations: dp.annotation, 
		};
		
		Plotly.newPlot('select_fieldset_dotplot_svg', data, layout);
	},
	
	formatAxisName: function(axisName, maxLen) {
		// return name, tokenized by the break character, where each token does not exceed maxLen
		var newStr = '';

		let aName = axisName;
		var numLines = 1;
		if (axisName.length > maxLen) {
			// the name will not fit on one line. Segment it.
			var line = '';
		    for (let character of aName) {
				line += character;
		    	if (line.length >= maxLen) {
		    		// write the line, purge the line buffer
		    		newStr += line;
					line = '';
					if (numLines >= 3) {
	    				// only three lines will fit on the axes label. append an ellipses and break
	    				newStr += '...';
						numLines += 1;
	    				break;
	    			}
		    		if (axisName.length > newStr.length) {
		    			// append the line break only if this is not the last line
		    			newStr += '<br>';
					}
					numLines += 1;
		    	}
		    }
		    newStr += line;
		}
		else {
			// the original string will fit on one line
			newStr = axisName;
		}
		
	    return newStr;
	},
	
	findNewPos: function(dpHash, x, y, point) {
		// perturb the position slightly until unique position found, then remove old position.
		var incr = 0.2;
		var mult = 1.0;
		x = parseFloat(x); // cast the position string to a float
		y = parseFloat(y); // cast the position string to a float
		var newX = x;
		var newY = y;
		while (dotplot.pointExists(newX, newY, dpHash) && (mult <= 5)) {
			newX = x + incr*mult;
			if (!dotplot.pointExists(newX, y, dpHash)) { break;}
			if (!dotplot.pointExists(newX, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(newX, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			newX = x - incr*mult;
			if (!dotplot.pointExists(newX, y, dpHash)) { break;}
			if (!dotplot.pointExists(newX, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(newX, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			newX = x;
			if (!dotplot.pointExists(x, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(x, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			mult = mult+1; // stop after 5 tries
		}
		// add new pos
		if (!(newY in dpHash)) {
			dpHash[newY] = {};
		}
		dpHash[newY][newX] = {};
		dpHash[newY][newX][point] = dpHash[y][x][point];
		// remove the old pos
		if (y in dpHash) {
			if (x in dpHash[y]) {
				if (point in dpHash[y][x]) {
					delete dpHash[y][x][point];
				}
			}
		}
		return dpHash;
	},
	
	pointExists: function(x, y, dpHash) {
		exists = false;
		if (y in dpHash) {
			if (x in dpHash[y]) {
				exists = true;
			}
		}
		return exists;
	},
	
};


