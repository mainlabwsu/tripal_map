
/* 
 * File: tripal_dotplot.js
 * Obtain settings from MapViewer PHP form, call Dot Plot draw to display selected chromosomes.
 */

(function($) {
  Drupal.behaviors.tripal_map_dotplot_Behavior = {
    attach: function (context, settings) {
    	
    	$('#select_fieldset_dotplot').once('select_fieldset_dotplot', function() {

    	var primMapId = 			Drupal.settings.mapDotPlotJS.primary_map_id;
    	var primMap =				Drupal.settings.mapDotPlotJS.primary_map;
    	var primLinkageGroup =		Drupal.settings.mapDotPlotJS.prim_linkage_group;
    	var primMapUnitType = 		Drupal.settings.mapDotPlotJS.primary_map_unit_type;
    	var primGeneticFeatures = 	Drupal.settings.mapDotPlotJS.dotplot_prim_genetic_features;

    	var secMapId = "";
    	var secMap = "";
    	var secChromosome = "";
		var secMapUnitType = "cM";
    	var secFeatures = "";
		var genomeComparison = false;
		var genomePageLink = "";
		
    	if ( typeof Drupal.settings.mapDotPlotSecGenomeJS != "undefined") {
    		genomeComparison = true;
    		secMap =		 Drupal.settings.mapDotPlotSecGenomeJS.genome_analysis_name;                   	
    		secChromosome =  Drupal.settings.mapDotPlotSecGenomeJS.genome_scaffold;
    		genomePageLink = Drupal.settings.mapDotPlotSecGenomeJS.genome_page_link;
    		secFeatures =  	 Drupal.settings.mapDotPlotSecGenomeJS.dotplot_sec_genome_features_comparison;
     	}
    	
    	if ( typeof Drupal.settings.mapDotPlotSecJS != "undefined") {
    		secMapId =       Drupal.settings.mapDotPlotSecJS.secondary_map_id;                   	
    		secMap =		 Drupal.settings.mapDotPlotSecJS.secondary_map;                   	
    		secChromosome =  Drupal.settings.mapDotPlotSecJS.secondary_linkage_group;
    		secMapUnitType = Drupal.settings.mapDotPlotSecJS.secondary_map_unit_type;
    		secFeatures =  	 Drupal.settings.mapDotPlotSecJS.dotplot_sec_genetic_features;
    	}

    	var filteredPrimGeneticFeatures = [];
    	primGeneticFeatures.forEach(function(d) {
    		if (d.linkage_group == primLinkageGroup) {
    			filteredPrimGeneticFeatures.push(d);
    		}
    	});
 
    	// find the Dot plot correspondences
    	var corres = [];
    	if (genomeComparison) {
    		var genomeComparison = true;
    		corres = tripalMap.findDotplotCorrespondences(filteredPrimGeneticFeatures, secFeatures, genomeComparison);
    	}
    	else {
        	var filteredSecGeneticFeatures = [];
    		secFeatures.forEach(function(d) {
    			if (d.linkage_group == secChromosome) {
    				filteredSecGeneticFeatures.push(d);
    			}
    		});
    		
        	var featuresCombined = filteredPrimGeneticFeatures.concat(filteredSecGeneticFeatures);
            corres = tripalMap.findDotplotCorrespondences(featuresCombined);
    	}
    	
    	var container =  "#select_fieldset_dotplot";
    	$(container).before('<div></div><div id ="select_fieldset_dotplot_svg" width="100%"></div>');
    	$(container).remove(); // positional indicator container no longer needed

    	dotplot.drawDotplotPlotly(primMapId, primMap, primLinkageGroup, primMapUnitType, secMapId, secMap, secMapUnitType, secChromosome, genomePageLink, corres);
    });
    }
  };
})(jQuery);

