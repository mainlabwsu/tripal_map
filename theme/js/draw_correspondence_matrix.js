////////////////////////////////////////////////////////////
// Display correspondence matrix with D3.js

var correspondenceMatrix = correspondenceMatrix || {};

correspondenceMatrix = {  
		
	drawCorrespondenceMatrix: function(rows, cols, correspondences, comparisonType) {
		
		// return if there is no map
		if ((rows === null ) || (cols === null )) {
			return;
		}
		
		if (!correspondences) {
			throw new Error('Please pass correspondences data');
		}
		
		if (!Array.isArray(correspondences) || !correspondences.length || !Array.isArray(correspondences[0])) {
			throw new Error('Correspondences should be a 2-D array');
		}
		
		var manyToOne = false;
		if (rows['type'] == "ref_organism") {
			manyToOne = true;
		}
		
		//$features = {rows: [type: refmap,  name: $ref_map_name, lgs: [lg1, lg2, ...]},
		//             cols: {type: compmap,  name: $comp_map_name, lgs: [lg1, lg2, ...]},
		//             corresp: [[1,2,...],[2,3,...],...]}
		var svgMaxWidth = 1100;
		var svgMaxHeight = 600;
		
		var numrows = correspondences.length;
		var numcols = correspondences[0].length;
		var cellsize = 45;
		var rectWidth = numcols * cellsize;
		var rectHeight = numrows * cellsize;
		var colMapLabelWidth = 400;
		var rowMapLabelHeight = 400;
		var margin = {"x": 0, "y": 0};
		
		var x = "";
		var y = "";
		if (tripalMap.d3VersionFour()) {
			x = d3.scaleBand()
				.domain(d3.range(numcols))
				.range([0, rectWidth]);
			
			y = d3.scaleBand()
				.domain(d3.range(numrows))
				.range([0, rectHeight]);
		}
		else {
			x = d3.scale.ordinal()
				.domain(d3.range(numcols))
				.rangeBands([0, rectWidth]);
			
			y = d3.scale.ordinal()
				.domain(d3.range(numrows))
				.rangeBands([0, rectHeight]);
		}
		
		// If the exact mapName string occurs in the linkage group name, filter it out to reduce duplication
		//rows: {"type":"ref_organism"
		var labelsColLink = cols['lgs'];
		cols['lgs'] = tripalMap.customLgSort(cols['lgs'].map(x => tripalMap.lGNameReduction(String(x), cols['name'])));
		
		var labelsRowLink = "";
		var labelsRow = "";
		var mapIdRow = "";
		if (manyToOne) {
			labelsRowLink = rows['maps']['feature_ids'];
			labelsRow = rows['maps']['names'];
			mapIdRow = 'All';
    	}
    	else {
    		labelsRowLink = rows['lgs'];
    		rows['lgs'] = tripalMap.customLgSort(rows['lgs'].map(x => tripalMap.lGNameReduction(String(x), rows['name'])));
    		labelsRow = rows['lgs'];
    		mapIdRow = rows['feature_id'];
    	}

	    var options = { 
	    	"data": correspondences, "labelsCol": cols['lgs'], "labelsColLink": labelsColLink, "mapNameCol": cols['name'], 
	    	"mapIdCol": cols['feature_id'], "labelsRow": labelsRow, "labelsRowLink": labelsRowLink, "mapNameRow": rows['name'],
	    	"mapIdRow": mapIdRow, "start_color" : /*'#ffffff'*/'#DEECF7'/*#B9DCF3'*/, "end_color" : '#1C7EDC',/*'#3498db'*/ 
	    	"rectWidth": rectWidth,	"rectHeight": rectHeight, "xScale": x, "yScale": y, 
	    	"comparisonType": comparisonType, "cols": cols};

	    var svgFieldset = "#select_fieldset_correspondence_matrix_svg";
	    d3.select(svgFieldset).selectAll("svg").remove(); 
	    var svg = d3.select(svgFieldset)
	    	.append("svg").attr("class", "TripalMap").attr("width", svgMaxWidth).attr("height", svgMaxHeight + 50);

	    var labelDim = correspondenceMatrix.calculateLabelDimensions(svg, options, x, y, rectWidth, rectHeight, colMapLabelWidth, rowMapLabelHeight, svgMaxHeight);
	    var matrixWidth = margin.x + rectWidth + labelDim.x;
	    var matrixHeight = margin.y + rectHeight + labelDim.y;
	    
		if ((matrixWidth > svgMaxWidth) || (matrixHeight > svgMaxHeight)) {
			var cmScroll = "cm_scroll";
		    d3.select(svgFieldset).selectAll("svg").remove(); 
		    d3.select(svgFieldset).append("div").attr("class", "TripalMap").attr("id", cmScroll);
		    svg = d3.select("#"+cmScroll)
				.append("svg").attr("class", "TripalMap").attr("width", matrixWidth + 50).attr("height", matrixHeight + 50);
		}

		var pngFileName = 'Correspondence Matrix_' + options["mapNameCol"] + ' vs ' + options["mapNameRow"] + '.png'; 
		var stp = svg.append("svg:image");
		stp.attr("xlink:href", Drupal.settings.baseUrl+"/"+Drupal.settings.tripal_map.modulePath+"/theme/images/save_as_png.png")
		.attr('width', 35)
		.attr('height', 25)
		.attr("transform", "translate(" + 0 + "," + margin.y +")")
		.attr("id", "save_to_png")
		.on("click", function(d) { tripalMap.onSaveToPngClick(svg, pngFileName);})
		.on("mouseover", function(d) { tripalMap.onSaveToPngMouseOver(svg);})
		.on("mouseout", function(d) { tripalMap.onSaveToPngMouseOut(svg);});
		
		// correspondence matrix frame 
    	var stpn = stp.node().getBoundingClientRect();
    	var transY = stpn.height;

	    var cmFrame = svg.append("g");
        	cmFrame.attr("id", "cmFrame")
        	.attr("transform", "translate("+ margin.x + "," + (margin.y + transY) + ")") // position below the download-to-png icon
        	.attr("visibility", "unhidden");

	    var labels = cmFrame.append('g')
			.attr('class', "labels");

	    var colMapLabelHeight = correspondenceMatrix.drawColLabels(labels, options.mapNameCol, options.mapIdCol, options.labelsCol, x, y, rectWidth, colMapLabelWidth, options);
	    var rowMapLabelWidth = correspondenceMatrix.drawRowLabels(labels, options.mapNameRow, options.mapIdRow, options.labelsRow, options.labelsRowLink, y, rectHeight, rowMapLabelHeight, svgMaxHeight, options.comparisonType);
	    var translateX = rowMapLabelWidth; 
	    var translateY = colMapLabelHeight;
		labels.select(".column-labels").attr("transform", "translate("+ translateX + "," + 0 + ")");
		labels.select(".row-labels").attr("transform", "translate("+ 0 + "," + translateY + ")");

	    correspondenceMatrix.drawMatrix(options, cmFrame, translateX, translateY);
	},        
    
	calculateLabelDimensions: function(svg, options, x, y, rectWidth, rectHeight, colMapLabelWidth, rowMapLabelHeight, svgMaxHeight) {

		var labels = svg.append("g")
			.attr("class", "labels");
	    var colMapLabelHeight = correspondenceMatrix.drawColLabels(labels, options.mapNameCol, options.mapIdCol, options.labelsCol, x, y, rectWidth, colMapLabelWidth, options);
	    var rowMapLabelWidth = correspondenceMatrix.drawRowLabels(labels, options.mapNameRow, options.mapIdRow, options.labelsRow, options.labelsRowLink, y, rectHeight, rowMapLabelHeight, svgMaxHeight, options.comparisonType);
	    svg.selectAll(".labels").remove();
	    
		var labelDim = {"x": rowMapLabelWidth, "y": colMapLabelHeight};
		
		return labelDim;
	},
	
	drawColLabels: function(svg, mapNameCol, mapIdCol, labelsDataCol, x, y, width, mapLabelWidth, options) {
		
		var columnLabels = svg.append("g")
			.attr("class","column-labels");
		
		var colUrl = Drupal.settings.baseUrl + "/mapviewer/" + mapIdCol;
		if ((options.comparisonType == 'genomeComparison') || (options.comparisonType == 'genomesComparison')) {
			colUrl = Drupal.settings.baseUrl + options.cols['analysis_link'];
		}
		var columnMapLabel = columnLabels.append("g");
		var columnMapLabela = columnMapLabel.attr("class","column-map-label").append("a");	
			columnMapLabela.attr("xlink:href", colUrl );
		
		var colMapLabelText = columnMapLabela.append("text");
		colMapLabelText.attr("x", 0)
			.style("font-size", "1.2em").style("line-height", "1.1em").style("text-anchor", "start")
			.text(mapNameCol)
			.call(tripalMap.wrap, mapLabelWidth); // wrap the map name if too long  
		
		var colMapTextWidth = mapLabelWidth;
		var colMapTextHeight = colMapLabelText.node().getBBox().height;
		
		// Columns
		var columnLabels = columnLabels.append("g")
    		.attr("class", "column-cell-labels")
    		.selectAll(".column-cell-label")
    		.data(labelsDataCol)
    		.enter().append("g")
    			.attr("class", "column-cell-label")
    			.attr("transform", function(d, i) { return "translate(" + x(i) + "," + 0 + ")"; });
    	
    	xrange = "";
    	yrange = "";
    	if (tripalMap.d3VersionFour()) {
    		xrange = x.bandwidth();
    		yrange = y.bandwidth();
    	}
    	else {
    		xrange = x.rangeBand();
    		yrange = y.rangeBand();
    	}
    	columnLabels.append("line")
    		.style("stroke", "black")
    		.style("stroke-width", "1px")
    		.attr("x1", xrange / 2).attr( "x2", xrange / 2).attr("y1", 17).attr("y2", 19);
    		
    	columnLabels.append("text")
    		.attr("x", 0)
    		.attr("y", yrange / 2)
    		.attr("dy", ".82em")
    		.attr("text-anchor", "start")
    		.attr("transform", "rotate(290)")
    		.text(function(d, i) { return d; });

		var mapLabelBuf = 20; // padding between axes line and label text, plus 290-270 degree angle shift
		var colLabelsHeight = svg.select(".column-cell-labels").node().getBBox().height;
		var yPosGridOffset = colLabelsHeight + colMapTextHeight;
		var yCellLabelOffset = yPosGridOffset - mapLabelBuf;

		svg.selectAll(".column-cell-labels").attr("transform","translate( 0, "+ yCellLabelOffset +")");

		return yPosGridOffset;
	},

	drawRowLabels: function(svg, mapNameRow, mapIdRow, labelsDataRow, labelsRowLink, y, height,  mapLabelMaxLength, svgMaxHeight, comparisonType) {
		
		var rowLabels = svg.append("g")
		.attr("class","row-labels");

		var rowMapLabel = rowLabels.append("g");
			rowMapLabel.attr("class", "row-map-label")
			.attr("transform","rotate(270)");
		
		var rowMapLabela = rowMapLabel.append("a");	
			rowMapLabela.attr("xlink:href", Drupal.settings.baseUrl + "/mapviewer/" + mapIdRow);
			
		var rowMapLabelText = rowMapLabela.append("text");
		rowMapLabelText.style("font-size", "1.2em").style("line-height", "1.1em").style("text-anchor", "end")
			.text(mapNameRow)
			.call(tripalMap.wrap, mapLabelMaxLength); // wrap the map name if too long
		
		var mapLabelBuf = 20;
		var rowMapTextWidth = rowMapLabelText.node().getBBox().width;
		var rowMapTextHeight = rowMapLabelText.node().getBBox().height;

		// Rows
    	var rowLabels = rowLabels.append("g")
    		.attr("class", "row-cell-labels")
    		.selectAll("row-cell-label")
    		.data(labelsDataRow)
    		.enter().append("g")
    			.attr("class", "row-cell-label")
    			.attr("id", function(d,row) {
    				var mapRow = mapIdRow;
    	    		if (mapIdRow == 'All') {
    	    			mapRow = labelsRowLink[row];
    	    		}
    	    		return mapRow;
    			})
    			.attr("transform", function(d, i) { return "translate(" + 0 + "," + y(i) + ")"; });

    	var a = rowLabels;
    	if (comparisonType == 'genomesComparison') {
    		var a = rowLabels.append("a");
    		var mapviewerPath = "/mapviewer/";

    		a.on("click", function(d, row) { 
    			var path = "";
    			var mapRow = mapIdRow;
    			if (mapIdRow == 'All') {
    				mapRow = labelsRowLink[row];
    				path = Drupal.settings.baseUrl + mapviewerPath + mapRow;
    			}
    			window.open(path, '_blank')
    		});
    	}
		
    	yrange = "";
    	if (tripalMap.d3VersionFour()) {
    		yrange = y.bandwidth();
    	}
    	else {
    		yrange = y.rangeBand();
    	}

    	a.append("line")
    		.style("stroke", "black")
    		.style("stroke-width", "1px")
    		.attr("x1", 0)
    		.attr( "x2", -2)
    		.attr( "y1", yrange / 2)
    		.attr( "y2", yrange / 2);

    	a.append("text")
    		.attr("x", -8)
    		.attr( "y", yrange / 2)
    		.attr( "dy", ".32em")
    		.attr( "text-anchor", "end")
    		.text(function(d, i) { return d; });
    	        	
    	var rowLabelsWidth = svg.select(".row-cell-labels").node().getBBox().width;
    	var rowLabelsHeight = svg.select(".row-cell-labels").node().getBBox().height;

    	var rowMapLabelVerticalPos = -1 * rowMapTextWidth;
    	if (rowLabelsHeight > svgMaxHeight) {
    		// the row labels are vertically taller than the map/organism label and exceed the div display view 
    		// so align the map/organism label to the center of the height of the div display view (svgMaxHeight)
    		rowMapLabelVerticalPos = -1 * svgMaxHeight/2 + rowMapTextWidth/2;
		}
    	else {
    		if (rowLabelsHeight < rowMapTextWidth) {
    			// the map text height is greater than the row labels height, so anchor the map/organism label at the top starting point 
        		rowMapLabelVerticalPos = 0;
        	}
        	else {
        		// the row labels are vertically taller than the map/organism label, but still within the div display view 
        		// so align the map/organism label to the center of the rowLabelsHeight
        		rowMapLabelVerticalPos = -1 * rowLabelsHeight/2 + rowMapTextWidth/2;
    		}
    	}   	
    	rowMapLabelText.attr("transform", "translate( "+ rowMapLabelVerticalPos +", " + (rowMapTextHeight ) + ")");
    	svg.selectAll(".row-cell-labels")
    		.attr("transform", "translate( " + ((rowMapTextHeight) + (rowLabelsWidth + rowMapTextHeight)) + ", " + 0 + ")");
    	
    	var xPosGridOffset = ((rowMapTextHeight) + (rowLabelsWidth + rowMapTextHeight)); // map is translated sideways, so offset is the horizontal (height) with potentially multiple wrapped lines
    	return  xPosGridOffset;
	},

	drawMatrix: function(options, svg, translateX, translateY) {

		var data = options.data;
		var mapIdRow = options.mapIdRow;
		var mapIdCol = options.mapIdCol;
		var labelsCol = options.labelsCol;
		var labelsRow = options.labelsRow;
		var labelsColLink = options.labelsColLink;
		var labelsRowLink = options.labelsRowLink;
    	var startColor = options.start_color;
    	var endColor = options.end_color;
    	var width = options.rectWidth;
    	var height = options.rectHeight;
    	var x = options.xScale;
    	var y = options.yScale;
    	var comparisonType = options.comparisonType;
    	var numrows = data.length;
    	var numcols = data[0].length;

    	var minValAr = [];
    	var maxValAr = [];
    	data.forEach( function(layer) {
    		layerNum = layer.map(function(item) {return parseInt(item); });
    		// remove zeros from array for minVal calculation
    		layerNum = layerNum.filter(x => x !== 0);
    		if (Math.min(...layerNum) != 0) {
    			minValAr.push(Math.min(...layerNum));
    		}
    		if (Math.max(...layerNum) != 0) {
    			maxValAr.push(Math.max(...layerNum));
    		}
    	});
    	var minValue = minValAr.length < 1 ? 0 : Math.min(...minValAr); 
    	var maxValue = maxValAr.length < 1 ? 0 : Math.max(...maxValAr); 
    	minValue = (minValue != 0) ? minValue : ((maxValAr.length <= 2) ? 0 : (Math.min(...maxValAr)));
    	var colorMap = d3.scaleLinear()
    		.domain([minValue, maxValue])
    		.range([startColor, endColor]);

    	var matrix = svg.append("g")
    		.attr("class","matrix")
    		.attr("transform", "translate("+ translateX + "," + translateY + ")");
    	
    	matrix.append("rect")
			.style("stroke", "black")
			.style("stroke-width", "2px")
			.attr("width", width)
			.attr("height", height);

    	var rows = matrix.append("g")
    		.attr("class", "rows")
    		.selectAll("row")
    		.data(data)
    		.enter().append("g")
    			.attr("class", "row")
    			.attr("transform", function(d, i) { return "translate(0," + y(i) + ")"; });

    	var cells = rows.selectAll(".cell")
    		.data(function(d) { return d; })
    		.enter().append("g")
    		.attr("class", "cell")
    		.attr("transform", function(d, i) { return "translate(" + x(i) + ", 0)"; });

    	// use encodeURI rather than encodeURIComponent as the latter replaces forward slashes with %2F which confuses Drupal menu_hook
    	var a = cells.append("a");
    	var mapviewerPath = "/mapviewer_comparison/";
    	if (comparisonType == 'genomeComparison') {
    		mapviewerPath = "/mapviewer_genome_comparison/";
    	}
    	if (comparisonType == 'genomesComparison') {
    		mapviewerPath = "/correspondence_matrix_genome/";
    	}
    	if (tripalMap.d3VersionFour()) {
    		var row = -1;
        	a.attr("xlink:href", function(d, col) { 
        		if (col == 0) {
        			row += 1;
        		}
        		
        		var alink = Drupal.settings.baseUrl + mapviewerPath + mapIdRow +"/" 
        			+ encodeURI(labelsRowLink[row].toString().replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "/" + mapIdCol + "/"
        			+ encodeURI(labelsColLink[col].toString().replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_"));
        		
        		return alink;
        	});
    	}
    	else {
    		a.on("click",function(d,col,row){  
    			var path = "";
    			var mapRow = mapIdRow;
    			var lgRow = labelsRowLink[row];
    			if (mapIdRow == 'All') {
    				mapRow = labelsRowLink[row];
    				lgRow = 'All';
    				if (comparisonType == 'genomesComparison') {
    					path = Drupal.settings.baseUrl + mapviewerPath + mapRow +"/" + mapIdCol; 
    				}
    			}
    			if (comparisonType != 'genomesComparison') {
    			  path = Drupal.settings.baseUrl + mapviewerPath + mapRow +"/" 
    				+ encodeURI(lgRow.toString().replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "/" + mapIdCol + "/" 
    				+ encodeURI(labelsColLink[col].toString().replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_"));
    		    }
    			window.open(path, '_blank')
    		});
    	}
    	xrange = "";
    	yrange = "";
    	if (tripalMap.d3VersionFour()) {
    		xrange = x.bandwidth();
    		yrange = y.bandwidth();
    	}
    	else {
    		xrange = x.rangeBand();
    		yrange = y.rangeBand();
    	}
    
    	var g = a.append("g");
    	g.append('rect')
    		.attr("width", xrange)
    		.attr("height", yrange)
    		.style("stroke-width", 0);
    	
    	g.append("text")
    		.attr("dy", ".32em")
    		.attr("x", xrange / 2)
    		.attr( "y", yrange / 2)
    		.attr( "text-anchor", "middle")
    		.style("fill", function(d, i) {
    			var range = maxValue - minValue;
    			var ret = d > (range/3 + minValue) ? 'white' : 'black'; 
    			ret = (minValue == maxValue) ? 'black' : ret;
    			return ret; })
    		.text(function(d, i) { return d; });
			
    	rows.selectAll(".cell")
    		.data(function(d, i) {
    			return data[i]; })
   			.style("fill", function(cell, i) {
   				var cellShade = colorMap(cell);
   				if (cell == "0") {
   					cellShade = '#ffffff';
   				}
   				return cellShade;
   			});
	}	
};
