
// Classes:
// GenomeFrame instantiated for Reference and Comparison chromosomes
//   contains MapLabel, and instances of GenomeView and ZoomedView classes
//
//
var mapViewerGenome = mapViewerGenome || {};

mapViewerGenome = {  

	GenomeObj: class {
		
		constructor() {
			this.translateX = 0;
			this.translateY = 0;
			this.height = 0;
			this.width = 0;
			this.svg = 0;
			this.containerName = "";
		}

		drawContainer(svgParent) {
			var container = svgParent.append("g")
				.attr("id", this.containerName).attr( "transform", "translate(" + this.translateX + "," + this.translateY + ")")
				.attr("visibility", "unhidden");
			this.svg = container;	
			return this.svg;
		}

		setTranslate(x,y) {
			this.translateX = x;
			this.translateY = y;
		}
	},
	
};	

mapViewerGenome.GenomeFrame = class extends mapViewerGenome.GenomeObj { 

	constructor(geneData, invertGenomeScaffold, show) {
		super();
		this.containerName = 'genome_frame';
		this.scaffoldRect = {height: 400, width: 25, zoomedDist: 120};
		this.geneData = geneData;
		this.genomeView = new mapViewerGenome.GenomeView( this.geneData, this.scaffoldRect, invertGenomeScaffold, show);
	}

	calculateDimensions(svgParent) {
		svgParent.selectAll("#"+this.containerName).remove(); 
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;
		this.genomeView.draw(this.svg);

		var width = this.svg.node().getBBox().width;
		var height = this.svg.node().getBBox().height;
		var dimensions = {"width": width, "height": height};

		this.genomeView.width = 0;
		svgParent.selectAll("#"+this.containerName).remove(); 
		return dimensions; 
	}

	draw(svgParent) {
		// genome frame 
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;
		this.genomeView.draw(this.svg);
		
		this.height = this.svg.node().getBBox().height + 20 ; // padding for legend
		this.width = this.svg.node().getBBox().width;

		return svgParent;
	}

};
	
	
mapViewerGenome.GenomeView = class extends mapViewerGenome.GenomeObj {
		
	constructor( geneData, scaffoldRect, invertGenomeScaffold, show) {
		super();
		this.containerName = "genome_view";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.show = show;
		this.scaffoldView = new mapViewerGenome.ScaffoldView(geneData, scaffoldRect, invertGenomeScaffold, show);
		this.scaffoldZoomedView = new mapViewerGenome.ScaffoldZoomedView();
		this.genomeCorrespondences = new mapViewerGenome.GenomeCorrespondences();
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		this.genomeCorrespondences.draw(this.svg);
		this.scaffoldZoomedView.draw(this.svg);
		this.scaffoldView.draw(this.svg);
		this.height = this.svg.node().getBBox().height;
		this.width += this.svg.node().getBBox().width;

		return svgParent;
	}
		
};

mapViewerGenome.ScaffoldView =  class extends mapViewerGenome.GenomeObj {

	constructor(geneData, scaffoldRect, invertGenomeScaffold, show) {
		super();
		this.containerName = "scaffold_view";
		this.genomeName = geneData.getGenomeName();
		this.scaffoldName = geneData.getScaffoldName();
		this.jbrowseLink = geneData.getJBrowseLink();
		this.genomePageLink = geneData.getGenomePageLink();
		this.genomeLink = this.jbrowseLink; // default the genome to link to JBrowse 
		if (this.genomePageLink) {
			// if genome page link available, use it for genome hyperlink
			this.genomeLink = this.genomePageLink; 
		}
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldWidth = this.scaffoldRect.width;
		this.showRuler = show.Ruler;
		this.genomeLabel = new mapViewerGenome.GenomeLabel(this.genomeName, this.scaffoldName, this.genomeLink);
		this.polygon = new mapViewerGenome.Polygon(geneData, scaffoldRect);
		this.markerView = new mapViewerGenome.MarkerView(geneData, scaffoldRect, invertGenomeScaffold);
		this.geneView = new mapViewerGenome.GeneView(geneData, scaffoldRect, invertGenomeScaffold);
		this.ruler = new mapViewerGenome.Ruler(geneData, scaffoldRect, invertGenomeScaffold);
			
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
	
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;
		this.genomeLabel.setTranslate(tmpTranslateX, tmpTranslateY);
		this.genomeLabel.draw(this.svg);
		tmpTranslateY += this.genomeLabel.height;

		tmpTranslateX += 5;
		this.markerView.setTranslate(tmpTranslateX, tmpTranslateY);
		this.markerView.draw(this.svg);
	
		tmpTranslateX += this.scaffoldWidth;
		tmpTranslateX += this.markerView.width;
			
		
		// in the case where genes exist in addition to genetic markers in the scaffold, draw the genes
		if (this.geneData.getGenes().getGroups().length > 0 ) { 
	
			// calculate the polygon width
			tmpTranslateX += 5; // buffer after the marker View
			this.geneView.setTranslate(tmpTranslateX, tmpTranslateY);
			this.geneView.draw(this.svg);
	
			tmpTranslateX += this.geneView.width;

		}

		if (this.showRuler) {
			var padding = 10;
			tmpTranslateX += padding;
			this.ruler.setTranslate(tmpTranslateX, tmpTranslateY);
			this.ruler.draw(this.svg);
		}
			
		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;
			
		return svgParent;
	}
		
};
	
mapViewerGenome.ScaffoldZoomedView = class extends mapViewerGenome.GenomeObj {

	constructor() {
		super();
		this.containerName = "scaffold_zoomed_view";
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		this.height = this.svg.node().getBBox().height;
		this.width += this.svg.node().getBBox().width;
		return svgParent;
	}
	
};

mapViewerGenome.GenomeCorrespondences =  class extends mapViewerGenome.GenomeObj {

	constructor() {
		super();
		this.containerName = "genome_correspondences";
	}
		
	draw(svgParent) {
		var svg = d3.select("#select_fieldset_mapViewer_svg").selectAll("svg");
		svgParent = svg;
	
		this.drawContainer(svgParent);
		this.height = this.svg.node().getBBox().height;
		this.width += this.svg.node().getBBox().width;
		return svgParent;
	}	
};

mapViewerGenome.GeneView = class extends mapViewerGenome.GenomeObj {
	
	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "gene_view";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffold = new mapViewerGenome.GeneScaffold(geneData, scaffoldRect, invertGenomeScaffold);

	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;			
	
		this.scaffold.setTranslate(tmpTranslateX, tmpTranslateY);
		this.scaffold.draw(this.svg);

		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;
	}
};

mapViewerGenome.MarkerView = class extends mapViewerGenome.GenomeObj {
		
	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "marker_view";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldWidth = this.scaffoldRect.width;
		this.scaffold = new mapViewerGenome.MarkerScaffold(geneData, scaffoldRect, invertGenomeScaffold);

	}
		
	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;			
	
		this.scaffold.draw(this.svg); 
		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;

	}
	remove(svgParent) {
		svgParent.selectAll("#"+this.containerName).remove();
		this.height  = 0;
		this.width   = 0;

	}
};


mapViewerGenome.GeneScaffold = class extends mapViewerGenome.GenomeObj {

	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "gene_scaffold";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldWidth = this.scaffoldRect.width;
		this.scaffoldRectangle = new mapViewerGenome.ScaffoldRectangle(scaffoldRect);
		this.genesOnRect = new mapViewerGenome.GenesOnRect(geneData, scaffoldRect, invertGenomeScaffold);
		this.brushRect = "";
	
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;

		var geneGroups = this.geneData.getGenes().getGroups();
		var maxSegmentWidth = geneGroups.map(function(d) {return d.num_genes;}).reduce((a,b) => a < b ? b : a);
		tmpTranslateX = maxSegmentWidth > this.scaffoldWidth ? (maxSegmentWidth - this.scaffoldWidth) : 0;

		this.scaffoldRectangle.setTranslate(tmpTranslateX, tmpTranslateY);
		this.scaffoldRectangle.draw(this.svg);
		this.genesOnRect.setTranslate(tmpTranslateX, tmpTranslateY);
		this.genesOnRect.draw(this.svg);

		this.height  = this.svg.node().getBBox().height;
		this.width  += this.genesOnRect.width; //this.svg.node().getBBox().width;

		return svgParent;

	}
};

mapViewerGenome.MarkerScaffold = class extends mapViewerGenome.GenomeObj {

	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "marker_scaffold";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldRectangle = new mapViewerGenome.ScaffoldRectangle(scaffoldRect);
		this.genomeMarkersOnRect = new mapViewerGenome.GenomeMarkersOnRect(geneData, scaffoldRect, invertGenomeScaffold);
		this.brushRect = "";
		
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;
		this.scaffoldRectangle.draw(this.svg);
		this.genomeMarkersOnRect.draw(this.svg);
		
		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;

	}

};
	

mapViewerGenome.GenesOnRect = class extends mapViewerGenome.GenomeObj {

	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "genes_on_rect";
		this.geneData = geneData;
		this.geneColor = geneData.getGenes().getColor();
		this.jBrowseLink = this.geneData.getJBrowseLink();
		this.scaffoldName = this.geneData.getScaffoldName(); //this.geneData.getScaffoldJBrowseName();
		this.scaffoldRect = scaffoldRect;
		this.scaffoldHeight = scaffoldRect.height;
		this.scaffoldWidth = scaffoldRect.width;
		this.geneGroups = geneData.getGenes().getGroups();
		this.invertGenomeScaffold = invertGenomeScaffold;
	}

	draw(svgParent) {
		
		//"buckets":{"1":{"num_genes":49,"gene_label":"Gorai.002G000100-JGI_221_v2.1"}, ..}}

		// track the largest segment for the negative offset width and initial translate position
		var maxSegmentWidth = this.geneGroups.map(function(d) {return d.num_genes;}).reduce((a,b) => a < b ? b : a);
		// if the max segment width exceeds the width of the scaffold, extend / translate the starting position accordingly, as the segments are drawn in reverse 
		//-> this is taken care of by the parent container when the width of the object is set in the translation
		//var tmpTranslateX = maxSegmentWidth > this.scaffoldWidth ? (maxSegmentWidth - this.scaffoldWidth) : 0;
		this.drawContainer(svgParent);
		var tmpTranslateY = 0;
		
		var geneClassName = this.containerName + "_gene";
		var geneNameClassName = this.containerName + "_gene_name";
		var linesToGeneNameClassName = this.containerName + "_lines_to_gene_name";

		var _this = this;
		var numSegments = 150;
		var segmentHeight = this.scaffoldHeight/numSegments;
		var maxGenomePos = this.geneData.getMaxGenomePos();
		var geneSegmentSize = maxGenomePos/numSegments;

    	var inv1 = 1;
    	var inv2 = 0;
    	if (this.invertGenomeScaffold) {
    	  inv1 = 0;
    	  inv2 = 1;
    	}

    	// draw all gene segments on rect. 
    	var geneGroups = this.svg.append("g");
    	geneGroups.attr("class", geneClassName)
    		.attr("transform", "translate(0,0)")
    		.selectAll("rect") 
    		.data(this.geneGroups)
    		.enter().append("rect")
    		.attr("class", geneClassName)
    		.attr("width", function(d) { return d.num_genes; })
        	.attr("id", this.containerName)
        	.attr("x", function(d) { 
        		return (-1 * d.num_genes) + _this.scaffoldWidth; })
    		.attr("y", function(d, i) { 
    			var pos1 = segmentHeight*i;
    			return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1 - segmentHeight)));} )
			.attr("height", segmentHeight)
    		.style("fill", this.geneColor /*"#0018F5"*/)
    		.attr("stroke", "black")
    		.style("stroke-width", .5);

    	
    	// draw lines to gene names beside view.
    	var labelLineLength = 25;
    	var labelStartPosX = this.scaffoldWidth;
    	var labelEndPosX = labelStartPosX + labelLineLength;
    	var filteredGeneGroups = [];
    	var count = 0;
    	var offset = 10;
    	for (var key in this.geneGroups) {
    		count = count + 1;
    		if (count == offset) {
    			// only select every 10th gene group to provide the label
    			filteredGeneGroups.push( this.geneGroups[key]);
    			count = 0;
    		}
    	}

    	var linesToGeneGroups = this.svg.append("g");
	    linesToGeneGroups.attr("class", linesToGeneNameClassName)
    		.attr("transform", "translate(0,0)")
    		.selectAll("line")
    		.data(filteredGeneGroups)
    		.enter().append("line")
    		.attr("class", linesToGeneNameClassName)
    		.attr("x1", labelStartPosX ).attr("y1", function(d, i) {
    			var pos1 = segmentHeight*i*offset;
    			return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1)));})
    		.attr("x2", labelEndPosX ).attr("y2", function(d, i) {
    			var pos2 = segmentHeight*i*offset;
    			return (_this.translateY + (inv1 * (pos2 - 5)) + (inv2 * (_this.scaffoldHeight - (pos2 + 5))));})
    		.style("stroke", "black")
        	.style("stroke-width", 1);
	    
	    var count = 0;
	    // position the reference gene names beside the view
		var geneNames = this.svg.append("g");
    	geneNames.append("rect")
    		.attr("class", geneNameClassName)
    		
    	geneNames.attr("class", geneNameClassName)
    		.attr("transform", "translate(0,0)")
			.selectAll("text")
    		.data(filteredGeneGroups)
			.enter().append("a")
			.attr("xlink:href", function(d, i) {
				var segmentUnit = geneSegmentSize*offset;
 				var y1 = geneSegmentSize*i*offset;
    			var y2 = geneSegmentSize*i*offset + segmentUnit;
    			return _this.jBrowseLink /*+ _this.scaffoldName*/ + ":"+ y1 + ".." + y2;})
			.attr("target", "_blank")
    		.append("text")
    		.attr("class", geneNameClassName)
    		.attr("x", labelEndPosX )
    		.attr("y", function(d, i) {
    			var pos1 = segmentHeight*i*offset;
    			return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1) ));})
    		.style("fill", "black") 
    		.style("text-anchor", "left")
    		.text(function(d) {	return d.gene_label;});

		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width + maxSegmentWidth - this.scaffoldWidth;

		return svgParent;
	}
			
};

mapViewerGenome.GenomeMarkersOnRect = class extends mapViewerGenome.GenomeObj {
	// draw the genetic markers on the scaffold that have correspondences to the reference linkage group	
	constructor( geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "genome_markers_on_rect";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldWidth = this.scaffoldRect.width;
		this.scaffoldHeight = this.scaffoldRect.height;
		this.invertGenomeScaffold = invertGenomeScaffold;
		this.markerData = this.geneData.getMarkerData();
		this.markers = this.markerData.getMarkers();
		this.maxScaffoldPos = this.geneData.getMaxGenomePos(); // max position on scaffold, either gene, mRNA, or genetic marker (not necessarily marker correspondence)
		this.jBrowseLink = this.geneData.getJBrowseLink();
		this.scaffoldName = this.geneData.getScaffoldName(); //this.geneData.getScaffoldJBrowseName();
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;			

		var markerClassName = this.containerName + "_marker";
		var markerNameClassName = this.containerName + "_marker_name";
		var linesToMarkerNameClassName = this.containerName + "_lines_to_marker_name";

		var range = this.maxScaffoldPos - 0; // assume scaffold starts at position 0;
		var scalingFactor = this.scaffoldHeight / range;
		
    	var inv1 = 1;
    	var inv2 = 0;
    	if (this.invertGenomeScaffold) {
    	  inv1 = 0;
    	  inv2 = 1;
    	}
		
		var _this = this;
	    // draw all gene marker segments on rect. 
	    var genomeMarkers = this.svg.append("g");
	    genomeMarkers.attr("class", markerClassName)
	    	.attr("transform", "translate(0,0)")
	    	.selectAll("rect") 
	    	.data(this.markers)
	    	.enter().append("rect")
	    	.attr("width", _this.scaffoldWidth)
	       	.attr("id", function(d) { return markerClassName+"_"+d.genome_marker_name;})
	       	.attr("x", _this.translateX )
	    	.attr("y", function(d) { 
	    		var pos1 = scalingFactor*(d.genome_marker_min);
	    		return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1) ));} )
			.attr("height", function(d) {
				var diff = d.genome_marker_max - d.genome_marker_min;
				var posDiff = (diff == 0) ? 1 : diff;  
				return scalingFactor * posDiff; } )
	    	.style("fill", "#0018F5")
	    	.attr("stroke", "black")
	    	.style("stroke-width", .5);
	    
	    // obtain filtered markers at regular interval range
	    var startPos = 0; //this.markerData.getLowestMarkerPosition();
	    var stopPos = this.maxScaffoldPos; //this.markerData.getHighestMarkerPosition();
	    var range = stopPos - startPos;
	    var interval = range/20;
	  
	    // sort the markers by position. 
	    var sorted = this.markers.sort(function(a, b) { return a.genome_marker_min - b.genome_marker_min;});
	    
	    // accumulate markers that are evenly spaced apart
	    var counter = parseInt(startPos);
	    var filteredLabelMarkers = [];
		sorted.forEach(function(d) {
	    	if (parseInt(d.genome_marker_min) >= counter) {
	    		filteredLabelMarkers.push(d);
	    		counter = parseInt(d.genome_marker_min) + interval;
	    	}
	    });
	    
	    // draw lines to marker names beside scaffold view.
    	var labelLineLength = 25;
    	var labelStartPosX = this.translateX + this.scaffoldWidth;
    	var labelEndPosX = labelStartPosX + labelLineLength;
	        	
	    var linesToMarkerNames = this.svg.append("g");
	    linesToMarkerNames.attr("class", linesToMarkerNameClassName)
    		.attr("transform", "translate(0,0)")
    		.selectAll("line")
    		.data(filteredLabelMarkers)
    		.enter().append("line")
    		.attr("class", linesToMarkerNameClassName)
    		.attr("x1", labelStartPosX ).attr("y1", function(d, i) { 
    			var pos1 = scalingFactor*(d.genome_marker_min);
    			return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1)));})
    		.attr("x2", labelEndPosX ).attr("y2", function(d, i) { 
    			var pos2 = scalingFactor * d.genome_marker_min;
    			return (_this.translateY + (inv1 * (pos2 - 5)) + (inv2 * (_this.scaffoldHeight - (pos2 + 5))));})
    		.style("stroke", "black")
        	.style("stroke-width", 1);
	    
	    // position the reference gene names beside the view
		var markerNames = this.svg.append("g");
    	markerNames.append("rect")
    		.attr("class", markerNameClassName)
    		
        	markerNames.attr("class", markerNameClassName)
    		.attr("transform", "translate(0,0)")
    		.selectAll("a")
    		.data(filteredLabelMarkers)
			.enter().append("a")
			.attr("xlink:href", function(d) {
				// create the JBrowse link with genetic marker region track
				var baseJBrowseLink = _this.jBrowseLink;
				var startPos = (parseInt(d.genome_marker_min) + 1);
				var endPos = d.genome_marker_max;
				var JBrowseLinkLoc = ":"+ startPos + ".." + endPos + "&" ;
				var markerType = "genetic marker"; 
				var chr = baseJBrowseLink.slice(baseJBrowseLink.indexOf('loc=') + 4); 
				var dataSubstr = baseJBrowseLink.slice(baseJBrowseLink.indexOf("data=data"), baseJBrowseLink.indexOf('&'));
				var index = dataSubstr.split("/",2).join("/").length + 1;
				var genome = dataSubstr.slice(index);
				var region = markerType + " Region";
				var _region = markerType + "_Region";
	            var genomeName = genome + " " + region;
                var addFeatures = "addFeatures=[{\"seq_id\":\"" + chr + "\",\"start\":" + startPos + ", \"end\": " + endPos + ", \"name\":\"" + genomeName + "\"}]&";
                var addTracks = "addTracks=[{\"label\":\"" + _region + "\",\"key\":\"" + region + "\",\"type\":\"JBrowse/View/Track/HTMLFeatures\",\"store\":\"url\"}]&";
                var tracks = "tracks=DNA,genes,"+_region+ "&highlight=";
				var fullJBrowseLink = baseJBrowseLink + JBrowseLinkLoc + addFeatures + addTracks + tracks;
				return fullJBrowseLink;})
			.attr("target", "_blank")
			.append("text")
			.attr("class", markerNameClassName)
			.attr("x", labelEndPosX )
    		.attr("y", function(d, i) {
    			var pos1 = scalingFactor*(d.genome_marker_min);
    			return (_this.translateY + (inv1 * pos1) + (inv2 * (_this.scaffoldHeight - pos1)));})
    		.style("fill", "black") 
    		.style("text-anchor", "left")
    		.text(function(d) {	return d.genome_marker_name;});

		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;
	}
};
	

mapViewerGenome.Polygon = class extends mapViewerGenome.GenomeObj {

	constructor( geneData, scaffoldRect) {
		super();
		this.containerName = "polygon";
		this.geneData = geneData;
		this.scaffoldRect = scaffoldRect;
		this.scaffoldHeight = this.scaffoldRect.height;
		this.maxScaffoldPos = this.geneData.getMaxGenomePos(); 
		this.polygonWidth = 60; // default. set to actual width of marker scaffold including labels during runtime
	}
		
	setWidth(polygonWidth) {
		this.polygonWidth = polygonWidth;
	}

	draw(svgParent) {
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;		
		
		var minScaleDomain = 0;
		var maxScaleDomain = this.maxScaffoldPos;
		var range = this.maxScaffoldPos - 0; // assume scaffold starts at position 0;
		var scalingFactor = this.scaffoldHeight / range;

		var	topRight = {x: this.polygonWidth, y: (minScaleDomain)*scalingFactor}; 
		var topLeft = {x: 0, y: this.scaffoldHeight};
		var bottomRight = {x: this.polygonWidth, y:(maxScaleDomain)*scalingFactor};
		var bottomLeft = {x: 0, y: 0}; 
	
		var polygon = svgParent.select("#"+this.containerName).append("polygon");
    	polygon.attr("id", "polygon").attr("class", "polygon");
    
		// draw the zoom polygon
    	polygon.style("fill","transparent").style("stroke-width", .5)
    		.style("stroke", "#696969")
    		.attr("points", topLeft.x + "," + topLeft.y + " " + bottomLeft.x + "," + bottomLeft.y + " " + 
					topRight.x + "," + topRight.y + " " + bottomRight.x + "," + bottomRight.y );

    	this.svg = polygon;
			
		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;

	}
};

	
mapViewerGenome.ScaffoldRectangle = class extends mapViewerGenome.GenomeObj {

	constructor(scaffoldRect) {
		super();
		this.containerName = "scaffold_rectangle";
		this.scaffoldWidth = scaffoldRect.width;
		this.scaffoldHeight = scaffoldRect.height;
	}
	
	draw(svgParent) {

		var Rect = svgParent.append("rect");
	    Rect.attr("width", this.scaffoldWidth)
	        .attr("id", this.containerName).attr("x", this.translateX)
	        .style("stroke-width", .5);

		// Zoomed view with flat end
		Rect.attr("y", this.translateY)
			.attr("height", this.scaffoldHeight)
	        .attr("rx", 0).attr("ry", 1) 
	       	.style("fill", "#FFDCDC").attr("stroke", "black");
		this.svg = Rect;

		this.height  = this.svg.node().getBBox().height;
		this.width  += this.svg.node().getBBox().width;

		return svgParent;
	}
	
	setTranslate(x,y) {
		this.translateX = x;
		this.translateY = y;
	}
		
};
	
mapViewerGenome.Ruler = class extends mapViewerGenome.GenomeObj {

	constructor(geneData, scaffoldRect, invertGenomeScaffold) {
		super();
		this.containerName = "scaffold_y_ruler";
		this.scaffoldRect = scaffoldRect;
		this.invertGenomeScaffold = invertGenomeScaffold;
		// set ruler orientation and axis
		 var zoomedSubsection = 1;
		 this.geneData = geneData;
		 this.scaffoldElementMaxPos = this.geneData.getMaxGenomePos(); 
		var startingPos = 0;

		// Update the scale domain: domain is the dataset size, range is the svg dimension to render
		// use the marker data to determine the max y domain ( stop value is higher than the start_pos or qtl_peak)
		this.scaleRange = d3.scaleLinear().range([0, this.scaffoldRect.height]);
		// set initial zoomed domain to display only a small subsection of markers on launch
		var zoomSubsectionEndPos = startingPos + this.scaffoldElementMaxPos*zoomedSubsection;
	
		if (this.invertGenomeScaffold) {
			// reverse the order of the ruler positions
			this.scaleRange.domain([zoomSubsectionEndPos, startingPos]);
		}
		else {
			this.scaleRange.domain([startingPos, zoomSubsectionEndPos]);
		}
	
		this.scale = this.scaleRange;
		 this.axisUnit = " bp";
		 this.rulerY = d3.svg.axis().scale(this.scale).orient("right").tickSize(1);
	}

	draw(svgParent) {
			
		var scaffoldOrZoomedView = svgParent; 
		scaffoldOrZoomedView.append("g") 
			.attr("id",this.containerName).attr("class", "y axis").attr("width", 1) 
			.attr("height", 1).attr("transform", "translate("+this.translateX+","+this.translateY+")")
			.call(this.rulerY);

		this.svg = scaffoldOrZoomedView.select("#"+this.containerName);
		this.height = this.svg.node().getBBox().height;
		// Add Y axis label:
		this.svg.append("text")
			.attr("id","chr_y_ruler_units").attr("text-anchor", "end")
			.attr("x", 15).attr("y", this.height + 10)
			.text(this.axisUnit);

		this.height = this.svg.node().getBBox().height;
   		// only the width of the scaffold ruler is set, the zoom ruler overlaps with the polygon 
		this.width = this.svg.node().getBBox().width;

		return svgParent;	    	
			
	}
};

mapViewerGenome.GenomeLabel = class extends mapViewerGenome.GenomeObj {

	constructor(genomeName, scaffoldName, genomeLink) {
		super();
		this.containerName = "genome_label";
		this.genomeId = 0;//genomeId;
		this.genomeName = genomeName;
		this.scaffoldName = scaffoldName;
		this.genomeLink = genomeLink;
	}
		
	draw(svgParent) {

		if (this.scaffoldName == "Null") {
			drupal_set_message(t('scaffold name is Null'), 'warning');				
		}

		var genomeUrl = this.genomeLink; //jbrowseLink; //Drupal.settings.baseUrl; // + "/jbrowse/index.html?" + "data=data/" + this.genomeName + "&loc=" + this.scaffoldName;
	
		this.drawContainer(svgParent);
		var tmpTranslateX = 0;
		var tmpTranslateY = 0;			
		var scaffoldGenomeLabelg = this.svg;

		var scaffoldGenomeLabelrect = scaffoldGenomeLabelg.append("rect");
	
		var titleMaxWidth = 320;
		var genomeLabelPadding = 15;
		this.translateX += genomeLabelPadding;
		this.translateY += genomeLabelPadding;
		var scaffoldViewXlinkText = scaffoldGenomeLabelg.append("text");
		scaffoldViewXlinkText.attr("id", "text-select").attr("class", "mvtitle_text")
	    	.attr("x", this.translateX).attr("y", this.translateY)
	    	.style("font-size", "1.3em").style("line-height", "1.2em")
	    	.text(this.scaffoldName + ' of genome '+ this.genomeName)
	    	.call(tripalMap.wrap, titleMaxWidth);

		// obtain height of wrapped genome and scaffold title text
	    var titleText = scaffoldViewXlinkText;
	    var padding = 8;
	    var titleTextHeight = titleText.node().getBBox().height + padding;

		// add the genome Detail button to link to JBrowse
		var svgRectRight = scaffoldGenomeLabelg.selectAll(".mvtitle_text").node().getBoundingClientRect().right - svgParent.node().getBoundingClientRect().x + 22;
		var button = scaffoldGenomeLabelg.append("g");
		button.attr("id", "buttonGenomeLabel")
	    	.attr("transform", "translate(" + (svgRectRight + this.translateX) + "," + (this.translateY ) + ")");

	    var bbox = button.node().getBBox();
	    var rect = button.append("rect");
	    rect.attr("x", bbox.x - 12)
	        .attr("y", bbox.y - 20)
	        .attr("width", 135 )
	        .attr("height", 30 )
	        .style("fill", "#f5f5f5").style("stroke-width", "1px").style("stroke", "#e3e3e3");

	    var buttonUrl = button.append("a");
	    buttonUrl.attr("xlink:href", genomeUrl)
	    var buttonUrlText = buttonUrl.append("text");
	    buttonUrlText.style("font-size", "1.3em").style("line-height", "1.2em").text("Genome Detail");
 
	    this.svg = scaffoldGenomeLabelg;

	    var buf = 25;
	    this.height = this.svg.node().getBBox().height + buf + (genomeLabelPadding*2); //buffer for scaffold below
	    this.width = this.svg.node().getBBox().width + (genomeLabelPadding*2);

		scaffoldGenomeLabelrect.attr("width", this.width ).attr("height", (this.height - buf ))
		.attr("y", bbox.y - 10);
	    	
		return svgParent;
			
	}
		
};
	
	
