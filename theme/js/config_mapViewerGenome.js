
// Classes:
//
// Marker data instantiated for Reference and Comparison chromosomes
// 

var configMapViewerGenome = configMapViewerGenome || {};

configMapViewerGenome = {  
 
	GenomeData: class {

		/* example data:
		"analysis_name":"Gossypium raimondii (D5) genome JGI assembly v2.0 (annotv2.1)", "analysis_scaffold_name":"Chr02_JGI_v2.0",
		"num_buckets":200, "buckets":{ "1":{"num_genes":49,"gene_label":"Gorai.002G000100-JGI_221_v2.1"},"2":..}}
		*/
		constructor(geneData, markerData, markerTypeColorMap) {
			this.genomeName = geneData.analysis_name;
			this.scaffoldName = geneData.analysis_scaffold_name;
			this.jbrowseLink = geneData.jbrowse_link;
			this.genomePageLink = geneData.genome_page_link;
			this.maxGenomePos = 0;
			this.genes = new configMapViewerGenome.Genes(geneData, markerTypeColorMap); 
			this.markers = new configMapViewerGenome.Markers(markerData);
			this.calculateMaxGenomePos();
		}
		
		getGenomeName() {
			return this.genomeName;
		}
		getScaffoldName() {
			return this.scaffoldName;
		}

		getJBrowseLink() {
			return this.jbrowseLink;
		}
		getGenomePageLink() {
			if (this.genomePageLink) {
				this.genomePageLink = Drupal.settings.baseUrl+this.genomePageLink;
			}
			return this.genomePageLink;
		}
		getMaxGenomePos() {
			return this.maxGenomePos;
		}
		calculateMaxGenomePos() {
			this.maxGenomePos = this.genes.getMaxPos() > this.markers.getMaxPos() ? this.genes.getMaxPos() : this.markers.getMaxPos();
		}
		
		// genes
		getGenes() {
			return this.genes;
		}
		
		// markers
		getMarkerData() {
			return this.markers;
		}
		
		findCorrespondences( uniqueMarkers ) {
			
		}
		
	},
	
	Genes: class {

		// class for genes and mRNA data
		constructor(geneData, markerTypeColorMap) {
			this.geneData = geneData;
			this.groups = geneData.buckets;
			this.maxGenePos = geneData.max_gene_pos;
			this.numGroups = geneData.num_buckets;
			this.numGenes = this.getNumGenes();
			this.groupLabels = []; // [[pos1: name], [pos2: name], ...]]
			
			// assign gene color from marker color map
			var markerColor = "";
			var markerType = "gene";
			if (markerType in markerTypeColorMap) {
				markerColor = markerTypeColorMap[markerType]; // marker type exists in the colormap, assign the corresponding color
			}
			else {
				markerColor = "black"; // as a default set the marker color to black
			}
			this.color = markerColor;
		}
		
		getGroups() {
			return Object.values(this.groups);
		}
		getNumGroups() {
			return this.numGroups;
		}
		
		getNumGenes() {
			this.numGenes = (Object.values(this.groups).map(function(i) {return i.num_genes;})).reduce((a, b) => a + b, 0);
			return this.numGenes;
		}
		getMaxPos() {
			return this.maxGenePos;
		}
		getColor() {
			return this.color;
		}
	},
	
	Markers: class {
		
		// class for markers on the genome scaffold
		constructor(markerData) {
			this.markerData = markerData.markers;
			this.lowestMarkerCorresPosition = 0;
			this.highestMarkerCorresPosition = 0;
			this.maxMarkerPos = markerData.max_marker_pos; // max position of any genetic marker on scaffold, not necessarily a marker correspondence
		}

		getMarkers() {
			return this.markerData;
		}
		
		getLowestMarkerPosition() {
			this.lowestMarkerCorresPosition = (Object.values(this.markerData).map(function(i) {
				return parseInt(i.genome_marker_min);
			})).reduce((a, b) => a < b ? a : b);
			return this.lowestMarkerCorresPosition;
		}
		
		getHighestMarkerPosition() {
			this.highestMarkerCorresPosition = (Object.values(this.markerData).map(function(i) {
				return parseInt(i.genome_marker_min);
			})).reduce((a, b) => a < b ? b : a);
			return this.highestMarkerCorresPosition;
		}
		
		getMaxPos() {
			return this.maxMarkerPos;
		}
		
	},
		
};

