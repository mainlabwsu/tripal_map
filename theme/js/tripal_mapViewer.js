/* 
 * File: tripal_mapViewer.js
 * Obtain settings from MapViewer PHP form, call linkageGroup draw to display selected chromosomes.
 */

(function($) {
	Drupal.behaviors.tripal_mapViewerBehavior = {
	attach: function (context, settings) {

		$('#select_fieldset_mapViewer').once('select_fieldset_mapViewer', function() {

		// Reference chromosome	
		var geneticFeatures =			Drupal.settings.mapViewerDisplayJS.mapViewer_genetic_features;
		var mapName =					Drupal.settings.mapViewerDisplayJS.reference_map_name;        
		var linkageGroupName =			Drupal.settings.mapViewerDisplayJS.reference_linkage_group;
		var mapUnitType = 				Drupal.settings.mapViewerDisplayJS.reference_map_unit_type;
		var markerHighlight =			Drupal.settings.mapViewerDisplayJS.reference_genetic_marker_highlight;        
		markerHighlight['marker_pos'] = parseFloat(markerHighlight['marker_pos']); // convert string to float for marker pos

		// Comparison chromosome
		var geneticFeaturesComparison =	Drupal.settings.mapViewerDisplayComparisonJS.mapViewer_genetic_features_comparison;
		var mapComparisonName =			Drupal.settings.mapViewerDisplayComparisonJS.comparison_map_name;        
		var linkageGroupComparisonName=	Drupal.settings.mapViewerDisplayComparisonJS.comparison_linkage_group;        
		var mapComparisonUnitType = 	Drupal.settings.mapViewerDisplayComparisonJS.comparison_map_unit_type;
		var showComparison =			Drupal.settings.mapViewerDisplayComparisonJS.show_comparison;

		// Comparison genome scaffold
		var genomeFeaturesComparison = 	Drupal.settings.mapViewerDisplayGenomeJS.mapViewer_genome_features_comparison;
		var analysisComparisonName =   	Drupal.settings.mapViewerDisplayGenomeJS.genome_analysis_name;        
		var scaffoldComparisonName =    Drupal.settings.mapViewerDisplayGenomeJS.genome_scaffold;        
		var showGenomeComparison = 		Drupal.settings.mapViewerDisplayGenomeJS.show_genome_comparison;
		var invertGenomeScaffold = 		Drupal.settings.mapViewerDisplayGenomeJS.invert_genome_scaffold;

		var sData = genomeFeaturesComparison;
		var markerTypeDisplayStates = Drupal.settings.mapViewerDisplayJS.marker_type_display_states;
		var markerTypeColorMap =      Drupal.settings.mapViewerDisplayJS.marker_type_color_map;

		var showRuler = 0;
		if ($("#show_ruler_mapViewer:checked").val() !== undefined) {
			showRuler = 1;
		}

		var showMarkerPos = 0;
		if ($("#marker_pos_mapViewer:checked").val() !== undefined) {
			showMarkerPos = 1;
		}

		var expandQTL = 0;
		if ($("#qtl_expand_mapViewer:checked").val() !== undefined) {
			expandQTL = 1;
		}

		var strMarkerTypeDisplayStates = "";
		if (Object.keys(markerTypeDisplayStates).length > 0) {
			strMarkerTypeDisplayStates = JSON.stringify(markerTypeDisplayStates);
		}

		var show = {Ruler: showRuler, MarkerPos: showMarkerPos, ExpandQTL: expandQTL, Comparison: (showComparison || showGenomeComparison)};
		var markers = obtainMarkers(geneticFeatures, geneticFeaturesComparison, showComparison);
		var uniqueMarkers = markers[0];  
		var uniqueMarkersComparison = markers[1];

		// Find markers that contribute most to the dimensional extent of the chromosome view. 
		// Take all qtls, haplotype block and bin if exist.
		QTLLaneMarkerTypes = ['QTL', 'haplotype_block', 'bin'];
		var uniqueMarkersQTL = uniqueMarkers.slice(0).filter(i => (QTLLaneMarkerTypes.includes(i.genetic_marker_type)));
		var filteredUniqueMarkers = uniqueMarkers.slice(0).filter(i => (!(QTLLaneMarkerTypes.includes(i.genetic_marker_type))));  // deep copy array
		// if displaying genetic_marker_name or the genetic_marker_locus_name 
		if (Drupal.settings.tripal_map.corresMarkerNameType == 'Genetic Marker Name') {
			// Sort non QTL markers by name length, keeping only the longest one. (QTL has separate lane and can uses abbreviated text so handle separately)
			filteredUniqueMarkers.sort((a, b) => parseFloat(b.genetic_marker_name.length) - parseFloat(a.genetic_marker_name.length));
		}
		else {
			filteredUniqueMarkers.sort((a, b) => parseFloat(b.genetic_marker_locus_name.length) - parseFloat(a.genetic_marker_locus_name.length));
		}
		if (filteredUniqueMarkers.length > 0) {
			uniqueMarkersQTL.push(filteredUniqueMarkers[0]); // add the marker with the longest name
			// add the markers at positional start and end of linkage group to assess the full linkage group width at all positions
			filteredUniqueMarkers.sort((a, b) => parseFloat(b.marker_start_pos) - parseFloat(a.marker_start_pos));
			var len = filteredUniqueMarkers.length;
			uniqueMarkersQTL.push(filteredUniqueMarkers[0]); // add the last marker 
			uniqueMarkersQTL.push(filteredUniqueMarkers[len-1]); // add the first marker
		}
		filteredUniqueMarkers = uniqueMarkersQTL;
		   
		// Take all qtls, haplotype block and bin if exist.
		var uniqueMarkersComparisonQTL = uniqueMarkersComparison.slice(0).filter(i => (QTLLaneMarkerTypes.includes(i.genetic_marker_type)));
		var filteredUniqueMarkersComparison = uniqueMarkersComparison.slice(0).filter(i => (!(QTLLaneMarkerTypes.includes(i.genetic_marker_type))));  // deep copy array
		
		// if displaying genetic_marker_name or the genetic_marker_locus_name 
		if (Drupal.settings.tripal_map.corresMarkerNameType == 'Genetic Marker Name') {
			// Sort non QTL markers by name length, keeping only the longest one. (QTL has separate lane and can uses abbreviated text so handle separately)
			filteredUniqueMarkersComparison.sort((a, b) => parseFloat(b.genetic_marker_name.length) - parseFloat(a.genetic_marker_name.length));
		}
		else {
			filteredUniqueMarkersComparison.sort((a, b) => parseFloat(b.genetic_marker_locus_name.length) - parseFloat(a.genetic_marker_locus_name.length));
		}
		if (filteredUniqueMarkersComparison.length > 0) {
			uniqueMarkersComparisonQTL.push(filteredUniqueMarkersComparison[0]);
		}
		filteredUniqueMarkersComparison = uniqueMarkersComparisonQTL;

		// initialize marker data for dimensional calculations
		var markerDataCalcRet = initMarkerData(linkageGroupName, mapName, mapUnitType, filteredUniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
			linkageGroupComparisonName, mapComparisonName, mapComparisonUnitType, filteredUniqueMarkersComparison, showGenomeComparison,
			sData);	   

		var markerDataCalc = markerDataCalcRet[0];
		var genomeData = markerDataCalcRet[1];

		var translateY = 15;
		var svgHeight = 655;
		var containerRet = createSVGContainer(show, mapName, filteredUniqueMarkers, linkageGroupName, markerHighlight, markerDataCalc,
			mapComparisonName, linkageGroupComparisonName, filteredUniqueMarkersComparison, showComparison,
			genomeData, showGenomeComparison, invertGenomeScaffold, translateY, svgHeight);
		
		var svg = containerRet[0];
		var chrFrameRefWidth = containerRet[1];
		var pageFit = containerRet[2];
	    	
		//====  Initialize marker data with full marker set for drawing on the chromosome views.====
		var markerDataRet = initMarkerData(linkageGroupName, mapName, mapUnitType, uniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
			linkageGroupComparisonName, mapComparisonName, mapComparisonUnitType, uniqueMarkersComparison, showGenomeComparison,
			sData);	    
				
		var markerData = markerDataRet[0];
		var genomeData = markerDataRet[1];

		// Moved draw the save to png icon to PHP users toolbar
		markerData.findCorrespondences(mapName, linkageGroupName, genomeData);

		// also draws genome scaffold
		svg = drawChromosomes(uniqueMarkers, mapName, linkageGroupName, markerHighlight, markerData, chrFrameRefWidth,
			uniqueMarkersComparison, mapComparisonName, linkageGroupComparisonName, showComparison,
			genomeData, showGenomeComparison, invertGenomeScaffold, pageFit, show, translateY, svg);

		svg = drawLegend(markerData, genomeData, translateY, svgHeight, svg, showComparison, showGenomeComparison);

    });


	//=================================================================================================================================
	// Helper functions
	//=================================================================================================================================
	function obtainMarkers(geneticFeatures, geneticFeaturesComparison, showComparison) {
		// Obtain markers, comparison markers
		// be very careful when creating a unique list of markers as some have the same name, but different position. 
		var uniqueMarkers = [];
		var uniqueMarkersComparison = [];

		for (var key in geneticFeatures) {
			uniqueMarkers.push(geneticFeatures[key]);
		}

		// Obtain comparison marker data if enabled
		if (showComparison) {
			for (var key in geneticFeaturesComparison) {
				uniqueMarkersComparison.push(geneticFeaturesComparison[key]);
			}
		}
		var ret = [uniqueMarkers, uniqueMarkersComparison]; 
		return ret;
	};

	function initMarkerData(linkageGroupName, mapName, mapUnitType, uniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
		linkageGroupComparisonName, mapComparisonName, mapComparisonUnitType, uniqueMarkersComparison, showGenomeComparison, sData) {

		var markerData = new configMapViewer.MarkerData(strMarkerTypeDisplayStates, markerTypeColorMap);
		var genomeData = {};

		var linkageGroupId = "lg";
		var mapId = uniqueMarkers[0].featuremap_id;
		if (!(linkageGroupName)) {
			linkageGroupName = "Null";
		}

		markerData.addLinkageGroup(uniqueMarkers, linkageGroupName, mapName, linkageGroupId, mapId, mapUnitType, mapViewer.OrientationEnum.LEFT);

		if (showComparison) {
			var linkageGroupComparisonId = "lgComp";
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			if (!(linkageGroupComparisonName)) {
				linkageGroupComparisonName = "Null";
			}
			markerData.addLinkageGroup(uniqueMarkersComparison, linkageGroupComparisonName, mapComparisonName, linkageGroupComparisonId, mapComparisonId, mapComparisonUnitType, mapViewer.OrientationEnum.RIGHT);
		}
			    
		/*
		* sData: {"status":0, "data":{"analysis_name":"Prunus persica Whole Genome v1.0 Assembly & Annotation",
		* "analysis_scaffold_name":"scaffold_4","num_buckets":200,"buckets":{"1":{"num_genes":16,"gene_label":"ppa003523m.g"},"2":{"num_genes":31,"gene_label":"ppa026835m.g"},"3":
		*/
		else if (showGenomeComparison) {
			if ( Object.entries(sData).length > 0) {
				sDataGenomeGenes = sData[0];
				sDataGenomeMarkers = sData[1];
				genomeData = new configMapViewerGenome.GenomeData(sDataGenomeGenes, sDataGenomeMarkers, markerTypeColorMap);
			}
		}
	
		var ret = [markerData, genomeData];
		return ret;
	};
		    	
	function createSVGContainer(show, mapName, uniqueMarkers, linkageGroupName, markerHighlight, markerData,
		mapComparisonName, linkageGroupComparisonName, uniqueMarkersComparison, showComparison,
		genomeData, showGenomeComparison, invertGenomeScaffold, translateY, svgHeight) {
  
		// prepend drawing view container before the control panel PHP form
		var container =  "#select_fieldset_mapViewer";
		$(container).before('<div></div><div id ="select_fieldset_mapViewer_svg" width="100%"></div>');

		// clear the svg container first
		var svgFieldset = "#select_fieldset_mapViewer_svg";
		d3.select(svgFieldset).selectAll("svg").remove(); 

		var svgMaxWidth = 1100;
		var pageFit = false;

		var svg = d3.select(svgFieldset)
			.append("svg").attr("class", "TripalMap").attr("width", svgMaxWidth).attr("height", svgHeight);

		// set the initial Marker Position, the case where the page specifically highlights a marker
		var initMarkerHighlight = markerHighlight;

		// get the svg width
		var mapId = uniqueMarkers[0].featuremap_id;
		var init = true;
		var chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
			linkageGroupName, markerData, initMarkerHighlight, init);
		var chrFrameDimensions = chrFrameReference.calculateDimensions(svg);
		var chrFrameRefWidth = chrFrameDimensions["width"];
		var chrFrameRefHeight = chrFrameDimensions["height"];
 
		var chrFrameCompDimensions = {"width": 0, "height": 0};
		var chrFrameCompWidth = 0;
		var chrFrameCompHeight = 0;
		var chrFrameComparison = "";
		var chrFrameGenomeComparison = "";
		var chrFrameGenomeCompDimensions = {"width": 0, "height": 0};

		if (showComparison) {
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			chrFrameComparison = new mapViewer.ChrFrame(mapViewer.OrientationEnum.RIGHT, pageFit, show, mapComparisonName, 
				mapComparisonId, linkageGroupComparisonName, markerData, initMarkerHighlight, init);
			chrFrameCompDimensions = chrFrameComparison.calculateDimensions(svg); 
			chrFrameCompWidth = chrFrameCompDimensions["width"];
			chrFrameCompHeight = chrFrameCompDimensions["height"];
		}
		else if (showGenomeComparison) {
			var genomeFrame = new mapViewerGenome.GenomeFrame(genomeData, invertGenomeScaffold, show);
			var genomeFrameDimensions = genomeFrame.calculateDimensions(svg);
			var genomeFrameWidth = genomeFrameDimensions["width"];
			var genomeFrameHeight = genomeFrameDimensions["height"];
	
			chrFrameCompWidth = genomeFrameWidth; 
			chrFrameCompHeight = genomeFrameHeight; 
		}

		// if a scrollbar is required, nest the svg in the div for scrolling
		var svgWidth = chrFrameRefWidth + chrFrameCompWidth + 100 + 35;
		var maxChrFrameHeight = chrFrameRefHeight > chrFrameCompHeight ? chrFrameRefHeight : chrFrameCompHeight;
		var legendHeight = 15;
		var legendBuf = 35;
		legendHeight += legendBuf;
		var svgTotalHeight = maxChrFrameHeight + translateY + legendHeight;
		var mvScroll = "mv_scroll";
		if (svgWidth > svgMaxWidth) {
			// scrolling is required so try to fit the page by adapting the qtl and polygon sizes
			pageFit = true;
			chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
				linkageGroupName, markerData, initMarkerHighlight, init);
			chrFrameDimensions = chrFrameReference.calculateDimensions(svg);
			chrFrameRefWidth = chrFrameDimensions["width"];
			svgWidth = chrFrameRefWidth + chrFrameCompWidth + 200 + 35;
            
			d3.select(svgFieldset).selectAll("svg").remove(); 
			d3.select(svgFieldset)
				.append("div").attr("class", "TripalMap").attr("id", mvScroll);
			svg = d3.select("#"+mvScroll)
				.append("svg").attr("class", "TripalMap").attr("width", svgWidth).attr("height", svgTotalHeight);
				// try to fit the page by sending flag to drawing code
		}
		else {
			d3.select(svgFieldset).selectAll("svg").remove(); 
			svg = d3.select(svgFieldset)
				.append("svg").attr("class", "TripalMap").attr("width", svgMaxWidth).attr("height", svgTotalHeight);
		}
		var ret = [svg, chrFrameRefWidth, pageFit]; 
		return ret;
	};		   

	function drawChromosomes(uniqueMarkers, mapName, linkageGroupName, markerHighlight, markerData, chrFrameRefWidth,
		uniqueMarkersComparison, mapComparisonName, linkageGroupComparisonName, showComparison,
		genomeData, showGenomeComparison, invertGenomeScaffold, pageFit, show, translateY, svg) {

		// set the initial Marker Position, the case where the page specifically highlights a marker
		var initMarkerHighlight = markerHighlight;
		var genomeFrame  = "";

		// create chromosome frames for linkage groups
		var mapId = uniqueMarkers[0].featuremap_id;
		var init = false;
		var chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
				linkageGroupName, markerData, initMarkerHighlight, init);
		var chrFrameComparison = "";
		if (showComparison) {
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			chrFrameComparison = new mapViewer.ChrFrame(mapViewer.OrientationEnum.RIGHT, pageFit, show, mapComparisonName, 
					mapComparisonId, linkageGroupComparisonName, markerData, initMarkerHighlight, init);
		}
		else if (showGenomeComparison) {
			genomeFrame = new mapViewerGenome.GenomeFrame(genomeData, invertGenomeScaffold, show);
		}
				
		// draw markers on linkage groups
		// Reference linkage group
		var translateX = 0;
		chrFrameReference.setTranslate(translateX, translateY);
		svg = chrFrameReference.draw(svg);
		
		translateX = chrFrameRefWidth + 15; // take the statically calculated width based on full data set instead of zoomed section 
		// Comparison linkage group
		if (showComparison) {
			chrFrameComparison.setTranslate(translateX, translateY);
			svg = chrFrameComparison.draw(svg);
		}
				
		// draw genome scaffold
		else if (showGenomeComparison)  {
			translateX += 25;
			genomeFrame.setTranslate(translateX, translateY);
			svg = genomeFrame.draw(svg); 
		}

		return svg;
	};		

	function drawLegend(markerData, genomeData, translateY, svgHeight, svg, showComparison, showGenomeComparison) {

		// draw the legend (including marker types that are configured as hidden) but only for the selected linkage groups	
		var legend = new tripalMap.Legend(markerData, genomeData);
		var chrFrameRefHeight = svg.selectAll("#chrFrame_0").node().getBBox().height
		var chrFrameCompHeight = 0;
		if (showComparison) {
			chrFrameCompHeight = svg.selectAll("#chrFrame_1").node().getBBox().height;
		}
		else if (showGenomeComparison) {
			chrFrameCompHeight = svg.selectAll("#genome_frame").node().getBBox().height; 
		}

		var maxChrFrameHeight = chrFrameRefHeight > chrFrameCompHeight ? chrFrameRefHeight : chrFrameCompHeight;

		var padding = 35; // padding between chromosome frame and legend
		var legendHeight = 60;
		var buf = translateY + padding + legendHeight;
		maxChrFrameHeight = maxChrFrameHeight > (svgHeight - buf) ? (svgHeight - buf)  : maxChrFrameHeight; 
		
		var _translateY = maxChrFrameHeight + translateY + padding; 
		legend.setTranslate(0, _translateY);
		svg = legend.draw(svg);
		return svg; 
	};

	}};
})(jQuery);
