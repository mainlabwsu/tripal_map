<?php

/*
 * File: mainlab_feature_bin_map_loci.tpl.php
 * From MainLab Tripal extension module that includes custom tables for Chado used
 * by the Main Bioinformatics Laboratory. Version core = 7.x.
 * Modified for tripal_map to display link to MapViewer page from the feature Map Positions pane.
 */

$feature = $variables['node']->feature;
$total_features = $feature->num_loci;
$num_per_page = 25;

$pager = tripal_map_mainlab_tripal_get_pager($total_features, $num_per_page, 'map_loci');
$feature_positions = tripal_map_mainlab_tripal_get_bin_loci($feature->feature_id, $feature->featuremap_id, $num_per_page, $pager['page'] - 1);
$all_positions = tripal_map_mainlab_tripal_get_bin_loci($feature->feature_id, $feature->featuremap_id, 'ALL', 0);

// get the total number of records
if($total_features > 0){ ?>
  <div class="tripal_feature-data-block-desc tripal-data-block-desc" style="float:left">This map contains <?php print number_format($total_features) ?> marker loci:</div> 
  <?php 
  // Preparing download
  $dir = 'sites/default/files/tripal/mainlab_tripal/download';
  if (!file_exists($dir)) {
      mkdir ($dir, 0777);
  }
  $download = $dir . '/map_loci_data_feature_id_' . $feature->feature_id . '.csv';
  $handle = fopen($download, "w");
  fwrite($handle, "Marker Loci for Bin: " . $feature->name. "\n");
  fwrite($handle, '"#","Linkage Group","Marker Name","Locus Name","Type","Position"' . "\n");
  $counter = 0;
  foreach ($all_positions as $position) {
      $pos = $position->position || $position->position === '0' ? round($position->position, 2) . ' ' . $feature->unittype_id->name : '-';
      fwrite($handle, '"' . ($counter + 1) . '","'. $position->lg . '","' . $position->genetic_marker . '","'
      . $position->marker . '","' . $position->type . '","' . $pos . '"' . "\n");
      $counter ++;
  }
  fclose($handle);
  ?>
  <div style="float: right">Download <a href="<?php print '/' . $download;?>">Table</a></div>
  <?php   
  // the $headers array is an array of fields to use as the colum headers.
  // additional documentation can be found here
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $headers = array('Linkage Group', 'Marker Name', 'Locus Name', 'Type', 'Position', 'MapViewer');
  
  // the $rows array contains an array of rows where each row is an array
  // of values for each column of the table in that row.  Additional documentation
  // can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $rows = array();
  foreach ($feature_positions as $position){
    $link = tripal_map_mainlab_tripal_link_record('feature', $position->feature_id);
    if (!$link) {
        $link = tripal_map_mainlab_tripal_link_record ('feature', $position->marker_feature_id);
    }
    $pos = $position->position || $position->position === '0' ? round($position->position, 2) . ' ' . $feature->unittype_id->name : '-';
    $mapviewer = (!$feature->feature_id)? "N/A" : "<a href=\"/mapviewer/$feature->featuremap_id"
      . "/$position->linkage_group/$position->feature_id\" target=\"_blank\">View</a>";
    
    $rows[] = array(
      $position->lg,
      $link ? "<a href='$link'>" . $position->genetic_marker . '</a>': $position->genetic_marker,
      $position->marker,
      $position->type,
      $pos,
      $mapviewer
    );
  } 

  // the $table array contains the headers and rows array as well as other
  // options for controlling the display of the table.  Additional
  // documentation can be found here:
  // https://api.drupal.org/api/drupal/includes%21theme.inc/function/theme_table/7
  $table = array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature-table-featurepos',
      'class' => 'tripal-data-table'
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  
  // once we have our table array structure defined, we call Drupal's theme_table()
  // function to generate the table.
  print theme_table($table);

  // Add pager
  print $pager['pager'];
}

