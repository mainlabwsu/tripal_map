<?php

/*
 * File: mainlab_feature_heritable_phenotypic_marker_map_positions.tpl.php
 * From MainLab Tripal extension module for Tripal that includes custom tables for Chado used
 * by the Main Bioinformatics Laboratory. Version core = 7.x.
 * Modified for tripal_map to display link to MapViewer page from the feature Map Positions pane.
 */

$feature = $variables['node']->feature;
$map_positions = $feature->mainlab_mtl->map_positions;
$counter_pos = count($map_positions);

if ($counter_pos > 0) {
  $cmap_enabled = variable_get('mainlab_tripal_cmap_links', 1);
  $header = array ('#', 'Map Name', 'Linkage Group', 'Bin', 'Chromosome', 'Position', 'MapViewer');
  if ($cmap_enabled) {
    $header[] = 'CMap';
  }

  $feature_prop = property_exists($feature, 'featureprop') ? $feature->featureprop : array();
  $num_feature_prop = count($feature_prop);
  $mtl_marker_id = -1;
  if ($num_feature_prop > 0) {
    $feature_id = property_exists($feature_prop[0],'feature_id') ? $feature_prop[0]->feature_id : 0;
    if ($feature_id) {
      $mtl_marker_id = property_exists($feature_id,'feature_id') ? $feature_id->feature_id : -1;
    }
  }

  $rows = array ();
  $counter = 1; 
  foreach ($map_positions AS $pos) {
    $link = tripal_map_mainlab_tripal_link_record('featuremap', $pos->featuremap_id);
    $map = $link ? "<a href=\"$link\">$pos->name</a>" : $pos->name;
    $lg = $pos->linkage_group ? $pos->linkage_group : "N/A";
    $bin = $pos->bin ? $pos->bin : "N/A"; 
    $chr = $pos->chr ?$pos->chr : "N/A";
    
    $start = round($pos->mtl_start, 2) == 0 ? "N/A" : round($pos->mtl_start, 2);
    $highlight = $node->feature->uniquename;

    $linkage_group = $pos->linkage_group;
    if ($linkage_group) {
      $linkage_group = tripal_map_encode_special_chars($linkage_group);
    }
    else {
      $linkage_group = "";
    }
    
    $mapviewer = (!$pos->featuremap_id || !$pos->linkage_group)? "N/A" : "<a href=\"/mapviewer/$pos->featuremap_id".
      "/$linkage_group/$mtl_marker_id\" target=\"_blank\">View</a>";

    if ($cmap_enabled) {
      $cmap = (!$pos->urlprefix || !$pos->accession)? "N/A" : "<a href=\"$pos->urlprefix$pos->accession" . 
      "&ref_map_acc=-1&highlight=" . $highlight . "\">View</a>";
      $rows[] = array ($counter, $map, $lg, $bin, $chr, $start, $mapviewer, $cmap);
    }
    else {
      $rows[] = array ($counter, $map, $lg, $bin, $chr, $start, $mapviewer);
    }
    
    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_mtl-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print theme_table($table);
} ?>
