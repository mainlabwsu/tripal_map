<?php

/**
 * @ingroup mainlab_featuremap
 */

function tripal_map_preprocess_mainlab_featuremap_base(&$variables) {
  $featuremap = $variables['node']->featuremap;
  $num_loci = chado_query(
    "SELECT count (F.uniquename)
      FROM {featurepos} FP
      INNER JOIN {feature} F ON F.feature_id = FP.feature_id
      WHERE F.type_id = (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
        AND featuremap_id = :featuremap_id", array(':featuremap_id' => $featuremap->featuremap_id))->fetchField();
  $num_lg = chado_query(
    "SELECT count (distinct F.uniquename)
      FROM {featurepos} FP
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      WHERE F.type_id = (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'linkage_group' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'sequence'))
        AND featuremap_id = :featuremap_id", array('featuremap_id' => $featuremap->featuremap_id))->fetchField();
  $cmap_r = chado_query(
    "SELECT db.urlprefix, X.accession
      FROM {featuremap_dbxref} FD
      INNER JOIN {dbxref} X ON FD.dbxref_id = X.dbxref_id
      INNER JOIN {db} ON db.db_id = X.db_id
      WHERE featuremap_id = :featuremap_id
      ", array(':featuremap_id' => $featuremap->featuremap_id));
  $cmap = $cmap_r->fetchObject();
  $cmap_url = is_object($cmap) ? $cmap->urlprefix . $cmap->accession : NULL;

  $featuremap->num_loci = $num_loci;
  $featuremap->num_lg = $num_lg;
  $featuremap->cmap_url = $cmap_url;

  // this is the preprocess for the genetic map overview form drawn at the top of the Map Overview
  tripal_map_preprocess_tripal_map_genetic_map_overview($variables);
}


function tripal_map_preprocess_tripal_map_genetic_map_overview(&$variables) {
  $node = $variables['node'];
  $featuremap = $variables['node']->featuremap; // duplicate from above

  // if we are editing an existing node then the map is already part of the node
  $featuremap_id = $featuremap->featuremap_id;
  $variables['tripal_map_genetic_map_overview_form'] = drupal_get_form('tripal_map_genetic_map_overview_form', $featuremap_id);

}


function tripal_map_preprocess_mainlab_feature_genetic_marker_map_positions(&$variables) {

  $feature = $variables['node']->feature;
  // get map positions
  $results = chado_query(
    "SELECT FM.featuremap_id, CF.nid AS nid, FM.name AS name, X.accession,
    DB.urlprefix, LG.name AS linkage_group, BIN.name AS bin,
    LGP.value AS chr, FPP.value AS locus_start, LOCUS.name AS locus_name

    FROM {feature} LOCUS
    INNER JOIN {feature_relationship} FR 		ON FR.subject_id = LOCUS.feature_id
    INNER JOIN {featurepos} FP 					ON LOCUS.feature_id = FP.feature_id
    INNER JOIN {featuremap} FM 					ON FP.featuremap_id = FM.featuremap_id
    LEFT JOIN {featuremap_dbxref} FD 			ON FP.featuremap_id = FD.featuremap_id
    LEFT JOIN {dbxref} X 						ON FD.dbxref_id = X.dbxref_id
    LEFT JOIN {db}	 							ON db.db_id = X.db_id
    INNER JOIN {feature} LG 					ON FP.map_feature_id = LG.feature_id
    LEFT JOIN
      (SELECT * FROM {featureprop} WHERE type_id =
      (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'chr_number'
        AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
    ) LGP ON LG.feature_id = LGP.feature_id
    INNER JOIN {featureposprop} FPP 			ON FP.featurepos_id = FPP.featurepos_id
    LEFT JOIN
      (SELECT F2.name, FR2.subject_id FROM {feature} F2
        INNER JOIN {feature_relationship} FR2 ON FR2.object_id = F2.feature_id
        WHERE FR2.type_id =
        (SELECT cvterm_id FROM {cvterm} WHERE name = 'located_in' AND 
          cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship'))
        ) BIN ON LOCUS.feature_id = BIN.subject_id
    LEFT JOIN chado_featuremap CF ON FM.featuremap_id = CF.featuremap_id
    WHERE FR.type_id =
      (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'instance_of'
        AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship')
      )
    AND LOCUS.type_id =
      (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'marker_locus'
        AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
    AND FPP.type_id =
      (SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'start'
        AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
    AND FR.object_id = :object_id", array(':object_id' => $feature->feature_id));
  $positions = array();
  $counter = 0;
  while ($pos = $results->fetchObject()) {
    $positions [$counter] = $pos;
    $counter ++;
  }
  $feature->map_positions = $positions;

}


function tripal_map_preprocess_mainlab_feature_QTL_map_positions(&$variables) {

  $feature = $variables['node']->feature;
  $sql = "
  SELECT FM.featuremap_id, CF.nid AS nid, FM.name AS name, X.accession,
  DB.urlprefix,  LG.name AS linkage_group, BIN.name AS bin,
  LGP.value AS chr, START.value AS QTL_start, STOP.value AS QTL_stop,
  PEAK.value AS QTL_peak,  QTL.uniquename AS QTL_name
  
  FROM {feature} QTL
  INNER JOIN {featurepos} FP 							ON QTL.feature_id = FP.feature_id
  INNER JOIN {featuremap} FM 							ON FP.featuremap_id = FM.featuremap_id
  LEFT JOIN {featuremap_dbxref} FD 					ON FP.featuremap_id = FD.featuremap_id
  LEFT JOIN {dbxref} X 								ON FD.dbxref_id = X.dbxref_id
  LEFT JOIN {db} 										ON db.db_id = X.db_id
  INNER JOIN {feature} LG 							ON FP.map_feature_id = LG.feature_id
  LEFT JOIN (
    SELECT FP.feature_id, FP.value
    FROM {featureprop} FP
    WHERE FP.type_id =
      (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'chr_number' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
  ) LGP ON LG.feature_id = LGP.feature_id
  LEFT JOIN (
    SELECT featurepos_id, value
    FROM {featureposprop} FPP
    WHERE FPP.type_id =
      (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
  ) START                                           ON FP.featurepos_id = START.featurepos_id
  LEFT JOIN (
    SELECT featurepos_id, value
    FROM {featureposprop} FPP
    WHERE FPP.type_id =
      (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
  ) STOP 										ON FP.featurepos_id = STOP.featurepos_id
  LEFT JOIN (
    SELECT featurepos_id, value
    FROM {featureposprop} FPP
    WHERE FPP.type_id =
      (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
      )
  ) PEAK 											ON FP.featurepos_id = PEAK.featurepos_id
  LEFT JOIN (
    SELECT F2.name, FR2.subject_id
    FROM {feature} F2
    INNER JOIN {feature_relationship} FR2 		ON FR2.object_id = F2.feature_id
      WHERE FR2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'located_in')
  ) BIN 											ON QTL.feature_id = BIN.subject_id
  LEFT JOIN public.chado_featuremap CF 			ON FM.featuremap_id = CF.featuremap_id
    WHERE QTL.type_id = (SELECT type_id FROM {feature} WHERE feature_id = :feature_id1) AND
      QTL.feature_id = :feature_id2
  ";
  $qtl_map_pos = tripal_map_mainlab_tripal_dbresult_to_array(chado_query($sql, array(':feature_id1' => $feature->feature_id,
    ':feature_id2' => $feature->feature_id)));

  $feature->mainlab_qtl->map_positions = $qtl_map_pos;
}


function tripal_map_preprocess_mainlab_feature_heritable_phenotypic_marker_map_positions(&$variables) {

  $feature = $variables['node']->feature;
  $sql = "
  SELECT FM.featuremap_id, CF.nid AS nid, FM.name AS name, X.accession, DB.urlprefix,
  LG.name AS linkage_group, BIN.name AS bin, LGP.value AS chr,
  START.value AS MTL_start, STOP.value AS MTL_stop, PEAK.value AS MTL_peak,
  MTL.uniquename AS MTL_name
  FROM {feature} MTL
  INNER JOIN {featurepos} FP 					ON MTL.feature_id = FP.feature_id
  INNER JOIN {featuremap} FM 					ON FP.featuremap_id = FM.featuremap_id
  LEFT JOIN {featuremap_dbxref} FD 			ON FP.featuremap_id = FD.featuremap_id
  LEFT JOIN {dbxref} X 						ON FD.dbxref_id = X.dbxref_id
  LEFT JOIN {db} 								ON db.db_id = X.db_id
  INNER JOIN {feature} LG 					ON FP.map_feature_id = LG.feature_id
  LEFT JOIN (
    SELECT FP.feature_id, FP.value
    FROM {featureprop} FP
    WHERE FP.type_id =
      (SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'chr_number' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
  	  )
  ) LGP 										ON LG.feature_id = LGP.feature_id
  LEFT JOIN (
    SELECT featurepos_id, value
      FROM {featureposprop} FPP
      WHERE FPP.type_id = (
        SELECT cvterm_id
        FROM {cvterm}
        WHERE name = 'start' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
  ) START 									ON FP.featurepos_id = START.featurepos_id
  LEFT JOIN (
    SELECT featurepos_id, value
    FROM {featureposprop} FPP
    WHERE FPP.type_id = (
      SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
    )
  ) STOP 										ON FP.featurepos_id = STOP.featurepos_id
  LEFT JOIN (
    SELECT featurepos_id, value
    FROM {featureposprop} FPP
    WHERE FPP.type_id = (
      SELECT cvterm_id
      FROM {cvterm}
      WHERE name = 'mtl_peak' AND cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')
    )
  ) PEAK 										ON FP.featurepos_id = PEAK.featurepos_id
  LEFT JOIN (
    SELECT F2.name, FR2.subject_id
    FROM {feature} F2
      INNER JOIN {feature_relationship} FR2 	ON FR2.object_id = F2.feature_id
        WHERE FR2.type_id = (
        SELECT cvterm_id FROM {cvterm} WHERE name = 'located_in')
  ) BIN 									ON MTL.feature_id = BIN.subject_id
  LEFT JOIN public.chado_featuremap CF 		ON FM.featuremap_id = CF.featuremap_id
    WHERE MTL.type_id = (SELECT type_id FROM {feature} WHERE feature_id = :feature_id1) AND
    MTL.feature_id = :feature_id2
  ";
  $mtl_map_pos = tripal_map_mainlab_tripal_dbresult_to_array(chado_query($sql, array(':feature_id1' => $feature->feature_id,
  ':feature_id2' => $feature->feature_id)));

  $feature->mainlab_mtl->map_positions = $mtl_map_pos;
}


// Convert a database result set into an object array
function tripal_map_mainlab_tripal_dbresult_to_array($dbresult) {
  $arr = array();
  while ($obj = $dbresult->fetchObject()) {
    array_push($arr, $obj);
  }
  
  return $arr;
}

// Link to Tripal v2 node. If node is not available, link to Tripal v3 entity
function tripal_map_mainlab_tripal_link_record ($base_table, $record_id) {
  $link = NULL;
	
  // tripal v2 link (node)
  if (function_exists('chado_get_nid_from_id')) {
    $nid = chado_get_nid_from_id($base_table, $record_id);
    if ($nid) {
      $link = "/node/$nid";
    }
  }
	
  // tripal v3 link (entity)
  if (function_exists('chado_get_record_entity_by_table') && $record_id) {
    $entity_id = chado_get_record_entity_by_table($base_table, $record_id);
    if ($entity_id) {
      $link = "/bio_data/$entity_id";
    }
  }

  return $link;
}


function tripal_map_mainlab_tripal_get_bin_loci($feature_id, $featuremap_id, $limit, $page) {
    $sql = "
      SELECT
        F.uniquename AS LG,
        F.name as linkage_group,
        FR.object_id AS feature_id,
        (SELECT name FROM {feature} WHERE feature_id = FR.object_id) AS genetic_marker,
        F2.name AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position
      FROM {featurepos} FP
      INNER JOIN {feature} F                        ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2                       ON F2.feature_id = FP.feature_id
      INNER JOIN {featureposprop} FPP ON FP.featurepos_id = FPP.featurepos_id AND 
        FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND 
        cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      INNER JOIN {feature_relationship} FR           ON FR.subject_id = F2.feature_id AND 
        FR.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'instance_of' AND 
        cv_id = (SELECT cv_id FROM {cv} WHERE name = 'relationship'))
      WHERE FP.feature_id IN (SELECT 
              subject_id FROM feature_relationship WHERE 
              object_id = :feature_id AND 
              type_id = ( SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND 
              cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) IN ('marker_locus', 'heritable_phenotypic_marker') 
      ORDER BY F.uniquename, FPP.value::float";
    
    $positions = tripal_map_mainlab_tripal_pager_query ($sql, array(':feature_id' => $feature_id, ':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
    return $positions;
}

function tripal_map_mainlab_tripal_get_bin_QTL($feature_id, $featuremap_id, $limit, $page) {
    $sql = "
      SELECT
        F.uniquename AS LG,
        F.name as linkage_group,
        (SELECT first (object_id) FROM {feature_relationship} WHERE 
          subject_id = F2.feature_id AND 
          type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))) AS feature_id,
        F2.uniquename AS marker,
        F2.feature_id AS marker_feature_id,
        (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) AS type,
        FPP.value AS position,
        FPP2.value AS stop,
        FPP3.value AS peak
      FROM {featurepos} FP
      INNER JOIN {feature} F ON F.feature_id = FP.map_feature_id
      INNER JOIN {feature} F2 ON F2.feature_id = FP.feature_id
      LEFT JOIN {featureposprop} FPP              ON FP.featurepos_id = FPP.featurepos_id AND 
        FPP.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'start' AND 
              cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP2             ON FP.featurepos_id = FPP2.featurepos_id AND 
        FPP2.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'stop' AND 
              cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'))
      LEFT JOIN {featureposprop} FPP3             ON FP.featurepos_id = FPP3.featurepos_id AND 
        FPP3.type_id = (SELECT cvterm_id FROM {cvterm} WHERE name = 'qtl_peak' AND 
             cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN')) WHERE FP.feature_id IN (
               SELECT subject_id 
               FROM feature_relationship WHERE 
                 object_id = :feature_id AND 
                 type_id = (
                    SELECT cvterm_id FROM chado.cvterm WHERE name = 'contained_in' AND 
                 cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'relationship')))
      AND FP.featuremap_id = :featuremap_id
      AND (SELECT name FROM {cvterm} WHERE cvterm_id = F2.type_id) = 'QTL'";
    $positions = tripal_map_mainlab_tripal_pager_query ($sql, array(':feature_id' => $feature_id,':featuremap_id' => $featuremap_id), $limit, $page, TRUE);
    return $positions;
}


// Initialize pager to 1 or get current page from URL
function tripal_map_mainlab_tripal_get_pager($total, $num_per_page = 25, $tripal_pane = '') {
    $page = 1;
    // calculate total pages
    $pages = (int) ($total / $num_per_page);
    // get current page from URL
    if ($total % $num_per_page != 0) {
        $pages ++;
    }
    $url = explode('?', request_uri());
    $path = $url[0];
    if (preg_match('/page=(\d+)/', $url[1], $matches)) {
        $page = $matches[1];
    }
    if ($page > $pages) {
        $page = $pages;
    }
    if ($page < 1) {
        $page = 1;
    }
    // Create pager HTML
    if ($tripal_pane) {
        $pane = 'pane=' . $tripal_pane;
    }
    $pager = '';
    if ($pages > 0) {
        $pager = "<div style=\"float:left; margin:10px 5px;\">Page</div>";
    }
    $counter = 0;
    $window = 14;
    if ($page - ($window / 2) > 1) {
        $pager .= "<div style=\"padding: 5px 10px;float:left;border:solid 1px #AAA;margin:3px;border-radius: 10px;\"><a href=\"$path?$pane&page=1\">First</a></div><div style=\"float:left; margin:10px 5px;\">...</div>";
    }
    for ($i = 1; $i <= $pages; $i ++) {
        if ($i >= $page - ($window / 2) && $counter <= $window) {
            if ($page != $i) {
                $pager .= "<div style=\"padding: 5px 10px;float:left;border:solid 1px #AAA;margin:3px;border-radius: 10px;\"><a href=\"$path?$pane&page=$i\">$i</a></div>";
                $counter ++;
            }
            else {
                $pager .= "<div style=\"padding: 5px 10px;float:left;border:solid 1px #AAA;margin:3px; color:#CCC; border-radius:10px\">$i</div>";
                $counter ++;
            }
        }
    }
    if ($page + ($window / 2) < $pages - 1) {
        $pager .= "<div style=\"float:left; margin:10px 5px;\">...</div><div style=\"padding: 5px 10px;float:left;border:solid 1px #AAA;margin:3px;border-radius: 10px;\"><a href=\"$path?$pane&page=$pages\">Last</a></div>";
    }
    
    return array('page' => $page, 'pages' => $pages, 'pager' => $pager);
}

// Return array of results objects for paging
function tripal_map_mainlab_tripal_pager_query($sql, $args = array(), $limit = 'ALL', $page = 0, $chado = FALSE) {
    $offset = $page * $limit;
    $sql .= " LIMIT $limit" . " OFFSET $offset";
    $results = NULL;
    if ($chado) {
        $results = chado_query($sql, $args);
    }
    else {
        $results = db_query($sql, $args);
    }
    $data = array();
    while ($obj = $results->fetchObject()) {
        $data [] = $obj;
    }
    return $data;
}

