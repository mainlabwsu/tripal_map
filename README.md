[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1407218.svg)](https://doi.org/10.5281/zenodo.1407218)

#  Contents 
 * Introduction
 * Requirements
 * Version
 * Download
 * Installation
 * Configuration
 * Use Cases
 * Problems/Suggestions
 

# Introduction
Tripal MapViewer is a Drupal module that provides interactive visualization of genetic maps.
The Tripal MapViewer module extends the Tripal suite of Drupal modules and can be added to any 
website that uses Tripal, where genetic map visualization functionality is desired.

Representing the next generation of the existing CMap tool, Tripal MapViewer is a browser based
tool for visualizing comparison of genetic maps from numerous species, and curating
map data. 

The Tripal MapViewer provides a modernized user interface to display chromosomes of
different species and view pair-wise comparisons of known correspondences of genetic
map features. The Tripal MapViewer module integrates with Drupal websites using the Tripal API and Chado
tables from a PostgreSQL database.  


# Requirements
* Drupal 7.x
* Tripal 7.x-2.x or Tripal 7.x-3.x
* PHP 7.3 or newer 

## Install Drupal

Please refer to this [page](http://tripal.info/tutorials/v2.x/installation/drupal) to install Drupal.

## Install Tripal 

Please refer to this [page](https://https://www.drupal.org/project/tripal) and 
[here](http://tripal.info/tutorials/v2.x/installation/tripal) to install Tripal v2 or
[here](http://tripal.info/tutorials/v3.x/installation/tripal) to install Tripal v3.


# Version

2.0.0


# Download

The Tripal MapViewer module can be downloaded from GitLab:
https://gitlab.com/mainlabwsu/tripal_map


# Installation

1. Install the tripal_map module on your Tripal site

   ![Materialized Views](images/MaterializedViews_Page1.png)

2. Populate the Materialized views:

    * Go to the Tripal menu. In Tripal v2 choose the Chado Schema option. In Tripal v3 choose Data Storage and then Chado. Select Materialized Views.
    * On installation of this module, drush jobs are automatically created to populate each of the four materialized views in the correct order, but if you edit them, follow the following procedure to repopulate them.
    * In the Materialized Views page, find the tripal\_map\_genetic\_markers\_mview materialized view and select "populate" in the right column
    * Populate the tripal\_map\_qtl\_and\_mtl\_mview also.
    * To use the genome comparison view when genome data is present, populate the tripal\_map\_gene\_sequence\_mview and tripal\_map\_genome\_correspondences\_mview. 
    * Selecting "populate" submits the job to generate the materialized views. Populate the materialized views in the specified order to account for dependencies between them. 
    * Use drush to manually run the job to populate the materialized views:
    ```
    cd /var/www/html
    drush trp-run-jobs --user=administrator
    ```
    * Refresh the Materialized Views page and the materialized views should show a status of "populated".


3. Editing the Materialized Views:
    * Depending on your specific Chado table naming scheme, it may be necessary to edit certain parts of the materialized views. 
    * To do this go to Materialized Views page and choose the Edit option beside the tripal\_map\_genetic\_markers\_mview materialized and tripal\_map\_qtl\_and\_mtl\_mview. If you have genome data and wish to use the genome comparison view feature, edit the tripal\_map\_gene\_sequence\_mview and tripal\_map\_genome\_correspondences\_mview.
    * You should not need to modify the schema, but in the query you may change the table names if they differ from your Chado naming scheme.
    * For example, it is likely that in the tripal\_map\_genetic\_markers\_mview materialized view, in this statement on line cv_id = (SELECT cv_id FROM {cv} WHERE name = 'MAIN'), the name of the controlled vocabulary (cv) will be something different than 'MAIN'.

4. Clear the Drupal Cache after populating the Materialized views. Do this after initial install and each time the Materialized views are updated to ensure proper creation and synchronization of cached data. 

## Please Note:
1. On uninstall, any changes made to the materialized views will be deleted. 
2. When data in the Chado tables is updated, those changes are reflected by Tripal MapViewer only after re-populating 
the materialized views and clearing the Drupal cache. Clear the Drupal cache either with the Drupal administration interface:

```
Administration > Configuration > Development > Performance,
and click the button "Clear all caches"
```

Or from the command line in Drupal 7 and earlier:

```
drush cache-clear all 
```
 
# Configuration

For Tripal version 3, Tripal MapViewer offers two fields that can be added and configured: the MapViewer Overview field and the MapViewer Marker Map Positions field:

## MapViewer Overview 

To add the Map Overview field to the Map page, visit the Structure option in the Drupal Administration and select the Tripal Content Types options. Select the Map content type:

![Map Tripal Content Type](images/MapViewer_Field_Map_Tripal_Content_Type.png)

In the Map content type page, add a new field by specifying the MapViewer Overview field type:

![MapViewer Overview Field Map](images/MapViewer_Overview_Field_Map_Tripal_Content_Type.png)

Create the MapViewer Overview field by completing the settings form as specified here:

![MapViewer Overview Field](images/MapViewer_Overview_Field.png)

## MapViewer Marker Map Positions

To add the Marker Map Positions field to the Genetic Marker, Heritable Phenotypic Marker or QTL markers, visit the Structure option in the Drupal Administration and select one of these Tripal Content Types:

![Marker Tripal Content Type](images/MapViewer_Marker_MapPositions_Field_Marker_Tripal_Content_Types.png)

Add a new MapViewer Marker Map Positions field in one of these marker content type pages and complete the settings form as specified here:

![MapViewer Marker Map Positions](images/MapViewer_Marker_MapPositions_Field.png)  


To configure aspects of Tripal MapViewer, visit the Administration page by going to the Tripal
menu, selecting the Extensions option and then choosing MapViewer.

![Admin Page](images/MapViewer_Admin_Page1.png)


## General Section
In the General section, the Tutorial Links manipulate the link values at the top of the MapViewer page. 
Changes in the Video and Text tutorial fields here in this dialog will be dynamically reflected in 
MapViewer Tutorial links.

![Tutorial Links](images/Tutorial_Links.png)


Updating the sample links will reflect in the MapViewer Quick Start page, available at https://yoursite.org/MapViewer

![Tutorial Links](images/MapViewer_QuickStart_SampleLinks4.png)


## Toolbar Section
In the Toolbar section there are two settings, the Linkage Group Display and the Organism Selector Display Preferences.

### Linkage Group Display
The Linkage Group Display section controls the setting for whether or not marker names for 
QTL and heritable phenotypic markers are abbreviated. The abbreviated names appear beside the linkage group, 
and the full name appears on the marker label popup on the top right.

![Toolbar Marker Name Abbreviation](images/Toolbar_MarkerNameAbbrev.png)

### Organism Selector Display Preference
The Organism Selector Display Preference specifies whether the common name or the genus and species name will be used to refer to the Organisms shown in the toolbar selector list. Here is an example of the Genus and Species:

![Organism Selector](images/Organism_Selector_Display_GenusSpecies2.png)

the Common Name:

![Organism Selector](images/Organism_Selector_Display_CommonName2.png)

Or both the Common Name and the Genus and Species:

![Organism Selector](images/Organism_Selector_Display_CommonName_and_GenusSpecies2.png)

### Genome View

The Show genome view display check box determines whether or not the genome toolbar is displayed. If genome data is present, select this checkbox to show the genome toolbar in the control panel below the linkage group display on the main MapViewer page.

## Chado Section 
In the Chado section there are two settings, Feature Position Type and Marker Correspondence Name Type

### Feature Position Type
The Feature Position Type region assigns the marker position names for start, stop and qtl_peak position and they
can be customized here if a different terminology is used by the marker fields. The names in these 
fields are accessed dynamically and used in the code that determines the positioning of markers on the linkage groups.

### Marker Correspondence Name Type
The Marker Correspondence Name Type specifies the name marker correspondences will be identified by. This can either be the marker locus name or the genetic marker name.

### In the Tools section the Map Overview Display 
can be customized to exclude specific maps from display using a semi-colon delimited list of map names. 

# Use Cases

## Use Case 1: 
View the map from the Map Overview

![Map Overview](images/Map_Overview1.png)

Specify the map id in the URL path, for example, https://yoursite.org/featuremap/439 or navigate to it from a 
Drupal node.

## Use Case 2: 
View a linkage group from the MapViewer

![View Linkage Group](images/View_Linkage_Group1.png)

Click on one of the glyphs in the Map Overview page or specify the map id in the URL path, 
and optional linkage group and marker id. For example, https://yoursite.org/mapviewer/200/LG3/1412994, to open the MapViewer.

## Use Case 3: 
1. Configure a linkage group from MapViewer:
    * Select an organism from the organism selector list, wait until the map and linkage group selection list options are updated as dependencies.
    
    ![Organism Selector](images/Reference_Organism_list1.png)
    
    * or map from the map selector list, and wait until the linkage group selection list options are updated
    
    ![Configure Linkage Group Map](images/Reference_map_list1.png)
    
    * or linkage group from the linkage group selector list.
     
    ![Configure Linkage Group LG](images/Reference_linkage_group_list1.png)

## Use Case 4: 
View correspondences between two linkage groups in the MapViewer:

Select the "Show Comparison Map" checkbox. Two linkage groups will be drawn, the reference linkage group on the left, 
and the comparison linkage group on the right. Scroll the zoomed section on each linkage group to view various areas 
of each linkage group and adjust the orientation of the correspondences. 

![Marker Correspondence](images/Marker_Correspondences2.png)

## Use Case 5:
View marker correspondences between a genetic map and a genome in the MapViewer:

Select the "Show Genome View" checkbox. A linkage group will be drawn on the left and genome chromosome/scaffold on the right.

![Control Panel Genome](images/MapViewer_ControlPanelGenomeV.png)

The left lane of the genome chromosome displays genetic markers and the blue horizontal bars on the right indicate 
the density of genes and mRNA on the genome. To reverse the vertical orientation of the chromosome, select the 
"Invert scaffold" checkbox in the control panel. The genetic markers and genes on the chromosome hyperlink to 
JBrowse where there specific region on the chromosome is displayed.

Scroll the zoomed section on the linkage group on the left to view genetic marker correspondences with the genome chromosome. 
Zoom on the left to expose further details for the linkage group along with correspondences, to focus on specific relationships of genetic markers in common between the linkage group and chromosome/scaffold. 

![Genome Correspondence](images/MapViewer_GenomeCorrespondences.png)


## Use Case 6: 
Change color of markers in the MapViewer. In the Marker color toolbar, select a marker type, then a color and press Submit.
To default to original settings, click the Reset button.

![Marker_Color](images/Set_Marker_Color1.png)

## Use Case 7: 
Change marker type visibility in the MapViewer. In the Marker visibility toolbar, select one or more marker types, 
choose to Show or Hide the marker type(s) and press Submit. To default to original settings, press Reset.

![Marker_Visibility](images/Set_Marker_Visibility1.png)


# Problems/Suggestions
Tripal MapViewer is under active development. For questions or bug reports, please contact the 
developers at the Main Bioinformatics Lab by email: dev@bioinfo.wsu.edu

## Citation
If you use Tripal MapViewer, please cite:

[Buble K, Jung S, Humann JL, Yu J, Cheng CH, Lee T, Ficklin SP, Hough H, Condon B, Staton ME, Wegrzyn JL, Main D. Tripal MapViewer: A tool for interactive visualization and comparison of genetic maps. Database (Oxford). 2019 Jan 1;2019:baz100.] doi: 10.1093/database/baz100. PMID: 31688940; PMCID: PMC6829499



