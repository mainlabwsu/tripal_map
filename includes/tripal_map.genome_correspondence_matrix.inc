<?php

/**
 * @file
 *	 Create form that displays genome correspondence matrix
 *	 Draw the Genome Correspondence Matrix page displaying a table of correspondences between the given
 *	 genome, and the genetic maps 
 *	 Toolbar selects the genome to compare against genetic maps 
 * 
 * @ingroup tripal_map
 */


/**
 * Implements hook_form().
 *
 * function: tripal_map_genomes_correspondence_matrix_form
 *   Creates the form to display a genome-to-genetic map correspondences matrix. 
 *   Called by URL for a specific route based on the map given parameters
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $featuremap_prim_id
 * @param $analysis_id
 *  
 * @return - A form array for the genome correspondences matrix form.
 *    
 * @ingroup tripal_map_includes
 */
function tripal_map_genomes_correspondence_matrix_form($form, &$form_state, $featuremap_prim_id = NULL, $analysis_id = NULL) {
    
  // if there is no genome supplied do not build the form
  if (!isset($form_state['build_info']['args'][0])) {
    return drupal_not_found();
  }
  
  if ((!isset($featuremap_prim_id) || strlen($featuremap_prim_id) == 0) ||
    (!isset($analysis_id) || strlen($analysis_id) == 0)) {
    return drupal_not_found();
  }
  
  // Generate the genome selector lists data for the correspondence matrix.
  if (!array_key_exists('storage', $form_state)) {
    $form_state['storage'] = array();
  }
  
  $select_options = tripal_map_get_genome_select_list_options();
  
  // add the JS files
  tripal_map_load_correspondence_matrix_js($form, $form_state);
  tripal_map_update_storage($form_state, 'flag_update_js_maps', FALSE, FALSE);
  
  // Enable to show selector menus, only if genetic map comparison 
  // Initialize toolbars and form state storage
  // set the update maps flag to false, if it does not exist yet.
  tripal_map_form_cm_genome_bar_init($form, $form_state, $select_options, $featuremap_prim_id, $analysis_id);
  
  tripal_map_form_correspondence_matrix_genome_bar($form, $form_state, $select_options, $featuremap_prim_id, $analysis_id);
  
  // add refmap, compmap and corres to JS  
  $features_corres_matrix = tripal_map_create_cm_genomes_JS_params($featuremap_prim_id, $analysis_id);
  //$features_corres_matrix = {rows: [type: refmap,  name: $ref_map_name, lgs: [lg1, lg2, ...]}, cols: {type: compmap,  name: $comp_map_name, lgs: [lg1, lg2, ...]}, corresp: [[1,2,...],[2,3,...],...]}
  $js_setting_corres_matrix = 'mapGenomesCorrespondenceMatrixJS';
  if (array_key_exists('flag_update_js_maps', $form_state['storage'])) {
      $form_state['storage']['flag_update_js_maps'] = TRUE;
  }
  // assign the correspondence matrix parameters to the JS client
  if ($form_state['storage']['flag_update_js_maps'] == TRUE) {
    tripal_map_update_drupal_add_js_params($js_setting_corres_matrix, NULL, NULL, NULL, $features_corres_matrix);
    $form_state['storage']['flag_update_js_maps'] = FALSE;
  }
  
  // attach JS library files
  $form = tripal_map_attach_d3_lib($form);
  $form['#attached']['js'][] = drupal_get_path('module', 'tripal_map') . '/theme/js/libraries/FileSaver.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'tripal_map') . '/theme/js/libraries/canvas-to-blob.min.js';
  
  // form the container where the correspondence matrix will be drawn
  $form['select'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => array('select_fieldset_correspondence_matrix'), // JS looks for this id to draw the matrix
      'class' => array("TripalMap")),
    '#collapsible' => FALSE,
  );

  return $form;
}

/**
 *
 * function: tripal_map_create_cm_genomes_JS_params
 *   Prepare genome correspondence matrix settings to provide to JS.
 *
 * @param $featuremap_prim_id 
 * @param $analysis_id
 *
 * @return - Feature array for the genome correspondence matrix 
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_cm_genomes_JS_params($featuremap_prim_id, $analysis_id) {

  $features_corres_matrix = array();

  // rows
  $features_corres_matrix['rows'] = array();
  $features_cm_rows = &$features_corres_matrix['rows'];
  $features_cm_rows['type'] = 'ref_organism';
  $features_cm_rows['maps']['feature_ids'] = array();
  $features_cm_rows['maps']['names'] = array(); 
  $features_cm_rows['name'] = ''; 
  
  // cols
  $analysis_name = "";
  $genome_scaffolds = array();
  
  // correspondences
  $features_corres_matrix['correspondences'] = array();
  $features_cm_matrix = &$features_corres_matrix['correspondences'];
  
  // each row in the result set represents a cell in the matrix grid: the number of correspondences for a specific map-to-analysis scaffold pairing
  // this does not address the zero entries, where no correspondence exists between a map and any scaffold.
  // obtain all the scaffolds in the analysis, even those without correspondences with genetic map(s). Query return the scaffold names in sorted order.
  $analysis_scaffolds_sql = "SELECT distinct(analysis_srcfeature_name) FROM {tripal_map_gene_sequence_mview} 
    WHERE analysis_id = :analysis_id ORDER BY analysis_srcfeature_name ASC";
  $args = array();
  $args[':analysis_id'] = $analysis_id;
  // find the number of scaffolds in the analysis, and their names. The analysis and their scaffolds are listed in the 
  // tripal_map_gene_sequence_mview materialized view table. obtain the alphabetical list.

  $analysis_scaffolds_res = chado_query($analysis_scaffolds_sql, $args);
  $analysis_scaffolds_num = $analysis_scaffolds_res->rowCount();
  
  // obtain all maps having correspondences with the analysis. Query returns the list of map names in sorted order.
  $corres_maps_sql = "
    SELECT distinct(map_name), c.map_id as map_id, genus as map_genus, analysis_genus  FROM (
       SELECT genus, map_id from {tripal_map_genetic_markers_mview} a
       UNION
       SELECT genus, map_id  from  {tripal_map_qtl_and_mtl_mview} b
     ) as c
     INNER JOIN {tripal_map_genome_correspondences_mview} GC   ON GC.map_id = c.map_id
     WHERE GC.analysis_id = :analysis_id
     ORDER BY map_name ASC";

  $corres_maps_res = chado_query($corres_maps_sql, $args);
  $corres_maps_num = $corres_maps_res->rowCount();

  while ($corres_maps_res && ($corres_maps_obj = $corres_maps_res->fetchObject())) {
    // check the genetic map genus matches that of the genome and cache the map_ids
    if (is_object($corres_maps_obj)) {
      $map_id = '';
      if (property_exists($corres_maps_obj, 'map_id')) {
        $map_id = $corres_maps_obj->map_id;
      }
      $analysis_genus = '';
      if (property_exists($corres_maps_obj, 'analysis_genus')) {
          $analysis_genus = $corres_maps_obj->analysis_genus;
      }
      $map_genus = '';
      if (property_exists($corres_maps_obj, 'map_genus')) {
          $map_genus = $corres_maps_obj->map_genus;
      }
      $match = tripal_map_organism_genome_cm_match($map_id, $analysis_genus, $map_genus);
      if ($match) {
        if (property_exists($corres_maps_obj, 'map_name')) {
          $map_name = $corres_maps_obj->map_name;
          array_push($features_cm_rows['maps']['names'], $map_name);
        }
        if (property_exists($corres_maps_obj, 'map_id')) {
          array_push($features_cm_rows['maps']['feature_ids'], $map_id);
          // create a zero-fill array for each map entry in the features cm matrix, the length of the number of scaffolds in the analysis.
          $features_cm_matrix[$map_id] = array_fill(0, $analysis_scaffolds_num, 0);
        }
      }
    }
  }
  
  // create a hash index lookup table for the scaffold in the array of sorted scaffolds, then place the corres number 
  // at the position of the lg array. sort
  $analysis_scaffolds = $analysis_scaffolds_res->fetchCol();
  tripal_map_customLgSort($analysis_scaffolds);
  // copy the array values to a fresh array with indices ordered from 0
  $fresh_array = array_values($analysis_scaffolds);
  // then array flip the order, i.e. keys from array become values and values from array become keys.
  $hash_scaffolds = array_flip($fresh_array);
  $genome_scaffolds = array_keys($hash_scaffolds);

  // obtain the number of correspondence markers between each reference map and analysis scaffold, aggregating over all map linkage groups
  $maps_and_genome_corres_sql = "SELECT map_id, analysis_srcfeature_name, analysis_name, SUM(num_genetic_marker_corres) as num_genetic_marker_corres
    FROM {tripal_map_genome_correspondences_mview} WHERE analysis_id = :analysis_id
    GROUP BY map_id, analysis_srcfeature_name, analysis_name ORDER BY map_id, analysis_srcfeature_name ASC";

  $maps_and_genome_corres_res = chado_query($maps_and_genome_corres_sql, $args);
  while ($maps_and_genome_corres_res && ($maps_and_genome_corres_obj = $maps_and_genome_corres_res->fetchObject())) {
    
    // iterate through all correspondences between map and a scaffold, indexed by map.
    if ( is_object($maps_and_genome_corres_obj)) {
      if (property_exists($maps_and_genome_corres_obj, 'analysis_name')) {
        $analysis_name = $maps_and_genome_corres_obj->analysis_name; 
      }
      $map_id = "";
      if (property_exists($maps_and_genome_corres_obj, 'map_id')) {
        $map_id = $maps_and_genome_corres_obj->map_id;
      }
      if (array_key_exists($map_id, $features_cm_matrix)) {
        // lookup the position in the linkage group corres matrix array that the scaffold number of correspondences will populate
        if (property_exists($maps_and_genome_corres_obj, 'analysis_srcfeature_name')) {
          $scaffold_name = $maps_and_genome_corres_obj->analysis_srcfeature_name;
          $scaffold_index = 0;
          if (array_key_exists($scaffold_name, $hash_scaffolds)) {
            $scaffold_index = $hash_scaffolds[$scaffold_name];
          }
          // check the map identifier already exists in the features_cm_matrix
          // only maps with genus matching the genome genus are in the features_cm_matrix
          if (property_exists($maps_and_genome_corres_obj, 'num_genetic_marker_corres')) {
            $features_cm_matrix[$map_id][$scaffold_index] = $maps_and_genome_corres_obj->num_genetic_marker_corres;
          }
        }
      }
    }
  }
  $features_cm_matrix = array_values($features_cm_matrix);  
  
  // comparison analysis genome
  $features_corres_matrix['cols'] = array();
  $features_cm_cols = &$features_corres_matrix['cols'];
  $features_cm_cols['type'] = 'compgenome';
  $features_cm_cols['name'] = $analysis_name;
  $features_cm_cols['feature_id'] = $analysis_id;
  $features_cm_cols['lgs'] = $genome_scaffolds;
  $features_cm_cols['analysis_link'] = tripal_map_get_analysis_entity_page_path($analysis_id);

  return $features_corres_matrix;

}


/**
 * function: tripal_map_form_cm_genome_bar_init
 *	 Initialize the correspondence matrix genome toolbar, using trigger values if available.
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_cm_prim_id
 * @param $analysis_id
 *  
 * @return - The form array for the Genome Correspondence Matrix form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_cm_genome_bar_init( &$form, &$form_state, $select_options, &$featuremap_cm_prim_id, &$analysis_id) {

  if (array_key_exists('storage', $form_state)) {
    // the form was updated, but the url is not up to date yet, obtain previous value from form state storage
    // use the map and analysis values from the form state
    if (array_key_exists('featuremap_cm_prim_id', $form_state['storage'])) {
      $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
    }
    if (array_key_exists('analysis_compkey', $form_state['storage'])) {
      $analysis_id = $form_state['storage']['analysis_compkey'];
    }
  }
    
  tripal_map_init_cm_genome_selectors_to_storage($form_state, $select_options, $featuremap_cm_prim_id, $analysis_id);
  
  $options_analysis = $select_options['options_analysis'];
  $fs_storage =& $form_state['storage'];
          
  if (tripal_map_get_form_trigger($form_state) == "select_analysis_cm") {
    tripal_map_update_storage($form_state, "analysis_compkey", $form_state['triggering_element']['#value'], TRUE);
  }
  
  $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
  $analysis_id = $form_state['storage']['analysis_compkey'];

  return $form;
}


/**
 *
 * function: tripal_map_init_cm_genome_selectors_to_storage
 *	 Initialize form state storage variables for selectors
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_prim_id
 * @param $analysis_id
 * 
 * @ingroup tripal_map_includes
 */
function tripal_map_init_cm_genome_selectors_to_storage(&$form_state, $select_options, $featuremap_prim_id, $analysis_id) {

  $options_analysis = $select_options['options_analysis'];
  
  $fs_storage =& $form_state['storage'];
  if (!(array_key_exists('storage', $form_state))) {
    // the storage key does not exist in form state
    $fs_storage = array();
  }

  if (!is_array($fs_storage)) {
    // the form state is initialized to null, by Drupal. Assign it to type array.
    $fs_storage = array();
  }

  $submit = FALSE;
  if (tripal_map_get_form_trigger($form_state) == 'Submit_Genome_Comparison') {
    $submit = TRUE;
  }
      
  // set the keys for the select options arrays for featuremap
  if (!(array_key_exists('featuremap_cm_prim_id', $fs_storage))) {
      // take the passed in parameter as default
      tripal_map_update_storage($form_state, 'featuremap_cm_prim_id', $featuremap_prim_id, FALSE);
  }
  
  // set the keys for the select options arrays for analysis
  if (!$submit) {
      if (is_array($options_analysis)) {
          // take the first analysis as default and write it to storage
          tripal_map_update_storage($form_state, 'analysis_compkey', array_key_first($options_analysis), TRUE);
      }
      if (isset($analysis_id) && strlen($analysis_id)) {
          // analysis_id value is previously set in form state storage, defer to analysis_id value passed in by the caller
          tripal_map_update_storage($form_state, 'analysis_compkey', $analysis_id, TRUE);
      }
  }
  
}


/**
 * function: tripal_map_form_correspondence_matrix_genome_bar
 *	 Create the reference toolbar with selectors for whole genome
 * 
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_cm_prim_id
 * @param $analysis_id
 *
 * @return - The form array for the Genome Correspondence Matrix form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_correspondence_matrix_genome_bar(&$form, &$form_state, $select_options, $featuremap_cm_prim_id, $analysis_id) {

  $fs_storage =& $form_state['storage'];
    
  $options_analysis = $select_options['options_analysis'];
  $analysis_key = $fs_storage['analysis_compkey'];
  $analysis_name = $options_analysis[$analysis_key];
    
  $form['correspondence_matrix_genome'] = array(
    '#type' => 'container',
  	'#prefix' => '<div id="select_correspondence_matrix" class="TripalMap">',
  	'#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['correspondence_matrix_genome']['analysis'] = array(
    '#type' => 'container',
  	'#prefix' => '<div id="select_analysis_cm" class="TripalMap">',
  	'#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
   
  $form['correspondence_matrix_genome']['analysis']['select_analysis_cm'] = array(
    '#type' => 'select',
    '#title' => t('Whole Genome'),
  	'#prefix' => '<div id="select_analysis_cm" class="form-item">',
  	'#suffix' => '</div>',
  	'#ajax' => array(
      'wrapper' => 'show-genome-correspondence_matrix-form',
      'callback' => 'tripal_map_show_org_map_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
      '#options' => $options_analysis,
      '#multiple' => FALSE,
      '#default_value' => $analysis_key,
  );
  
  $form['correspondence_matrix_genome']['analysis']['button_cm-submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
    '#name' => ('Submit_Genome_Comparison'),
    '#attributes' => array(
      'id' => 'cm-genome-submit'
    ),
    '#prefix' => '<div class="cm-genome-submit-button">',
    '#suffix' => '</div>',
    '#submit' => array('tripal_map_submit_cm_genome_rebuild_form_state'),
    '#after_build' => array('tripal_map_load_correspondence_matrix_js'),
  );

  $form['#prefix'] = '<div id="show-genome-correspondence_matrix-form">';
  $form['#suffix'] = '</div>';

  return $form;

}


/**
 * function: tripal_map_submit_cm_genome_rebuild_form_state
 *	 Rebuild the form state, create features from newly selected analysis and pass them to JS
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_submit_cm_genome_rebuild_form_state($form, &$form_state) {

  $form_state['rebuild'] = TRUE;
  tripal_map_update_storage($form_state, 'flag_update_js_maps', TRUE, TRUE);

}
