<?php

/**
 * @file
 *	 Create map comparison toolbars for select organism, map and linkage group for the MapViewer forms.
 *
 * @ingroup tripal_map
 */


/**
 * function: tripal_map_init_organism_comparison_selectors_to_storage
 *	 Initialize form state storage variables for comparison selectors
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options_corres 
 * @param $featuremap_id_comparison
 * @param $linkage_group_id_comparison
 *
 * @return - Update the form_state comparison selector values
 * 
 * @ingroup tripal_map_includes
 */
function tripal_map_init_organism_comparison_selectors_to_storage(&$form_state, $select_options_corres, 
    $featuremap_id_comparison, $linkage_group_id_comparison) {

  $map_org_comp = $select_options_corres['map_org'];

  $fs_storage =& $form_state['storage'];
  if (!(array_key_exists('storage', $form_state))) {
    // the storage key does not exist in form state
    $fs_storage = array();
  }

  if (!is_array($fs_storage)) {
    // the form state is initialized to null, by Drupal. Assign it to type array.
    $fs_storage = array();
  }

  $submit = FALSE;
  if ((tripal_map_get_form_trigger($form_state) == 'Submit_Comparison') || 
    (tripal_map_get_form_trigger($form_state) == 'Submit_Reference')) {
    $submit = TRUE;
  }

  // by default, do not show comparison. set to false if flag does not exist.
  tripal_map_update_storage($form_state, 'show_comp', FALSE, FALSE);
  
  // take the first organism as default
  if (!$submit) {
    tripal_map_update_storage($form_state, 'org_compkey', key(tripal_map_get_elem($select_options_corres, 'options_org')), TRUE);
    if ((isset($featuremap_id_comparison) && strlen($featuremap_id_comparison) > 0 ) ) {
      // if featuremap id is available, lookup the associated organism and use that as default
      tripal_map_update_storage($form_state, 'org_compkey', tripal_map_get_organism_id($map_org_comp, $featuremap_id_comparison), TRUE);
    }
  }

  // use the first map as default
  if (!$submit) {
    tripal_map_update_storage($form_state, 'maps_compkey', key($select_options_corres['options_map'][$fs_storage['org_compkey']]), TRUE);
    if (isset($featuremap_id_comparison) && strlen($featuremap_id_comparison)) {
      // set the default map to the id passed by featuremap_id_comparison
      tripal_map_update_storage($form_state, 'maps_compkey', $featuremap_id_comparison, TRUE);
    }
  }

  if (!$submit) {
    // use the first chromosome as a default
    tripal_map_update_storage($form_state, 'chr_compkey', key($select_options_corres['options_chr'][$fs_storage['org_compkey']][$fs_storage['maps_compkey']]), TRUE);
    if ((isset($featuremap_id_comparison) && strlen($featuremap_id_comparison) > 0 ) &&
      (isset($linkage_group_id_comparison) && strlen($linkage_group_id_comparison) > 0 )) {
          tripal_map_update_storage($form_state, 'chr_compkey', $linkage_group_id_comparison, TRUE );
    }
  }
  
}


/**
 * function: tripal_map_form_organism_comparison_bar_init
 *	 Initialize data structures for the comparison toolbar
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options_corres
 * @param $featuremap_id_comparison 
 * @param $linkage_group_name_comparison
 * @param $features_comparison
 * @param $show_comparison_checked
 * @param $show_comparison_enabled
 *
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_organism_comparison_bar_init( &$form, &$form_state, $select_options_corres, &$featuremap_id_comparison, 
  &$linkage_group_name_comparison, &$features_comparison, $show_comparison_checked, $show_comparison_enabled) {
  
  $fs_storage =& $form_state['storage'];
  
  // check if there was a trigger in the reference toolbar
  $ref_trigger = FALSE; 
  if ((tripal_map_get_form_trigger($form_state) == 'select_organism') || 
    (tripal_map_get_form_trigger($form_state) == 'select_map') ||
    (tripal_map_get_form_trigger($form_state) == 'select_chromosome')) {
    $ref_trigger = TRUE;
  }

  $submit = FALSE;
  if ((tripal_map_get_form_trigger($form_state) == 'Submit_Comparison') && 
    (tripal_map_get_form_trigger($form_state) == 'Submit_Reference')) {
    $submit = TRUE;
  }
  
  // by default pick the first element of each select list
  $org_id = key(tripal_map_get_elem($select_options_corres, 'options_org'));
  $featuremap_id_comparison = "";
  if (is_array($select_options_corres['options_map'][$org_id])) {
    $featuremap_id_comparison = key($select_options_corres['options_map'][$org_id]);
  }
  
  if (array_key_exists('maps_compkey', $form_state['storage']) && !$ref_trigger) {
    $featuremap_id_comparison = $fs_storage['maps_compkey'];
  }
  
  $linkage_group_id_comparison = "";
  if (!$ref_trigger && !$submit) {
    if (array_key_exists($featuremap_id_comparison, $select_options_corres['options_chr'][$org_id])) {
      if (is_array($select_options_corres['options_chr'][$org_id][$featuremap_id_comparison])) {
        $linkage_group_id_comparison = key($select_options_corres['options_chr'][$org_id][$featuremap_id_comparison]);
      }
    }
    if (array_key_exists('chr_compkey', $form_state['storage'])) {
      $linkage_group_id_comparison = $fs_storage['chr_compkey'];
    }
  } 
    
  // initialize the values for select lists for organism, map and linkage group, if none are
  // currently set. None are set if form_state storage for these is uninitialized.
  tripal_map_init_organism_comparison_selectors_to_storage($form_state, $select_options_corres, 
      $featuremap_id_comparison, $linkage_group_id_comparison);

  $org_compkey =& $fs_storage['org_compkey'];
  $maps_compkey =& $fs_storage['maps_compkey'];
  $chr_compkey =& $fs_storage['chr_compkey'];
  
  if (!$submit) {
    // if the reference map is updated, then set the comparison map to new defaults correspondingly 
    tripal_map_update_storage($form_state, 'org_comp', tripal_map_get_elem($select_options_corres, 'options_org'), TRUE);
    tripal_map_update_storage($form_state, 'maps_comp', $select_options_corres['options_map'][$org_compkey], TRUE);
    tripal_map_update_storage($form_state, 'chr_comp', $select_options_corres['options_chr'][$org_compkey][$maps_compkey], TRUE);
  }

  if (tripal_map_get_form_trigger($form_state) == 'select_organism_comparison') {
    // the organism comparison selection item has changed
    tripal_map_update_storage($form_state, 'org_compkey', $form_state['triggering_element']['#value'], TRUE);
    tripal_map_update_storage($form_state, 'maps_comp', $select_options_corres['options_map'][$org_compkey], TRUE);
    tripal_map_update_storage($form_state, 'maps_compkey', key($select_options_corres['options_map'][$org_compkey]), TRUE);
    tripal_map_update_storage($form_state, 'chr_comp', $select_options_corres['options_chr'][$org_compkey][$maps_compkey], TRUE);
    tripal_map_update_storage($form_state, 'chr_compkey', key($select_options_corres['options_chr'][$org_compkey][$maps_compkey]), TRUE);
  }
  if (tripal_map_get_form_trigger($form_state) == 'select_map_comparison') {
    // the map comparison selection item has changed
    tripal_map_update_storage($form_state, 'maps_compkey', $form_state['triggering_element']['#value'], TRUE);
    tripal_map_update_storage($form_state, 'chr_comp', $select_options_corres['options_chr'][$org_compkey][$maps_compkey], TRUE);
    tripal_map_update_storage($form_state, 'chr_compkey', key($select_options_corres['options_chr'][$org_compkey][$maps_compkey]), TRUE);
  }

  if (tripal_map_get_form_trigger($form_state) == 'select_chromosome_comparison') {
    // the chromosome comparison selection item has changed
    tripal_map_update_storage($form_state, 'chr_compkey', $form_state['triggering_element']['#value'], TRUE);
  }

  tripal_map_update_storage($form_state, 'show_comparison_enabled', $show_comparison_enabled, TRUE);

  if (tripal_map_get_form_trigger($form_state) == 'show_comparison') {
    //save the show comparison setting in form state storage, so it is accessible to JS
    tripal_map_update_storage($form_state, 'show_comp', $show_comparison_checked, TRUE);
  }
  
  $form['organism_comparison']['select_organism_comparison']['#default_value'] = $org_compkey;
  $form['organism_comparison']['map_chr_comparison']['select_map_comparison']['#options'] = $fs_storage['maps_comp'];
  $form['organism_comparison']['map_chr_comparison']['select_map_comparison']['#default_value'] = $maps_compkey;
  $form['organism_comparison']['map_chr_comparison']['select_chromosome_comparison']['#options'] = $fs_storage['chr_comp'];
  $form['organism_comparison']['map_chr_comparison']['select_chromosome_comparison']['#default_value'] = $chr_compkey;
  $form['organism_comparison']['#disabled'] = !$show_comparison_checked;

  $linkage_group_name_comparison = tripal_map_get_lg_name_from_id($form_state, $org_compkey, $maps_compkey, $chr_compkey);
  tripal_map_update_storage($form_state, 'linkage_group_comparison', $linkage_group_name_comparison, TRUE);
  tripal_map_update_storage($form_state, 'featuremap_id_comparison', $maps_compkey, TRUE);

  // get marker data based on featuremap_id and linkage group selected from drop lists
  if (array_key_exists('featuremap_id_comparison', $fs_storage)) {
    $featuremap_id_comparison = $fs_storage['featuremap_id_comparison'];
  }
  if (array_key_exists('linkage_group_comparison', $fs_storage)) {
    $linkage_group_name_comparison = $fs_storage['linkage_group_comparison'];
  }
  
  $organism_id_comparison = $org_compkey;
  $features_comparison = tripal_map_create_features($featuremap_id_comparison, $chr_compkey, $organism_id_comparison);

  return $form;
}


/**
 * function: tripal_map_form_table
 *	 Create the downloadable table for marker map correspondences
 *
 * @param $ref_map_name
 * @param $ref_chr_name
 * @param $comp_map_name
 * @param $comp_chr_name
 * @param $ref_chr
 * @param $ref_map
 * @param $comp_chr
 * @param $comp_map
 *
 * @return - Redirect to CSV table file location
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_table($ref_map_name, $ref_chr_name, $comp_map_name, $comp_chr_name, $ref_chr, $ref_map, $comp_chr, $comp_map ) {
    
  $map_params = array("ref_map_name" => $ref_map_name, "ref_chr_name" => $ref_chr_name, "comp_map_name" => $comp_map_name,
     "comp_chr_name" => $comp_chr_name, "ref_chr" => $ref_chr, "ref_map" => $ref_map, "comp_chr" => $comp_chr, "comp_map" => $comp_map);
        
  $download_table_file_name = tripal_map_create_marker_map_corres_filename($map_params);
  $download_table_file = tripal_map_create_marker_map_corres_table($map_params, $download_table_file_name);
  $table_url = $download_table_file_name;
        
  drupal_goto($table_url);

}


/**
 * function: tripal_map_form_add_organism_comparison_bar
 *	 Add the comparison toolbar to the MapViewer form
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $show_genome_checked
 *
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_add_organism_comparison_bar(&$form, &$form_state, $select_options, $show_genome_checked) {
    
  $fs_storage =& $form_state['storage'];

  global $base_url;

  $ref_map = tripal_map_get_elem($fs_storage, 'maps_key');
  $ref_chr = tripal_map_get_elem($fs_storage, 'chr_key');
  $comp_map = tripal_map_get_elem($fs_storage, 'maps_compkey');
  $comp_chr = tripal_map_get_elem($fs_storage, 'chr_compkey');
  
  // Add the dot plot to the top of the Control panel, now that ref map and chr is available
  if (!$show_genome_checked) {
    $dotplot_icon = $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/dotplot.png';
    $dotplot_url = $base_url . "/dotplot/" . $ref_map . "/" . $ref_chr . "/" . $comp_map . "/" . $comp_chr;
    $form['dotplot'] = array(
      '#type' => 'container',
      '#prefix' => 
  	    '<div id="show_dot_plot" class="TripalMap">
        <label class="dotplot">Dot Plot  <a href= ' . $dotplot_url . '
  	    target="_blank" title="Dot Plot"><img id="dotplot_img" src="' . $dotplot_icon . 
  	    '" align="top"></a></label>',
        '#suffix' => '</div>',
        '#collapsible' => FALSE,
    );
  }
  
  // Create the Correspondence Matrix link
  $correspondence_matrix_url = $base_url . "/correspondence_matrix/" . $ref_map . "/" . $comp_map;
  $form['main_comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="show_main_comparison" class="TripalMap">
      <label class="reference">Select Comparison Map: </label>'
      .'<a id="correspondence_matrix_link" href= ' . $correspondence_matrix_url . ' target="_blank"
      title="Correspondence Matrix">(View Correspondence Matrix)</a></label>',
      '#suffix' => '</div>',
      '#collapsible' => FALSE,
  );

  $form['main_comparison']['mc_frame'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="show_mc_frame" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['main_comparison']['mc_frame']['comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="show_comparison_map" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );

  $form['main_comparison']['mc_frame']['comparison']['show_comparison'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show comparison map'),
    '#default_value' => tripal_map_get_elem($fs_storage, 'show_comp'), 
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#disabled' => !tripal_map_get_elem($fs_storage, 'show_comparison_enabled')
  );

  // create the table icon
  $download_table_icon = $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/download_table.png';

  $ref_map = tripal_map_get_elem($fs_storage, 'maps_key');
  $ref_map_name = $fs_storage['maps'][$ref_map];
  $ref_chr = tripal_map_get_elem($fs_storage, 'chr_key');
  $ref_chr_name = $fs_storage['chr'][$ref_chr];
  
  $comp_map = tripal_map_get_elem($fs_storage, 'maps_compkey');
  $comp_map_name = tripal_map_get_map_name($comp_map);
  $comp_chr = tripal_map_get_elem($fs_storage, 'chr_compkey');
  $org_id = tripal_map_get_elem($fs_storage, 'org_compkey');
  $comp_chr_name = tripal_map_get_lg_name_from_id($form_state, $org_id, $comp_map, $comp_chr);
  
  $map_params = array("ref_map_name" => $ref_map_name, "ref_chr_name" => $ref_chr_name, "comp_map_name" => $comp_map_name, 
      "comp_chr_name" => $comp_chr_name, "ref_chr" => $ref_chr, "ref_map" => $ref_map, "comp_chr" => $comp_chr, "comp_map" => $comp_map);
  
  $download_table_url = $base_url . "/download_table/". rawurlencode(tripal_map_encode_reserved_symbols($ref_map_name)) . "/" 
      . rawurlencode(tripal_map_encode_reserved_symbols($ref_chr_name)) . "/" . rawurlencode(tripal_map_encode_reserved_symbols($comp_map_name)) . "/" 
      . rawurlencode(tripal_map_encode_reserved_symbols($comp_chr_name)) ."/" . $ref_chr ."/" . $ref_map . "/" . $comp_chr . "/" . $comp_map;
                        
  $form['main_comparison']['mc_frame']['comparison']['download_table'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="download_table" class="TripalMap">'
      . '<a id="download_table_link" href= ' . $download_table_url . ' target="_blank"' 
      . 'title="Download Table"><img id="download_table_img" src="' . $download_table_icon 
      . '"></a>Download Table ',
    '#suffix' => '</div>',
  );
  
  $form['main_comparison']['mc_frame']['organism_comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="select_organism_comparison" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
    '#disabled' => !tripal_map_get_elem($fs_storage, 'show_comp')
  );

  $form['main_comparison']['mc_frame']['organism_comparison']['select_organism_comparison'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#options' => tripal_map_get_elem($select_options, 'options_org'),
    '#multiple' => FALSE,
    '#default_value' => tripal_map_get_elem($fs_storage, 'org_compkey'),
  );

  $form['main_comparison']['mc_frame']['organism_comparison']['map_chr_comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="map_chr_comparison' . '" class="form-item">',
    '#suffix' => '</div>',
    '#validated' => TRUE,
  );

  $form['main_comparison']['mc_frame']['organism_comparison']['map_chr_comparison']['select_map_comparison'] = array(
    '#type' => 'select',
    '#title' => t('Map'),
    '#prefix' => '<div id="select_map_comparison' . '" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form',
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'progress' => array('type' => 'throbber'),
      'method' => 'replace',
    ),
    '#options' => tripal_map_get_elem($fs_storage, 'maps_comp'),
    '#multiple' => FALSE,
    '#default_value' => tripal_map_get_elem($fs_storage, 'maps_compkey'),
    '#needs_validation' => FALSE,
  );

  $form['main_comparison']['mc_frame']['organism_comparison']['map_chr_comparison']['select_chromosome_comparison'] = array(
    '#type' => 'select',
    '#title' => t('Linkage Group'),
    '#prefix' => '<div id="select_chromosome_comparison' . '" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'progress' => array('type' => 'throbber'),
      'method' => 'replace',
    ),
    '#options' => tripal_map_get_elem($fs_storage, 'chr_comp'),
    '#multiple' => FALSE,
    '#default_value' => tripal_map_get_elem($fs_storage, 'chr_compkey'),

  );

  $form['main_comparison']['mc_frame']['organism_comparison']['button_chromosome_comparison_mv-submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
    '#name' => ('Submit_Comparison'),
    '#attributes' => array(
      'id' => 'chromosome_comparison_mv-submit'),
    '#prefix' => '<div class="chromosome_comparison_mv-submit-button">',
    '#suffix' => '</div>',
    '#submit' => array('tripal_map_submit_rebuild_form_state'),
    '#after_build' => array('tripal_map_draw_mapViewer'),
  );
 
  return $form;
}


/**
 * function: tripal_map_create_marker_map_corres_filename
 *	 Create the downloadable table filename for the genetic marker correspondences between
 *   the reference genetic map and comparison genetic map
 *
 * @param $map_params - The names and identifiers for the reference map and comparison map
 *
 * @return - The download table file name string.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_marker_map_corres_filename($map_params) {
  
  // create the path
  $dir = 'sites/default/files/tripal/tripal_mapviewer';
  $fe = file_exists($dir);
  if (!file_exists($dir)) {
    $res = mkdir($dir, 0777);
  }
  else {
    // remove all files in this directory, to prevent a build up of cached table files
    if (file_exists($dir)) {
      $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
      $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
      foreach ( $ri as $file ) {
        $file->isDir() ?  rmdir($file) : unlink($file);
      }
    }  
  }

  $ref_map_name = tripal_map_encode_special_char_filename($map_params["ref_map_name"]);
  $ref_chr_name = tripal_map_encode_special_char_filename($map_params["ref_chr_name"]);
  $ref_chr_name = tripal_map_lg_name_reduction($ref_chr_name, $ref_map_name);
  $comp_map_name = tripal_map_encode_special_char_filename($map_params["comp_map_name"]);
  $comp_chr_name = tripal_map_encode_special_char_filename($map_params["comp_chr_name"]);
  $comp_chr_name = tripal_map_lg_name_reduction($comp_chr_name, $comp_map_name);

  
  $file_name = "Marker_Correspondences_" . $ref_map_name . "_" . $ref_chr_name .
  "_vs_" . $comp_map_name . "_" . $comp_chr_name;

  $file_name = tripal_map_decode_reserved_symbols($file_name);
  $file_name_char_limit = 251; // 255 linux file name character limit, substracting the extension
  if (strlen($file_name) > $file_name_char_limit) {
    // truncate the file name, removing the end to fit the character limit
    $file_name = substr($file_name, 0, $file_name_char_limit);
  }

  $download_table_file = $dir . "/" . $file_name . ".csv";

  return $download_table_file;

}


/**
 * function: tripal_map_create_marker_map_corres_table
 *   Create the downloadable table for the genetic marker correspondences between
 *   the reference genetic map and comparison genetic map
 *
 * @param $map_params - The names and identifiers for the reference map and comparison map
 * @param $downlaod_table_file - Filename for file holding downloadable table 
 *
 * @return - True if file creation succeeds otherwise False
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_marker_map_corres_table($map_params, $download_table_file) {

  $ret = false;
  
  // open the file with the generated file name
  $handle = fopen($download_table_file, "w");
  fwrite($handle, "Reference and Comparison Map Marker Correspondence". "\n");
    
  // create the query to obtain the marker correspondence data
 $sql = "SELECT F.map_name as map_name_ref, F.map_id as map_id_ref, F.linkage_group as linkage_group_ref,
    F.linkage_group_id as linkage_group_id_ref, F.marker_locus_name as marker_locus_name_ref, F.feature_id as genetic_marker_id_ref,
    F.marker_type as marker_type_ref, F.marker_pos as marker_pos_ref, F.marker_pos_type as marker_pos_type_ref,
    G.map_name, G.map_id, G.linkage_group, G.linkage_group_id,  G.marker_locus_name, G.feature_id as genetic_marker_id, G.marker_type, 
    G.marker_pos, G.marker_pos_type
    FROM (
      SELECT GMV.map_name, GMV.map_id, GMV.linkage_group, GMV.linkage_group_id, GMV.marker_locus_name,  GMV.genetic_marker_id as feature_id,
        GMV.marker_type, GMV.marker_pos, GMV.marker_pos_type
      FROM {tripal_map_genetic_markers_mview} GMV
      WHERE (( GMV.linkage_group_id = :ref_chr_id) AND (GMV.map_id = :ref_map_id))
      UNION
      SELECT QMV.map_name, QMV.map_id, QMV.linkage_group, QMV.linkage_group_id,  QMV.marker_locus_name, QMV.feature_id,
        QMV.marker_type, QMV.marker_pos, QMV.marker_pos_type
      FROM {tripal_map_qtl_and_mtl_mview} QMV
      WHERE (( QMV.linkage_group_id = :ref_chr_id) AND (QMV.map_id = :ref_map_id))
    ) F
    INNER JOIN
    (
      SELECT GMV.map_name, GMV.map_id, GMV.linkage_group, GMV.linkage_group_id, GMV.marker_locus_name,  GMV.genetic_marker_id as feature_id,
        GMV.marker_type, GMV.marker_pos, GMV.marker_pos_type
      FROM {tripal_map_genetic_markers_mview} GMV
      WHERE (( GMV.linkage_group_id = :comp_chr_id) AND (GMV.map_id = :comp_map_id))
      UNION
      SELECT QMV.map_name, QMV.map_id, QMV.linkage_group, QMV.linkage_group_id,  QMV.marker_locus_name, QMV.feature_id,
        QMV.marker_type, QMV.marker_pos, QMV.marker_pos_type
      FROM {tripal_map_qtl_and_mtl_mview} QMV
      WHERE (( QMV.linkage_group_id = :comp_chr_id) AND (QMV.map_id = :comp_map_id))
    ) G ON G.feature_id = F.feature_id";

  $args = array();
  $args[':ref_chr_id'] =  $map_params["ref_chr"];
  $args[':ref_map_id'] =  $map_params["ref_map"];
  $args[':comp_chr_id'] = $map_params["comp_chr"];
  $args[':comp_map_id'] = $map_params["comp_map"];
    
  $results = chado_query($sql, $args);
  $numrec = $results->rowCount();
  $features = array();
  $ref_pos_types = array();
  $corres_pos_types = array();
  while( $results && ($results_obj = $results->fetchObject())) {
    if (is_object($results_obj)) {
      // parse the reference marker and its correspondences into the features object array
      // obtain the reference genetic_marker, and index it: 
      // [ 1234 (genetic_marker_id_ref): [mapnameref: m1, lgref:lg1, markerlocusname: mln, markertyperef:genetic/QTL, start:3, stop:5,
      //   corres_markers:[ 2345 (genetic_marker_id): [mapname, lg, markerlocusname: mln, markertype, start:4, stop:6]]], ..]
      // collect its start/stop position, as well as the secondary the genetic marker position data. If multiple loci, either for the 
      // reference or secondary marker, each will have its own indexed entry in the key-value array object
      if (property_exists($results_obj, 'genetic_marker_id_ref')) {
        $genetic_marker_id_ref = $results_obj->genetic_marker_id_ref;
        if (!array_key_exists($genetic_marker_id_ref, $features)) {
          $features[$genetic_marker_id_ref] = array();
        }
        $elem = 'map_name_ref';
        if (property_exists($results_obj, $elem)) {
          $features[$genetic_marker_id_ref][$elem] = $results_obj->$elem;
        }
        $elem = 'linkage_group_ref';
        if (property_exists($results_obj, $elem)) {
          $features[$genetic_marker_id_ref][$elem] = $results_obj->$elem;
        }
        $elem = 'marker_locus_name_ref';
        if (property_exists($results_obj, $elem)) {
          $features[$genetic_marker_id_ref][$elem] = $results_obj->$elem;
        }
        $elem = 'marker_type_ref';
        if (property_exists($results_obj, $elem)) {
          $features[$genetic_marker_id_ref][$elem] = $results_obj->$elem;
        }
        $elem = 'marker_pos_type_ref';
        if (property_exists($results_obj, $elem)) {
          $marker_pos_type_ref = $results_obj->$elem;
          $b = in_array($marker_pos_type_ref, $ref_pos_types);
          if (!in_array($marker_pos_type_ref, $ref_pos_types)) {
            array_push($ref_pos_types, $marker_pos_type_ref);
          }
          if (property_exists($results_obj, 'marker_pos_ref')) {
            $features[$genetic_marker_id_ref][$marker_pos_type_ref] = $results_obj->marker_pos_ref;
          }
        }
        if (!array_key_exists('corres_markers', $features[$genetic_marker_id_ref])) {
          $features[$genetic_marker_id_ref]['corres_markers'] = array();
        }
        if (property_exists($results_obj, 'genetic_marker_id')) {
          $genetic_marker_id = $results_obj->genetic_marker_id;
          if (!array_key_exists($genetic_marker_id, $features[$genetic_marker_id_ref]['corres_markers'])) {
            $features[$genetic_marker_id_ref]['corres_markers'][$genetic_marker_id] = array();
          }
          $corres_marker =& $features[$genetic_marker_id_ref]['corres_markers'][$genetic_marker_id];
          $elem = 'map_name';
          if (property_exists($results_obj, $elem)) {
            $corres_marker[$elem] = $results_obj->$elem;
          }
          $elem = 'linkage_group';
          if (property_exists($results_obj, $elem)) {
            $corres_marker[$elem] = $results_obj->$elem;
          }
          $elem = 'marker_locus_name';
          if (property_exists($results_obj, $elem)) {
            $corres_marker[$elem] = $results_obj->$elem;
          }
          $elem = 'marker_type';
          if (property_exists($results_obj, $elem)) {
            $corres_marker[$elem] = $results_obj->$elem;
          }
          $elem = 'marker_pos_type';
          if (property_exists($results_obj, $elem)) {
            $marker_pos_type = $results_obj->$elem;
            if (!in_array($marker_pos_type, $corres_pos_types)) {
              array_push($corres_pos_types, $marker_pos_type);
            }
            if (property_exists($results_obj, 'marker_pos')) {
              $corres_marker[$marker_pos_type] = $results_obj->marker_pos;
            }
          }
        }
      }
    }
  }

  // provide the column names - for now, later move this down
  $ref_pos_types_str = '';
  foreach ($ref_pos_types as $ref_pos_type) {
    $ref_pos_types_str .= 'Reference Marker '. $ref_pos_type . ' Position (cM),';
  }
  $corres_pos_types_str = '';
  foreach ($corres_pos_types as $corres_pos_type) {
    $corres_pos_types_str .= 'Comparison Marker '. $corres_pos_type . ' Position (cM),';
  }
  
  fwrite($handle, 'Reference Map, Reference Linkage Group, Reference Marker Locus, Reference Marker Type,' .
    $ref_pos_types_str . 'Comparison Map, Comparison Linkage Group, Comparison Marker Locus,' .
    'Comparison Marker Type,' . $corres_pos_types_str ."\n");
  
  foreach ($features as $feature) {
    $marker_str = "";
    $marker_str .= '"' . $feature['map_name_ref'] . '",';
    $marker_str .= '"' . $feature['linkage_group_ref'] . '",';
    $marker_str .= '"' . $feature['marker_locus_name_ref'] . '",';
    $marker_str .= '"' . $feature['marker_type_ref'] . '",';
    foreach ($ref_pos_types as $ref_pos_type) {
      $marker_str .= '"' . (array_key_exists($ref_pos_type, $feature)?$feature[$ref_pos_type]:'') . '",';
    }
    foreach ($feature['corres_markers'] as $corres_marker) {
      $tmp_marker_str = $marker_str;  
      $marker_str .= '"' . $corres_marker['map_name'] . '",';
      $marker_str .= '"' . $corres_marker['linkage_group'] . '",';
      $marker_str .= '"' . $corres_marker['marker_locus_name'] . '",';
      $marker_str .= '"' . $corres_marker['marker_type'] . '",';
      foreach ($corres_pos_types as $corres_pos_type) {
        $marker_str .= '"' . (array_key_exists($corres_pos_type, $corres_marker)?$corres_marker[$corres_pos_type]:'') . '",';
      }
      fwrite($handle, $marker_str . "\n");
      // refresh the row string with the reference marker, and prepare for the next correspondence marker
      $marker_str = $tmp_marker_str;
    }   
  }
  
  fclose($handle);
  $ret = true;
  
  return $ret;
}


/**
 * function: tripal_map_update_drupal_add_js_show_comparison_params
 *	 Pass the show comparison settings to JS
 *
 * @param $show_comparison
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_update_drupal_add_js_show_comparison_params($show_comparison) {

  // pass flag for whether to display comparison linkage group or not
  $js_setting = "mapViewerDisplayComparisonJS";
  drupal_add_js(array($js_setting => array('show_comparison' => $show_comparison)), 'setting');

}

