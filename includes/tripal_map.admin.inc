<?php

/**
 * @file
 * This file contains the functions used for administration of the module
 *
 * @ingroup tripal_map
 */

/**
 * Administrative settings form
 * 
 * function: tripal_map_admin_form
 *	 Initialize the TripalMap Admin form
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @return - The form array for the TripalMap Admin form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_admin_form($form, $form_state) {

  // text tutorial link
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => 'General',
  );

  $form['general']['tutorial'] = array(
    '#type' => 'fieldset',
    '#title' => 'Tutorial links',
  );

  $form['general']['tutorial']['video_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Video Tutorial Link'),
    '#default_value' => variable_get('tripal_map_video_tutorial_link', t('')),
  );

  $form['general']['tutorial']['text_tut'] = array(
    '#type' => 'fieldset',
    '#title' => 'Text Tutorial',
  );
  
  $form['general']['tutorial']['text_tut']['text_tut_exclude'] = array(
    '#type' => 'checkbox',
    '#title' => 'Exclude',
    '#description' => 'To hide the text tutorial link, select this option.',
    '#default_value' => variable_get('tripal_map_exclude_text_tutorial_links', FALSE),
  );
  
  $form['general']['tutorial']['text_tut']['text_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => variable_get('tripal_map_text_tutorial_link', t('')),
  );

  $form['general']['tutorial']['sample_map_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Sample Map Link  (ex. /200 or https://"@path")', array( '@path' => '<full path to map page>')),
    '#default_value' => variable_get('tripal_map_sample_map_tutorial_link', t('')),
  );

  $form['general']['tutorial']['sample_correspondence_matrix_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Sample Correspondence Matrix Link  (ex. /200/201 <br/> as @example)',
      array( '@example' => '/<mapid1>/<mapid2>')),
    '#default_value' => variable_get('tripal_map_sample_correspondence_matrix_tutorial_link', t('')),
  );
  
  $form['general']['tutorial']['sample_dotplot_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Sample Dot Plot Link  (ex. /200/4963605/201/4944173 <br/> as @example)', 
      array( '@example' => '/<mapid1>/<lgid1>/<mapid2>/<lgid2>')),
    '#default_value' => variable_get('tripal_map_sample_dotplot_tutorial_link', t('')),
  );
  
  $form['general']['tutorial']['example_exported_figures_tut_link'] = array(
    '#type' => 'textfield',
    '#title' => t('View Example Exported Figures'),
    '#default_value' => variable_get('tripal_map_example_exported_figures_tutorial_link', t('')),
  );
  
  $form['toolbar'] = array(
    '#type' => 'fieldset',
    '#title' => 'Toolbar',
  );

  $form['toolbar']['lg_display'] = array(
    '#type' => 'fieldset',
    '#title' => 'Linkage group display',
  );

  $form['toolbar']['lg_display']['marker_name_abbrev'] = array(
    '#type' => 'checkbox',
    '#title' => 'Feature name abbreviation',
    '#description' => 'Select this option if you wish to use derived abbreviation of the QTL or heritable phenotypic marker names if none are available in Chado.',
    '#default_value' => variable_get('tripal_map_marker_name_abbrev', TRUE)
  );

  $form['toolbar']['organism'] = array(
    '#type' => 'fieldset',
    '#title' => 'Organism selector display preferences',
  );

  $options_array = array();
  if (variable_get('tripal_map_toolbar_organism_common_name')) {
    array_push($options_array, 'common_name');
  }
  if (variable_get('tripal_map_toolbar_organism_genus_species') ||
    !(variable_get('tripal_map_toolbar_organism_common_name'))) {
    array_push($options_array, 'genus_species');
  }

  $form['toolbar']['organism']['org_selector'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'common_name' => t('Common Name'),
      'genus_species' => t('Genus and Species'),
    ),
    '#default_value' => $options_array,
  );
  
  // Show genome toolbar
  $form['toolbar']['genome_view'] = array(
    '#type' => 'fieldset',
    '#title' => 'Genome View',
  );
  
  $form['toolbar']['genome_view']['show_genome_view'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show genome view display',
    '#description' => 'Select this option to show the genome view display in the control panel when genome data are present.',
    '#default_value' => variable_get('tripal_map_show_genome_view', FALSE),
  );
  
  $form['chado'] = array(
    '#type' => 'fieldset',
    '#title' => 'Chado',
  );

  $form['chado']['marker_pos_type'] = array(
    '#type' => 'fieldset',
    '#title' => 'Feature Position Type',
  );

  $form['chado']['marker_pos_type']['marker_start_pos_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Feature Start Position Name'),
    '#default_value' => variable_get('tripal_map_chado_marker_start_pos_name', t('')),
  );

  $form['chado']['marker_pos_type']['marker_stop_pos_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Feature Stop Position Name'),
    '#default_value' => variable_get('tripal_map_chado_marker_stop_pos_name', t('')),
  );

  $form['chado']['marker_pos_type']['marker_qtl_peak_pos_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Feature QTL Peak Position Name'),
    '#default_value' => variable_get('tripal_map_chado_marker_qtl_peak_pos_name', t('')),
  );
  
  $form['chado']['marker_corres'] = array(
    '#type' => 'fieldset',
    '#title' => 'Marker Correspondence Name Type',
  );
  
  $form['chado']['marker_corres']['corres_marker_name_type'] = array(
    '#type' => 'radios',
    '#title' => t('Marker Name Type'),
    '#description' => 'Select the name that marker correspondences will be identified by.',
    '#default_value' => variable_get('tripal_map_chado_corres_marker_name_type', 1),
    '#options' => array(t('Marker Locus Name'), t('Genetic Marker Name') ),
  );
  
  $form['additional'] = array(
    '#type' => 'fieldset',
    '#title' => 'Tools',
  );
  
  $form['additional']['map_overview'] = array(
    '#type' => 'fieldset',
    '#title' => 'Map Overview Display',
  );
  
  $form['additional']['map_overview']['map_overview_exclude_maps'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude these maps from display  (ex. map1; map2; map3)'),
    '#default_value' => variable_get('tripal_map_map_overview_exclude_maps', t('')),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Configuration'
  );

  return $form;
}


/**
 * function: tripal_map_admin_form_validate
 *	 Validate inputs for the TripalMap admin form
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_admin_form_validate($form, &$form_state) {

  // Ensure at least one of the organism selector display preferences checkboxes is selected
  if ($error = !($form_state['values']['org_selector']['common_name'] || 
      $form_state['values']['org_selector']['genus_species'])) {
    form_error($form['toolbar']['organism']['org_selector'], st('Please select one of the Organism Selector Display Preferences checkboxes'));
  }

}


/**
 * function: tripal_map_admin_form_submit
 *	 Called on Admin form submit, write the form state values to Drupal variables 
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_admin_form_submit($form, &$form_state) {

  // check if the organism format was updated
  $organism_trigger = tripal_map_update_organism($form_state);
  if ($organism_trigger) {
      variable_set('tripal_map_toolbar_organism_trigger', TRUE);
      // this is unset when the select list options are regenerated. If the admin form is submitted 
      // more than once the organism format is still updated, otherwise the cached version is used
  }
  
  variable_set('tripal_map_video_tutorial_link', $form_state['values']['video_tut_link']);
  variable_set('tripal_map_text_tutorial_link', $form_state['values']['text_tut_link']);
  variable_set('tripal_map_sample_map_tutorial_link', $form_state['values']['sample_map_tut_link']);
  variable_set('tripal_map_sample_correspondence_matrix_tutorial_link', $form_state['values']['sample_correspondence_matrix_tut_link']);
  variable_set('tripal_map_sample_dotplot_tutorial_link', $form_state['values']['sample_dotplot_tut_link']);
  variable_set('tripal_map_example_exported_figures_tutorial_link', $form_state['values']['example_exported_figures_tut_link']);
  variable_set('tripal_map_marker_name_abbrev', $form_state['values']['marker_name_abbrev']);
  variable_set('tripal_map_toolbar_organism_common_name', $form_state['values']['org_selector']['common_name']);
  variable_set('tripal_map_toolbar_organism_genus_species', $form_state['values']['org_selector']['genus_species']);
  variable_set('tripal_map_chado_marker_start_pos_name', $form_state['values']['marker_start_pos_name']);
  variable_set('tripal_map_chado_marker_stop_pos_name', $form_state['values']['marker_stop_pos_name']);
  variable_set('tripal_map_chado_marker_qtl_peak_pos_name', $form_state['values']['marker_qtl_peak_pos_name']);
  variable_set('tripal_map_chado_corres_marker_name_type', $form_state['values']['corres_marker_name_type']);
  variable_set('tripal_map_map_overview_exclude_maps', $form_state['values']['map_overview_exclude_maps']);
  variable_set('tripal_map_exclude_text_tutorial_links', $form_state['values']['text_tut_exclude']);
  variable_set('tripal_map_show_genome_view', $form_state['values']['show_genome_view']);

}

/**
 * function: tripal_map_update_organism
 *   Required when loading the select list options to determine if select list option regeneration required. 
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_update_organism($form_state) {

  $organism_trigger = FALSE;
    
  $common_name =  strval(variable_get('tripal_map_toolbar_organism_common_name'));
  $genus_species = strval(variable_get('tripal_map_toolbar_organism_genus_species'));
  $form_common_name = strval($form_state['values']['org_selector']['common_name']);
  $form_genus_species = strval($form_state['values']['org_selector']['genus_species']);

  // compare existing form to cached setting to determine if organism format was updated 
  if (($common_name != $form_common_name) || ($genus_species != $form_genus_species)) {
    $organism_trigger = TRUE;
  }

  return $organism_trigger;
}
