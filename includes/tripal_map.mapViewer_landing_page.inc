<?php

/**
 * @file
 *	 Create form that displays the MapViewer landing page
 *	 Displays the select organism, map and linkage group toolbars and links to the correspondences matrix
 * 
 * @ingroup tripal_map
 */


/**
 * Implements hook_form().
 *
 * function: tripal_map_mapviewer_landing_page_form
 *   Create form to display the landing page. Called by any reference to the /MapViewer page URL 
 *   
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @return - A form array for the MapViewer Landing Page form.
 *    
 * @ingroup tripal_map_includes
 */
function tripal_map_mapviewer_landing_page_form($form, &$form_state) {
    
  $form['landing_page'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => array('landing_page'), // the js looks for this id to draw the map
      'class' => array("TripalMap")),
    '#collapsible' => FALSE,
  );
  
  $form['landing_page']['intro'] = array(
    // Brief description of what MapViewer is.
    '#markup' => '<div id="mapviewer_intro">
      <p>MapViewer is a graphical tool for viewing and comparing genetic maps. It includes dynamically scrollable maps, correspondence
       matrices, dot plots, links to details about map features, and exporting functionality.  It was developed by the MainLab at Washington State
       University and is available for download for use in other Tripal databases.  </p></div>',
      /* Functionality:
      1. interfaces show all linkage groups of a map, with zoom into specific regions of a linkage group,
      2. comparisons of maps that share the same markers are displayed.
      */
  );
  
  // display the toolbar to View Map
  $create = FALSE;
  $select_options = tripal_map_get_select_list_options($create);

  $featuremap_id = NULL;
  $linkage_group = NULL;
  $collapsed = TRUE; 
  tripal_map_form_landing_page_map_bar_init($form, $form_state, $select_options, $featuremap_id, $linkage_group);
  tripal_map_form_add_landing_page_map_bar($form, $form_state, $select_options, $collapsed);
  
  // display the correspondence matrix genetic map and genome toolbars
  tripal_map_form_landing_page_cm_init($form, $form_state, $select_options, $collapsed);
  
  $form['landing_page']['usage'] = array(
    '#type' => 'container',
    '#attributes' => array(
    'id' => array('usage'), // the js looks for this id to draw the map
    'class' => array("TripalMap")),
    '#collapsible' => FALSE,
  );

  $video_tut_link = variable_get('tripal_map_video_tutorial_link', t(''));
  $text_tut_link = variable_get('tripal_map_text_tutorial_link', t(''));
  $exclude_text_tut_link = variable_get('tripal_map_exclude_text_tutorial_links', FALSE);
  
  // obtain the sample map tutorial link
  $sample_map_tut_link = variable_get('tripal_map_sample_map_tutorial_link', t(''));
  $sample_map_id = "";
  if ($sample_map_tut_link != '') {
    if (substr($sample_map_tut_link, 0, 4) == "http") {
      // if the sample map path is a fully formed path ex. "https://.." leave it as is  
    }
    else {
      // a partial/relative path is provided, extract the map identifier
      $sample_map_id = preg_replace('/[^0-9]/', '', $sample_map_tut_link);
      $sample_map_tut_link = tripal_map_lookup_featuremap_page($sample_map_id);
    }
  }
  else {
    // the sample map path is unspecified by the user. Obtain the first map from the mview
    $sql = "
      SELECT map_id FROM {tripal_map_genetic_markers_mview}
      UNION
      SELECT map_id FROM {tripal_map_qtl_and_mtl_mview}
    ";
   
    $results = chado_query($sql);
    $pos = $results->fetchObject();
    if ($results && is_object($pos)) {
      $sample_map_id = $pos->map_id;
      $sample_map_tut_link = tripal_map_lookup_featuremap_page($sample_map_id);
    }
  }

  global $base_url;
  
  // Create the Correspondence Matrix link
  $correspondence_matrix_url = variable_get('tripal_map_sample_correspondence_matrix_tutorial_link', t(''));
  if ($correspondence_matrix_url == '') {
    $ref_map = tripal_map_get_elem($form_state['storage'], 'maps_key');
    $comp_map = 0;
    $map_org = $select_options['map_org'];
    $ak = array_keys($map_org);
    if (!empty($ak)) {
      $comp_map = $ak[0];
      if ((count($ak) > 1) && ($comp_map == $ref_map)) {
        $comp_map = $ak[1];
      }
    }
    $correspondence_matrix_url = $base_url . "/correspondence_matrix/" . $ref_map . "/" . $comp_map;
  }
  else {
    if (substr($correspondence_matrix_url, 0, 4) == "http") {
      // if the sample correspondence matrix path is a fully formed path ex. "https://.." leave it as is
    }
    else {
      $correspondence_matrix_url =  $base_url . '/correspondence_matrix' . $correspondence_matrix_url;
    }
  }
  
  // Create the Dot Plot link
  $dotplot_url = variable_get('tripal_map_sample_dotplot_tutorial_link', t('')); 
  if ($dotplot_url == '') {
    // The default dot plot is unspecified by the user. Obtain the first map/lg comparison
    $ref_lg_id = reset($map_org[$ref_map]['linkage_groups']);
    
    $select_options_comp = tripal_map_create_comparison_options($select_options, $ref_map, $ref_lg_id);    
    
    $comp_org = key(tripal_map_get_elem($select_options_comp, 'options_org'));
    $comp_map = "";
    if (is_array($select_options_comp['options_map'][$comp_org])) {
      $comp_map = key($select_options_comp['options_map'][$comp_org]);
    }
    $comp_lg_id = "";
    if (is_array($select_options_comp['options_chr'][$comp_org][$comp_map])) {
      $comp_lg_id = key($select_options_comp['options_chr'][$comp_org][$comp_map]);
    }
    $dotplot_url = $base_url . "/dotplot/" . $ref_map . "/" . $ref_lg_id . "/" . $comp_map . "/" . $comp_lg_id;
  }
  else {
    if (substr($dotplot_url, 0, 4) == "http") {
      // if the sample dot plot path is a fully formed path ex. "https://.." leave it as is
    }
    else {
      $dotplot_url =  $base_url . '/dotplot' . $dotplot_url;
    }
  }
  
  $example_export_url = variable_get('tripal_map_example_exported_figures_tutorial_link', t('')); //'/MapViewer_Exports';
  
  $user_manual = '';
  if (!$exclude_text_tut_link) {
    $user_manual = '<li id="user_manual"><a href="' . $text_tut_link . '" target="_blank">User Manual</a></li>';
  }
  
  $form['landing_page']['usage']['title'] = array(
    '#markup' => '<div><h3>Learn more about MapViewer</h3>
    <ul><li id="video_tutorial"><a href="' . $video_tut_link . '" target="_blank">Video tutorial</a></li>' . $user_manual .   
    '<li id="sample_map"><a href="' . $sample_map_tut_link . '" target="_blank">View sample map</a></li>
    <li id="sample_correspondence_matrix"><a href="' . $correspondence_matrix_url . '" target="_blank">View example correspondence matrix</a></li>
    <li id="sample_dotplot"><a href="' . $dotplot_url . '" target="_blank">View example dot plot</a></li>
    <li id="sample_exported_figures"><a href="' . $example_export_url . '" target="_blank">View example exported figures</a></li>
    </ul></div>',
  );

  return $form;
  
}


/**
 * function: tripal_map_form_landing_page_map_bar_init
 *	 Initialize the reference toolbar, using trigger values if available.
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_id
 * @param $linkage_group
 *
 * @return - The form array for the Landing page form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_landing_page_map_bar_init( &$form, &$form_state, $select_options, &$featuremap_id, &$linkage_group) {

  tripal_map_init_organism_selectors_from_storage($form_state, $select_options, $featuremap_id, $linkage_group);
	
  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
  $options_chr = $select_options['options_chr'];

  $fs_storage =& $form_state['storage'];
  $org_key =& $fs_storage['org_key'];
  $maps_key =& $fs_storage['maps_key'];
  $chr_key =& $fs_storage['chr_key'];

  tripal_map_update_storage($form_state, 'maps', $options_map[$org_key], FALSE);
  tripal_map_update_storage($form_state, 'chr', $options_chr[$org_key][$maps_key], FALSE);

  $selector_toolbar = "landingpage_map_bar";
  $fs_selector_names = array("orgkey" => "org_key", "maps" => "maps", "mapskey" => "maps_key",
    "chr" => "chr", "chrkey" => "chr_key");
  $form_trigger_selectors = array(  "select_organism" => "select_organism", "select_map" => "select_map",
			"select_chromosome" =>"select_chromosome");

  tripal_map_trigger_update_storage_from_selectors($selector_toolbar, $fs_selector_names,
    $form_trigger_selectors, $select_options, $form_state);
	
  $form['landing_page']['toolbar']['map_select_frame']['select_organism']['#default_value'] = $org_key;
  $form['landing_page']['toolbar']['map_select_frame']['map_chr']['select_map']['#options'] = $fs_storage['maps'];
  $form['landing_page']['toolbar']['map_select_frame']['map_chr']['select_map']['#default_value'] = $maps_key;
  $form['landing_page']['toolbar']['map_select_frame']['map_chr']['select_chromosome']['#options'] = $fs_storage['chr'];
  $form['landing_page']['toolbar']['map_select_frame']['map_chr']['select_chromosome']['#default_value'] = $chr_key;

  return $form;

}


/**
 * function: tripal_map_form_add_landing_page_map_bar
 *	 Create the reference toolbar with selectors for organism, map and chromosome
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 *
 * @return - The form array for the Landing page form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_add_landing_page_map_bar(&$form, &$form_state, $select_options, $collapsed) {

  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
  $options_chr = $select_options['options_chr'];

  $fs_storage =& $form_state['storage'];
  $org_key    =& $fs_storage['org_key'];
  $maps_key   =& $fs_storage['maps_key'];
  $chr_key    =& $fs_storage['chr_key'];

  global $base_url;
  $form['landing_page']['toolbar'] = array(
    '#type' => 'fieldset',
    '#title' => '<img src="'.$base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/mapviewer_icon.png" height="45px">'
    .'<span id="fieldset-span-description">' . t(' Select a map and start using MapViewer ') . '</span>',
    '#prefix' => '<div id="map_select_fieldset" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
  );
  drupal_add_library('system', 'drupal.collapse');
  
  $form['landing_page']['toolbar']['map_select_fieldset'] = array(
    '#type' => 'container',
    '#prefix' => '',
    '#suffix' => '',  
    '#collapsible' => FALSE,
  );
  
  $form['landing_page']['toolbar']['map_select_fieldset']['info'] = array(
    '#markup' => '<div id= "mapviewer_toolbar_info">
    <p>When the "Submit" button is clicked, a new page for that map will open 
    and on that page a second linkage group for comparison can be selected. 
    Or, browse all <a href="/search/featuremap/list" target="_blank"> available maps</a>.</p></div>',
  );
  
  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="map_select_frame" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame']['select_organism'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
    '#ajax' => array(
      'wrapper' => 'map_select_frame',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'map_select_frame'),
    ),
    '#options' => $options_org,
    '#multiple' => FALSE,
    '#default_value' => $org_key,
  );

  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame']['map_chr'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="map_chr' . '" class="form-item">',
    '#suffix' => '</div>',
    '#validated' => TRUE,
  );

  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame']['map_chr']['select_map'] = array(
    '#type' => 'select',
    '#title' => t('Map'),
    '#prefix' => '<div id="select_map' . '" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'map_select_frame',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'map_select_frame'),
    ),
    '#options' => $fs_storage['maps'],
    '#multiple' => FALSE,
    '#default_value' => $maps_key,
    '#needs_validation' => FALSE,
  );

  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame']['map_chr']['select_chromosome'] = array(
    '#type' => 'select',
    '#title' => t('Linkage Group'),
    '#prefix' => '<div id="select_chromosome' . '" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'map_select_frame',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'map_select_frame'),
    ),
      
    '#options' => $fs_storage['chr'],
    '#multiple' => FALSE,
    '#default_value' => $chr_key,
  );

  global $base_url;
  $featuremap_id = tripal_map_get_elem($fs_storage, 'maps_key');
  $lg_id = tripal_map_get_elem($fs_storage, 'chr_key');
  $lg_name = tripal_map_get_lg_name_from_id($form_state, $org_key, $featuremap_id, $lg_id);
  
  $lg_name = tripal_map_encode_special_chars($lg_name);
  $lg_name = urlencode($lg_name);
  $mapviewer_url = $base_url . "/mapviewer/" . $featuremap_id . "/" . $lg_name;

  $form['landing_page']['toolbar']['map_select_fieldset']['map_select_frame']['button_map_select_landingpage-submit'] = array(
    '#type' => 'button',
    '#value' => t('Submit'),
    '#name' => ('Submit_Reference'),
    '#attributes' => array(
      'id' => 'map_select_landingpage-submit',
      'onclick' => 'javascript:window.open("' . $mapviewer_url . '"); return false;',
    ),
    '#executes_submit_callback' => FALSE,
    '#prefix' => '<div class="chromosome_mv-submit-button">',
    '#suffix' => '</div>',

  );
	 
  $form['#prefix'] = '<div id="show-map-chr-landingpage-form">';
  $form['#suffix'] = '</div>';

  return $form;

}

/**
 * function: tripal_map_form_landing_page_cm_init
 *	 Display genome correspondence matrix toolbar to View Genome Correspondence Matrix and initialize toolbars and form state storage
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $collapsed
 *
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_landing_page_cm_init(&$form, &$form_state, $select_options, $collapsed) {

  global $base_url;
  $form['landing_page']['cm_toolbar_map'] = array(
    '#type' => 'fieldset',
    '#title' => '<img src="'. $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/cm_genetic_icon.png" height="45px">'
    .'<span id="fieldset-span-description">' . t(' View the Correspondence Matrix and browse correspondences between genetic maps') . '</span>',
    '#prefix' => '<div id="cm_map_select_fieldset" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed, 
      
  );
  
  // display the genetic map correspondence matrix toolbar
  $featuremap_cm_prim_id = NULL;
  $featuremap_cm_sec_id = NULL;
  tripal_map_form_landing_page_cm_organism_bar_init( $form, $form_state, $select_options, $featuremap_cm_prim_id, $featuremap_cm_sec_id);
  tripal_map_form_landing_page_correspondence_matrix_bar($form, $form_state, $select_options, $featuremap_cm_prim_id, $featuremap_cm_sec_id);

  $form['landing_page']['cm_toolbar_genome'] = array(
      '#type' => 'fieldset',
      '#title' => '<img src="'. $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/cm_genome_icon.png" height="45px">'
      .'<span id="fieldset-span-description">' . t(' View the Correspondence Matrix and browse correspondences between whole genomes and maps') . '</span>',
      '#prefix' => '<div id="cm_genome_select_fieldset" class="TripalMap">',
      '#suffix' => '</div>',
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
  );
  
  // display genome correspondence matrix toolbar to View Genome Correspondence Matrix
  // Initialize toolbars and form state storage
  $select_genome_options = tripal_map_get_genome_select_list_options();
  $featuremap_prim_id = NULL;
  $analysis_id = NULL;
  tripal_map_form_landing_page_cm_genome_bar_init($form, $form_state, $select_genome_options, $featuremap_prim_id, $analysis_id);
  tripal_map_form_add_landing_page_correspondence_matrix_genome_bar($form, $form_state, $select_genome_options, $analysis_id);

}


/**
 * function: tripal_map_form_landing_page_cm_organism_bar_init
 *	 Initialize the correspondence matrix toolbar, using trigger values if available.
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_cm_prim_id
 * @param $featuremap_cm_sec_id
 *  
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
// assigns the $features array based on the featuremap id
function tripal_map_form_landing_page_cm_organism_bar_init(&$form, &$form_state, $select_options, &$featuremap_cm_prim_id, &$featuremap_cm_sec_id) {
    
  if (array_key_exists('storage', $form_state)) {
    // the form was updated, but the url is not up to date yet, obtain previous value from form state storage
    // use the linkage group and map values from the form state
    if (array_key_exists('featuremap_cm_prim_id', $form_state['storage'])) {
     $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
    }
    if (array_key_exists('featuremap_cm_sec_id', $form_state['storage'])) {
      $featuremap_cm_sec_id = $form_state['storage']['featuremap_cm_sec_id'];
    }
  }
    
  tripal_map_init_cm_organism_selectors_from_storage($form_state, $select_options, $featuremap_cm_prim_id, $featuremap_cm_sec_id);
    
  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
  $options_chr = $select_options['options_chr'];
  $fs_storage =& $form_state['storage'];
    
  // primary
  $selector_toolbar = "correspondence_matrix_primary";
 
  $fs_selector_names = array("orgkey" => "cm_prim_org_key", "maps" => "cm_prim_maps",
    "mapskey" => "cm_prim_maps_key", "chr" => "", "chrkey" => "");
  $form_trigger_selectors = array(  "select_organism" => "select_organism_primary_cm",
    "select_map" => "select_map_primary_cm", "select_chromosome" =>"");

  tripal_map_trigger_update_storage_from_selectors($selector_toolbar, $fs_selector_names,
    $form_trigger_selectors, $select_options, $form_state);
    
  $fs_cm_prim_org_key = $fs_storage['cm_prim_org_key'];
    
  // The 'All' option will be added during cm init and not at options_map source origin as it is not a map and instead refers to all maps
  $cm_prim_maps_plus_all = array("0" => "All") + $options_map[$fs_cm_prim_org_key];

  tripal_map_update_storage($form_state, 'cm_prim_maps', $cm_prim_maps_plus_all, TRUE);
   
  // secondary
  $selector_toolbar = "correspondence_matrix_secondary";
  $fs_cm_sec_org_key = $fs_storage['cm_sec_org_key'];
  
  tripal_map_update_storage($form_state, 'cm_sec_maps', $options_map[$fs_cm_sec_org_key], FALSE);
 
  $fs_selector_names = array("orgkey" => "cm_prim_sec_key", "maps" => "cm_sec_maps",
    "mapskey" => "cm_sec_maps_key", "chr" => "", "chrkey" => "");
  $form_trigger_selectors = array(  "select_organism" => "select_organism_secondary_cm",
    "select_map" => "select_map_secondary_cm", "select_chromosome" =>"");
  tripal_map_trigger_update_storage_from_selectors($selector_toolbar, $fs_selector_names,
    $form_trigger_selectors, $select_options, $form_state);
    
  tripal_map_update_storage($form_state, 'featuremap_cm_prim_id', $fs_storage['cm_prim_maps_key'], TRUE);
  tripal_map_update_storage($form_state, 'featuremap_cm_sec_id', $fs_storage['cm_sec_maps_key'], TRUE);
 
  $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
  $featuremap_cm_sec_id = $form_state['storage']['featuremap_cm_sec_id'];
  
  return $form;
  
}

/**
 * function: tripal_map_form_landing_page_correspondence_matrix_bar
 *	 Create the correspondence matrix toolbar with selectors for organism and map 
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_cm_prim_id
 * @param $featuremap_cm_sec_id
 *
 * @return - The form array for the Landing page form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_landing_page_correspondence_matrix_bar(&$form, &$form_state, $select_options, $featuremap_cm_prim_id, $featuremap_cm_sec_id) {
    
  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
    
  $fs_storage =& $form_state['storage'];
  $cm_prim_org_key = $fs_storage['cm_prim_org_key'];
  $cm_prim_maps_key = $fs_storage['cm_prim_maps_key'];
  $cm_sec_org_key = $fs_storage['cm_sec_org_key'];
  $cm_sec_maps_key = $fs_storage['cm_sec_maps_key'];
              
  $form['landing_page']['cm_toolbar_map']['info'] = array(
    '#markup' => '<div id= "genetic_cm_toolbar_info">
    <p> When the Submit button is clicked, a new correspondence matrix page will appear to allow browsing correspondence between two genetic maps or 
    between all genetic maps in a species and one genetic map.</p></div>',
  );
    
  $form['landing_page']['cm_toolbar_map']['gm'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="gm" class="TripalMap select_lp_correspondence_matrix">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['landing_page']['cm_toolbar_map']['gm']['organism_primary'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="select_organism_primary_cm" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
    
  $form['landing_page']['cm_toolbar_map']['gm']['organism_primary']['select_organism_primary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
    '#prefix' => '<div id="select_organism_primary_cm" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'gm',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'gm'),
     ),
        
     '#options' => $options_org,
     '#multiple' => FALSE,
     '#default_value' => $cm_prim_org_key,
  );
    
    unset($form_state['input']['select_map_primary_cm']);
    $form['landing_page']['cm_toolbar_map']['gm']['organism_primary']['select_map_primary_cm'] = array(
      '#type' => 'select',
      '#title' => t('Map'),
      '#prefix' => '<div id="select_map_primary_cm" class="form-item">',
      '#suffix' => '</div>',
      '#ajax' => array(
        'wrapper' => 'gm',
        'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
        'method' => 'replace',
        'progress' => array('type' => 'throbber'),
      ),
      '#attributes' => array(
        'update' => array('wrapper' => 'gm'),
      ),
      '#options' => $fs_storage['cm_prim_maps'],
        '#multiple' => FALSE,
        '#default_value' => strval($cm_prim_maps_key),
        '#needs_validation' => FALSE,
    );

  $form['landing_page']['cm_toolbar_map']['gm']['organism_secondary'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="select_organism_secondary_cm" class="TripalMap"><label class="reference">Compare To</label>',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
    
  $form['landing_page']['cm_toolbar_map']['gm']['organism_secondary']['select_organism_secondary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
    '#prefix' => '<div id="select_organism_secondary_cm" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'gm',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'gm'),
    ),
    '#options' => $options_org,
    '#multiple' => FALSE,
    '#default_value' => $cm_sec_org_key,
  );
    
  $form['landing_page']['cm_toolbar_map']['gm']['organism_secondary']['select_map_secondary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Map'),
    '#prefix' => '<div id="select_map_secondary_cm" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'gm',
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'gm'),
    ),
    '#options' => $fs_storage['cm_sec_maps'],
    '#multiple' => FALSE,
    '#default_value' => $cm_sec_maps_key,
    '#needs_validation' => FALSE,
  );
    
  global $base_url;
    
  $ref_map = $fs_storage['cm_prim_maps_key'];
  $comp_map = $fs_storage['cm_sec_maps_key'];
    
  $correspondence_matrix_url = $base_url . "/correspondence_matrix/" . $ref_map . "/" . $comp_map;
  $form['landing_page']['cm_toolbar_map']['gm']['organism_secondary']['button_cm-submit'] = array(
    '#type' => 'button',
    '#value' => t('Submit'),
    '#name' => ('Submit_CM'),
    '#attributes' => array(
      'id' => 'cm-submit',
      'onclick' => 'javascript:window.open("' . $correspondence_matrix_url . '"); return false;',
    ),
    '#prefix' => '<div class="genetic_map_cm_landingpage-submit-button">',
    '#suffix' => '</div>',
  );
      
  return $form;
    
}


/**
 * function: tripal_map_form_landing_page_cm_genome_bar_init
 *	 Initialize the correspondence matrix genome toolbar, using trigger values if available.
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options 
 * @param $featuremap_cm_prim_id - The initial genetic map
 * @param $analysis_id - The initial genome analysis
 *    
 * @return - Update form state storage values for genome correspondence matrix form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_landing_page_cm_genome_bar_init( &$form, &$form_state, $select_options, &$featuremap_cm_prim_id, &$analysis_id) {
    
  tripal_map_init_cm_genome_selectors_to_storage($form_state, $select_options, $featuremap_cm_prim_id, $analysis_id);
  
  $options_analysis = $select_options['options_analysis'];

  $fs_storage =&              $form_state['storage'];
  $featuremap_cm_prim_id =    $fs_storage['featuremap_cm_prim_id'];
  $analysis_id =              $fs_storage['analysis_compkey'];
    
  if (tripal_map_get_form_trigger($form_state) == "select_analysis_cm") {
    tripal_map_update_storage($form_state, "analysis_compkey", $form_state['triggering_element']['#value'], TRUE);
  }
    
  $analysis_id = $form_state['storage']['analysis_compkey'];
    
  return $form;

}


/**
 * function: tripal_map_form_add_landing_page_correspondence_matrix_genome_bar
 *	 Create the reference toolbar with selectors for whole genome
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $analysis_id
 *
 * @return - The form array for the Landing Page form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_add_landing_page_correspondence_matrix_genome_bar(&$form, &$form_state, $select_options, $analysis_id) {
    
  $fs_storage =& $form_state['storage'];

  $options_analysis = $select_options['options_analysis'];
  $analysis_key = $fs_storage['analysis_compkey'];
    
  $form['landing_page']['cm_toolbar_genome']['select_analysis'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="select_analysis" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['landing_page']['cm_toolbar_genome']['select_analysis']['info'] = array(
    '#markup' => '<div id= "genome_cm_toolbar_info">
    <p> When the Submit button is clicked, a new correspondence matrix page will appear to allow browsing correspondence between the whole genome and
    maps.</p></div>',
  );
  
  $form['landing_page']['cm_toolbar_genome']['select_analysis']['select_analysis_cm'] = array(
    '#type' => 'select',
    '#title' => t('Whole Genome'),
    '#prefix' => '<div id="select_analysis_cm' . '"  class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'select_analysis', 
      'callback' => 'tripal_map_genome_landingpage_form_ajax_callback', 
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#attributes' => array(
      'update' => array('wrapper' => 'select_analysis'),
    ),
    '#options' => $options_analysis,
    '#multiple' => FALSE,
    '#default_value' => $analysis_key,
  );

  global $base_url;
  
  $ref_map = tripal_map_get_elem($fs_storage, 'maps_key');
  $correspondence_matrix_url = $base_url . "/correspondence_matrix_genomes/" . $ref_map . "/" . $analysis_key . "/#" . $ref_map;
  
  $form['landing_page']['cm_toolbar_genome']['select_analysis']['button_analysis_select_landingpage-submit'] = array(
    '#type' => 'button',
    '#value' => t('Submit'),
    '#name' => ('Submit_Genome_Comparison'),
    '#attributes' => array(
    'id' => 'analysis_cm_select_landingpage-submit',
    'onclick' => 'javascript:window.open("' . $correspondence_matrix_url . '"); return false;', 
  ),

  '#executes_submit_callback' => FALSE,
  '#prefix' => '<div class="analysis_cm_select_landingpage-submit-button">', 
  '#suffix' => '</div>',
      
  );
    
  return $form;
  
}
