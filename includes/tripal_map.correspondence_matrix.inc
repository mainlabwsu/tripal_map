<?php

/**
 * @file
 *	 Create form that displays correspondence matrix
 *	 Draw the Correspondence Matrix page displaying a table of correspondences between the given
 *	 map, along with toolbars to select display of multi-map correspondences 
 *	 Toolbars select organism and map to compare against the selected map 
 * 
 * @ingroup tripal_map
 */


/**
 * Implements hook_form().
 *
 * function: tripal_map_correspondence_matrix_form
 *   When displaying a map correspondences matrix, we need a form.  This function creates the form 
 *   used for this. Called by URL for a specific route based on the map given parameters
 *
 * @param $form -The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $featuremap_prim_id
 * @param $featuremap_sec_id
 * @param $show_genome
 *  
 *  @return - A form array for the correspondences matrix form.
 *    
 * @ingroup tripal_map_includes
 */
function tripal_map_correspondence_matrix_form($form, &$form_state, $featuremap_prim_id = NULL, $featuremap_sec_id = NULL, $show_genome = NULL) {

  $genome_comp = false;
  $analysis_id = "";
  if (isset($show_genome) && $show_genome) {
    $genome_comp = true;
    $analysis_id = $featuremap_sec_id;  
  }
      
  // if there is no map supplied do not build the form
  if (!isset($form_state['build_info']['args'][0])) {
    return drupal_not_found();
  }

  if ((!isset($featuremap_prim_id) || strlen($featuremap_prim_id) == 0) ||
      (!isset($featuremap_sec_id) || strlen($featuremap_sec_id) == 0)) {
    return drupal_not_found();
  }
 
  // Generate the organism, and map selector lists data for the correspondence matrix.
  if (!array_key_exists('storage', $form_state)) {
  	$form_state['storage'] = array();
  }
  
  $create = FALSE;
  $select_options = tripal_map_get_select_list_options($create);
  
  // add the JS files
  tripal_map_load_correspondence_matrix_js($form, $form_state);
  tripal_map_update_storage($form_state, 'flag_update_js_maps', FALSE, FALSE);
  $js_setting_corres_matrix = "";
  
  if (!$genome_comp) {
    // primary featuremap id is 0, case where 'All' maps are compared
    $featuremapAll = false;
    if ($featuremap_prim_id == '0') {
      $featuremapAll = true;
    }
    if (((!tripal_map_get_organism_id($select_options['map_org'], $featuremap_prim_id)) && !$featuremapAll) || 
      (!tripal_map_get_organism_id($select_options['map_org'], $featuremap_sec_id))) {
      // there is no organism belonging to the primary or secondary map, and the primary map does not refer to 'All' maps 
      return drupal_not_found();
    }
      
    // Enable to show selector menus, only if genetic map comparison 
    // Initialize toolbars and form state storage
    // set the update maps flag to false, if it does not exist yet.
    tripal_map_form_cm_organism_bar_init($form, $form_state, $select_options, $featuremap_prim_id, $featuremap_sec_id);
    tripal_map_form_correspondence_matrix_bar($form, $form_state, $select_options);
  
    // add refmap, compmap and corres to JS
    // features correspondence matrix structure - {rows: [type: refmap,  name: $ref_map_name, lgs: [lg1, lg2, ...]}, cols: {type: compmap,  name: $comp_map_name, lgs: [lg1, lg2, ...]}, corresp: [[1,2,...],[2,3,...],...]}
    $features_corres_matrix = tripal_map_create_cm_JS_params($featuremap_prim_id, $featuremap_sec_id, $select_options, $form_state);
    $js_setting_corres_matrix = 'mapCorrespondenceMatrixJS';
    
  } 
  else {
    if (!tripal_map_get_organism_id($select_options['map_org'], $featuremap_prim_id)) {
      // there is no organism belonging to the primary map
      return drupal_not_found();
    }
      
    $features_corres_matrix = tripal_map_create_cm_genome_JS_params($featuremap_prim_id, $analysis_id, $select_options); 
    $js_setting_corres_matrix = 'mapGenomeCorrespondenceMatrixJS';
    if (array_key_exists('flag_update_js_maps', $form_state['storage'])) {
        $form_state['storage']['flag_update_js_maps'] = TRUE;
    }
  }
  
  // assign the correspondence matrix parameters to the JS client
  if ($form_state['storage']['flag_update_js_maps'] == TRUE) {
      tripal_map_update_drupal_add_js_params($js_setting_corres_matrix, NULL, NULL, NULL, $features_corres_matrix);
      $form_state['storage']['flag_update_js_maps'] = FALSE;
  }
  
  // attach JS library files
  $form = tripal_map_attach_d3_lib($form);
  $form['#attached']['js'][] = drupal_get_path('module', 'tripal_map') . '/theme/js/libraries/FileSaver.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'tripal_map') . '/theme/js/libraries/canvas-to-blob.min.js';
  
  // form the container where the correspondence matrix will be drawn
  $form['select'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => array('select_fieldset_correspondence_matrix'), // JS looks for this id to draw the matrix
      'class' => array("TripalMap")),
    '#collapsible' => FALSE,
  );
  
  return $form;
}


/**
 * function: tripal_map_create_cm_JS_params
 *	 Prepare correspondence matrix settings to provide to JS.
 *
 * @param $featuremap_prim_id
 * @param $featuremap_sec_id
 * @param $select_options
 * @param $form_state
 *
 * @return - Feature array for the correspondence matrix 
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_cm_JS_params($featuremap_prim_id, $featuremap_sec_id, $select_options, $form_state) {

  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
  $fs_storage =& $form_state['storage'];
  
  tripal_map_create_correspondences();
  
  $features_corres_matrix = array();
  // rows
  $features_corres_matrix['rows'] = array();
  $features_cm_rows = &$features_corres_matrix['rows'];
  // cols
  $comp_map_lgs = $map_org[$featuremap_sec_id]['linkage_groups'];

  // flip the array, so the keys (the linkage group names) become the values
  // then the custom linkage group sort will order them properly
  $flipped_comp_map_lgs = array_flip($comp_map_lgs);
  tripal_map_customLgSort($flipped_comp_map_lgs);
  // after the sort, reverse the array flip
  $comp_map_lgs = array_flip($flipped_comp_map_lgs);
  // correspondences
  $features_corres_matrix['correspondences'] = array();
  $features_cm_matrix = &$features_corres_matrix['correspondences'];
  
  if ($featuremap_prim_id == '0') {
    // if primary feature is 'All' (0 index) display correspondences for each map in that species with those in all 
    // linkage groups in the secondary map
    // get the organism displayed in the menu selector, since no specific map is chosen no organism is associated with it
    $fs_cm_prim_org_key = $fs_storage['cm_prim_org_key'];
    $ref_org_maps = $options_map[$fs_cm_prim_org_key];
    ksort($ref_org_maps);
    
    // rows are ref map, vary/iterate over the rows as maps, with corres_map_id, so rows are the maps,
    // cols are the lgs of the sec map
    foreach ($ref_org_maps as $ref_map_id => $ref_map_name) {
      $features_cm_matrix[$ref_map_id] = array();
      foreach ($comp_map_lgs as $sec_lgs => $lg ) {
              
        $features_cm_matrix[$ref_map_id][$lg] = array();
        // iterate through every map in this organism that the secondary map has correspondences with
        $map_corres_prim_res = db_query("SELECT SUM(lgval_corres) FROM {tripal_map_correspondences} WHERE map_id = '".$featuremap_sec_id.
          "' AND corres_map_id = '".$ref_map_id."' AND lgkey = '".$lg."'");
        
        $map_corres_num = 0;
        $tmpmap_corres_num = $map_corres_prim_res->fetchColumn();
        if ($tmpmap_corres_num) {
          $map_corres_num = $tmpmap_corres_num;
        }
              
        // append the total to the primary correspondences array
        $features_cm_matrix[$ref_map_id][$lg] = $map_corres_num;
      }
      $features_cm_matrix[$ref_map_id] = array_values($features_cm_matrix[$ref_map_id]);

    }
    
    // reference map
    $features_cm_rows['type'] = 'ref_organism';
    // get org name the organism name
    $features_cm_rows['name'] = $options_org[$fs_cm_prim_org_key];
    $features_cm_rows['maps']['feature_ids'] = array_keys($ref_org_maps);
    $features_cm_rows['maps']['names'] = array_values($ref_org_maps);
    $features_cm_matrix = array_values($features_cm_matrix);
  
  }
  else {
      
    $ref_map_lgs = $map_org[$featuremap_prim_id]['linkage_groups'];
    // flip the array, so the keys (the linkage group names) become the values
    // then the custom linkage group sort will order them properly
    $flipped_ref_map_lgs = array_flip($ref_map_lgs);
    tripal_map_customLgSort($flipped_ref_map_lgs);
    // after the sort, reverse the array flip
    $ref_map_lgs = array_flip($flipped_ref_map_lgs);
      
    // reference map
    $features_cm_rows['type'] = 'refmap';
    $features_cm_rows['name'] = tripal_map_get_map_name($featuremap_prim_id);
    $features_cm_rows['feature_id'] = $featuremap_prim_id;
    $features_cm_rows['lgs'] = array_keys($ref_map_lgs);
    
    foreach ($ref_map_lgs as $ref_map_lg_name => $ref_map_lg_id) {
      
      $features_cm_matrix[$ref_map_lg_id] = array();
      
      $ref_map_lgs_in_map_corres_prim_res = db_query("SELECT * FROM {tripal_map_correspondences} WHERE map_id = '".$featuremap_sec_id.
        "' AND corres_map_id = '".$featuremap_prim_id."' AND corres_lgkey = '".$ref_map_lg_id."'");
      
      if ($ref_map_lgs_in_map_corres_prim_res) {
        $ref_map_lgs_in_map_corres_prim = $ref_map_lgs_in_map_corres_prim_res->fetchAllAssoc('lgkey');
        foreach ($comp_map_lgs as $comp_map_lg_name => $comp_map_lg_id) {
          if (array_key_exists($comp_map_lg_id, $ref_map_lgs_in_map_corres_prim)) {
            $lg_corres_sec_val = $ref_map_lgs_in_map_corres_prim[$comp_map_lg_id]->lgval_corres;
            // append the number of correspondences between the two linkage groups to the array
            array_push($features_cm_matrix[$ref_map_lg_id], $lg_corres_sec_val);
          }
          else {
            array_push($features_cm_matrix[$ref_map_lg_id], 0);
          }
        }
      }
      else {
        $ref_map_lg_id_corres = array_fill(0, sizeof($comp_map_lgs), 0);
        $features_cm_matrix[$ref_map_lg_id] = $ref_map_lg_id_corres;
      }
    }
    $features_cm_matrix = array_values($features_cm_matrix);
  
  }
  
  // comparison map
  $features_corres_matrix['cols'] = array();
  $features_cm_cols = &$features_corres_matrix['cols'];
  $features_cm_cols['type'] = 'compmap';
  $features_cm_cols['name'] = tripal_map_get_map_name($featuremap_sec_id);
  $features_cm_cols['feature_id'] = $featuremap_sec_id;
  $features_cm_cols['lgs'] = array_keys($comp_map_lgs);
  
  return $features_corres_matrix;
}


/**
 * function: tripal_map_create_cm_genome_JS_params
 *	 Prepare the genome correspondence matrix settings to provide to JS.
 *
 * @param $featuremap_prim_id 
 * @param $analysis_id 
 * @param $select_options
 *   
 * @return - feature array for genome correspondence matrix 
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_cm_genome_JS_params($featuremap_prim_id, $analysis_id, $select_options) {

  $features_corres_matrix = array();
  // rows
  $features_corres_matrix['rows'] = array();
  $features_cm_rows = &$features_corres_matrix['rows'];
  // cols
  $analysis_name = "";
  $genome_scaffolds = array();
  // correspondences
  $features_corres_matrix['correspondences'] = array();
  $features_cm_matrix = &$features_corres_matrix['correspondences'];
  
  // reference map
  $map_org = $select_options['map_org'];
  
  $ref_map_lgs = $map_org[$featuremap_prim_id]['linkage_groups'];
  
  // flip the array, so the keys (the linkage group names) become the values
  // so the custom linkage group sort will order them properly
  $flipped_ref_map_lgs = array_flip($ref_map_lgs);
  tripal_map_customLgSort($flipped_ref_map_lgs);
  // after the sort, reverse the array flip
  $ref_map_lgs = array_flip($flipped_ref_map_lgs);
  
  $features_cm_rows['type'] = 'refmap';
  $features_cm_rows['name'] = tripal_map_get_map_name($featuremap_prim_id);
  $features_cm_rows['feature_id'] = $featuremap_prim_id;
  $features_cm_rows['lgs'] = array_keys($ref_map_lgs);

  // each row in the result set represents a cell in the matrix grid. The number of correspondences for a specific linkage group to chromosome scaffold pairing
  // this does not address the zero entries, where no correspondence exists between a linkage group and any scaffold.
  // the lgs are already sorted, and will be added by name, and the query returns the scaffold names in sorted order.
  $analysis_scaffolds_sql = "SELECT distinct(analysis_srcfeature_name) FROM {tripal_map_gene_sequence_mview} WHERE analysis_id = :analysis_id
     ORDER BY analysis_srcfeature_name ASC";
  $args2 = array();
  $args2[':analysis_id'] = $analysis_id;
  
  // find the number of scaffolds in the analysis, and their names. The analysis and their scaffolds are listed in
  // tripal_map_gene_sequence_mview table. Query this and obtain the alphabetical list from there.
  $analysis_scaffolds_res = chado_query($analysis_scaffolds_sql, $args2);
  $analysis_scaffolds_num = $analysis_scaffolds_res->rowCount();
  foreach ($ref_map_lgs as $ref_map_lg_name => $ref_map_lg_id) {
    // create a zero-fill array for each linkage group entry in the features cm matrix, the length of the number of scaffolds in the analysis.
    $features_cm_matrix[$ref_map_lg_id] = array_fill(0, $analysis_scaffolds_num, 0);
  }
  
  // create a hash index lookup table for the scaffold in the array of sorted scaffolds, then place the corres number at the position of the lg array.
  $analysis_scaffolds = $analysis_scaffolds_res->fetchCol();
  // sort
  tripal_map_customLgSort($analysis_scaffolds);
  // copy the array values to a fresh array with indices ordered from 0
  $fresh_array = array_values($analysis_scaffolds);
  // then array flip the order, i.e. keys from array become values and values from array become keys.
  $hash_scaffolds = array_flip($fresh_array);
  $genome_scaffolds = array_keys($hash_scaffolds);
  
  // obtain the number of correspondence markers between reference map id, and analysis id, "num_genetic_marker_corres"
  $map_and_analysis_corres_sql = "SELECT analysis_srcfeature_name, analysis_name, COUNT(*) as num_genetic_marker_corres, linkage_group_id
      FROM {tripal_map_genome_correspondences_mview} 
      WHERE map_id = :featuremap_prim_id AND 
        analysis_id = :analysis_id 
      GROUP BY analysis_srcfeature_name, analysis_name, linkage_group_id
      ORDER BY analysis_srcfeature_name ASC
    ";

  $args = array();
  $args[':featuremap_prim_id'] = $featuremap_prim_id;
  $args[':analysis_id'] = $analysis_id;
  
  $map_and_analysis_corres_res = chado_query($map_and_analysis_corres_sql, $args);
  
  while ($map_and_analysis_corres_res && ($map_and_analysis_corres_obj = $map_and_analysis_corres_res->fetchObject())) {
    // iterate through all the records where correspondences exists between a linkage group and a scaffold, indexed by linkage group.
    if ( is_object($map_and_analysis_corres_obj)) {
      if (property_exists($map_and_analysis_corres_obj, 'analysis_name')) {
        $analysis_name = $map_and_analysis_corres_obj->analysis_name; 
      }
      $lg_id = "";
      if (property_exists($map_and_analysis_corres_obj, 'linkage_group_id')) {
        $lg_id = $map_and_analysis_corres_obj->linkage_group_id;
      }
      if (array_key_exists($lg_id, $features_cm_matrix)) {
        // lookup the position in the linkage group corres matrix array that the scaffold number of correspondences will populate
        if (property_exists($map_and_analysis_corres_obj, 'analysis_srcfeature_name')) {
          $scaffold_name = $map_and_analysis_corres_obj->analysis_srcfeature_name;
          $scaffold_index = 0;
          if (array_key_exists($scaffold_name, $hash_scaffolds)) {
            $scaffold_index = $hash_scaffolds[$scaffold_name];
          }
          if (property_exists($map_and_analysis_corres_obj, 'num_genetic_marker_corres')) {
            $features_cm_matrix[$lg_id][$scaffold_index] = $map_and_analysis_corres_obj->num_genetic_marker_corres;
          }
        }
      }
    }
  }
  $features_cm_matrix = array_values($features_cm_matrix);  
  
  // comparison analysis genome
  $features_corres_matrix['cols'] = array();
  $features_cm_cols = &$features_corres_matrix['cols'];
  $features_cm_cols['type'] = 'compgenome';
  $features_cm_cols['name'] = $analysis_name;
  $features_cm_cols['feature_id'] = $analysis_id;
  $features_cm_cols['lgs'] = $genome_scaffolds;
  $features_cm_cols['analysis_link'] = tripal_map_get_analysis_entity_page_path($analysis_id);
  
  return $features_corres_matrix;
}



/**
 * function: tripal_map_form_cm_organism_bar_init
 *	 Initialize the reference toolbar, using trigger values if available.
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_cm_prim_id
 * @param $featuremap_cm_sec_id
 *
 * @return - The form array for the Correspondence Matrix form.
 *
 * @ingroup tripal_map_includes
 */
// assigns the $features array based on the featuremap id
function tripal_map_form_cm_organism_bar_init( &$form, &$form_state, $select_options, &$featuremap_cm_prim_id, &$featuremap_cm_sec_id) {
 
  if (array_key_exists('storage', $form_state)) {
    // the form was updated, but the url is not up to date yet, obtain previous value from form state storage
    // use the linkage group and map values from the form state
    if (array_key_exists('featuremap_cm_prim_id', $form_state['storage'])) {
      $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
    }
    if (array_key_exists('featuremap_cm_sec_id', $form_state['storage'])) {
      $featuremap_cm_sec_id = $form_state['storage']['featuremap_cm_sec_id'];
    }
  }

  tripal_map_init_cm_organism_selectors_from_storage($form_state, $select_options, $featuremap_cm_prim_id, $featuremap_cm_sec_id);
  
  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];
  $options_chr = $select_options['options_chr'];
  $fs_storage =& $form_state['storage'];

  // primary
  $selector_toolbar = "correspondence_matrix_primary";
  
  $fs_selector_names = array("orgkey" => "cm_prim_org_key", "maps" => "cm_prim_maps",
    "mapskey" => "cm_prim_maps_key", "chr" => "", "chrkey" => "");
  $form_trigger_selectors = array(  "select_organism" => "select_organism_primary_cm",
    "select_map" => "select_map_primary_cm", "select_chromosome" =>"");
  
  tripal_map_trigger_update_storage_from_selectors($selector_toolbar, $fs_selector_names,
    $form_trigger_selectors, $select_options, $form_state);
  
  $fs_cm_prim_org_key = $fs_storage['cm_prim_org_key'];
  
  // The 'All' option will be added during cm init and not at options_map source origin as it is not a map and instead refers to all maps
  $cm_prim_maps_plus_all = array("0" => "All") + $options_map[$fs_cm_prim_org_key];
  tripal_map_update_storage($form_state, 'cm_prim_maps', $cm_prim_maps_plus_all, TRUE);
  
  // secondary
  $selector_toolbar = "correspondence_matrix_secondary";
  $fs_cm_sec_org_key = $fs_storage['cm_sec_org_key'];
  tripal_map_update_storage($form_state, 'cm_sec_maps', $options_map[$fs_cm_sec_org_key], FALSE);
  
  $fs_selector_names = array("orgkey" => "cm_prim_sec_key", "maps" => "cm_sec_maps",
    "mapskey" => "cm_sec_maps_key", "chr" => "", "chrkey" => "");
  $form_trigger_selectors = array(  "select_organism" => "select_organism_secondary_cm",
    "select_map" => "select_map_secondary_cm", "select_chromosome" =>"");
  tripal_map_trigger_update_storage_from_selectors($selector_toolbar, $fs_selector_names,
    $form_trigger_selectors, $select_options, $form_state);

  tripal_map_update_storage($form_state, 'featuremap_cm_prim_id', $fs_storage['cm_prim_maps_key'], TRUE);
  tripal_map_update_storage($form_state, 'featuremap_cm_sec_id', $fs_storage['cm_sec_maps_key'], TRUE);
  
  $featuremap_cm_prim_id = $form_state['storage']['featuremap_cm_prim_id'];
  $featuremap_cm_sec_id = $form_state['storage']['featuremap_cm_sec_id'];
  
  return $form;

}


/**
 *
 * function: tripal_map_init_cm_organism_selectors_from_storage
 *	 Initialize form state storage variables for selectors
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options  
 * @param $featuremap_prim_id 
 * @param $featuremap_sec_id
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_init_cm_organism_selectors_from_storage(&$form_state, $select_options,  $featuremap_prim_id, $featuremap_sec_id) {

  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];

  $fs_storage =& $form_state['storage'];
  if (!(array_key_exists('storage', $form_state))) {
    $fs_storage = array();
  }
  if (!is_array($fs_storage)) {
    // storage is initialized by Drupal to null, set it to type array
    $fs_storage = array();
  }

  //cache the select options arrays for org, map 
  tripal_map_update_storage($form_state, 'select_options_ref', $select_options, FALSE);
  tripal_map_update_storage($form_state, 'organisms_all', $options_org, FALSE);
  if (!(array_key_exists('maps_all', $fs_storage))) {
    tripal_map_update_storage($form_state, 'maps_all', $options_map, FALSE);
    // the map storage is not created yet, so set the flag to update these values in JS and use on redraw
    if (array_key_exists('flag_update_js_maps', $form_state['storage'])) {
      $form_state['storage']['flag_update_js_maps'] = TRUE;
    }
  }
  
  tripal_map_set_select_options_keys_in_storage($form_state, $select_options,
    $featuremap_prim_id, 'cm_prim_org_key', 'cm_prim_maps_key');
  tripal_map_set_select_options_keys_in_storage($form_state, $select_options,
    $featuremap_sec_id, 'cm_sec_org_key', 'cm_sec_maps_key');

}

/**
 *
 * function: tripal_map_set_select_options_keys_in_storage
 *	 Initialize form state storage variables for selectors
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * @param $featuremap_id
 * @param $fs_org_key_name 
 * @param $fs_map_key_name
 *   
 * @ingroup tripal_map_includes
 */
function tripal_map_set_select_options_keys_in_storage(&$form_state, $select_options, $featuremap_id,
  $fs_org_key_name, $fs_map_key_name) {

  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];

  $fs_storage =& $form_state['storage'];

  // set the keys for the select options arrays for org, map
  if (!(array_key_exists($fs_org_key_name, $fs_storage))) {
    // take the first organism as default
    tripal_map_update_storage($form_state, $fs_org_key_name, key($options_org), FALSE);
    if ((isset($featuremap_id) && strlen($featuremap_id) > 0) && ($featuremap_id != '0')) {
      // if featuremap id is available, and it is not the 'All' option. Lookup the associated organism and use that as default
      tripal_map_update_storage($form_state, $fs_org_key_name, tripal_map_get_organism_id($map_org, $featuremap_id), TRUE);
    }
  }
  
  if (!(array_key_exists($fs_map_key_name, $fs_storage))) {
    // use the first map as default
    tripal_map_update_storage($form_state, $fs_map_key_name, key($options_map[$fs_storage[$fs_org_key_name]]), FALSE);
    if (isset($featuremap_id) && strlen($featuremap_id)) {
      // set the default map to the id passed by featuremap_id
      tripal_map_update_storage($form_state, $fs_map_key_name, $featuremap_id, TRUE);
    }
  }
  
}


/**
 * function: tripal_map_form_correspondence_matrix_bar
 *	 Create the reference toolbar with selectors for organism, map and chromosome
 * 
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options
 * 
 * @return -The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_correspondence_matrix_bar(&$form, &$form_state, $select_options) {

  $map_org = $select_options['map_org'];
  $options_org = $select_options['options_org'];
  $options_map = $select_options['options_map'];

  $fs_storage =& $form_state['storage'];
  $cm_prim_org_key = $fs_storage['cm_prim_org_key'];
  $cm_prim_maps_key = $fs_storage['cm_prim_maps_key'];
  $cm_sec_org_key = $fs_storage['cm_sec_org_key'];
  $cm_sec_maps_key = $fs_storage['cm_sec_maps_key'];
  
  $form['correspondence_matrix'] = array(
    '#type' => 'container',
  	'#prefix' => '<div id="select_correspondence_matrix" class="TripalMap">',
  	'#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['correspondence_matrix']['organism_primary'] = array(
    '#type' => 'container',
  	'#prefix' => '<div id="select_organism_primary_cm" class="TripalMap">',
  	'#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
   
  $form['correspondence_matrix']['organism_primary']['select_organism_primary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
  	'#prefix' => '<div id="select_organism_primary_cm" class="form-item">',
  	'#suffix' => '</div>',
  	'#ajax' => array(
      'wrapper' => 'show-org-correspondence_matrix-form',
      'callback' => 'tripal_map_show_org_map_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
      '#options' => $options_org,
      '#multiple' => FALSE,
      '#default_value' => $cm_prim_org_key,
  );
  
  unset($form_state['input']['select_map_primary_cm']);
  $form['correspondence_matrix']['organism_primary']['select_map_primary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Map'),
    '#prefix' => '<div id="select_map_primary_cm" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-org-correspondence_matrix-form',
      'callback' => 'tripal_map_show_org_map_form_ajax_callback',
      'progress' => array('type' => 'throbber'),
      'method' => 'replace',
    ),
    '#options' => $fs_storage['cm_prim_maps'],
    '#multiple' => FALSE,
    '#default_value' => strval($cm_prim_maps_key),
    '#needs_validation' => FALSE,
  );

  $form['correspondence_matrix']['organism_secondary'] = array(
    '#type' => 'container',
  	'#prefix' => '<div id="select_organism_secondary_cm" class="TripalMap"><label class="reference">Compare To</label>',
  	'#suffix' => '</div>',
  	'#collapsible' => FALSE,
  );

  $form['correspondence_matrix']['organism_secondary']['select_organism_secondary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Species'),
  	'#prefix' => '<div id="select_organism_secondary_cm" class="form-item">',
  	'#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-org-correspondence_matrix-form',
      'callback' => 'tripal_map_show_org_map_form_ajax_callback',
      'method' => 'replace',
  	  'progress' => array('type' => 'throbber'),
  	),
    '#options' => $options_org,
    '#multiple' => FALSE,
    '#default_value' => $cm_sec_org_key,
  );
  
  $form['correspondence_matrix']['organism_secondary']['select_map_secondary_cm'] = array(
    '#type' => 'select',
    '#title' => t('Map'),
    '#prefix' => '<div id="select_map_secondary_cm" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-org-correspondence_matrix-form',
      'callback' => 'tripal_map_show_org_map_form_ajax_callback',
      'progress' => array('type' => 'throbber'),
  	  'method' => 'replace',
     ),
     '#options' => $fs_storage['cm_sec_maps'],
     '#multiple' => FALSE,
     '#default_value' => $cm_sec_maps_key,
     '#needs_validation' => FALSE,
  );
  
  $form['correspondence_matrix']['organism_secondary']['button_cm-submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
    '#name' => ('Submit_CM'),
    '#attributes' => array('id' => 'cm-submit'),
    '#prefix' => '<div class="cm-submit-button">',
    '#suffix' => '</div>',
    '#submit' => array('tripal_map_submit_cm_rebuild_form_state'),
    '#after_build' => array('tripal_map_load_correspondence_matrix_js'),
  );

  $form['#prefix'] = '<div id="show-org-correspondence_matrix-form">';
  $form['#suffix'] = '</div>';

  return $form;

}

/**
 * function: tripal_map_load_correspondence_matrix_js
 *	 Initialize the JS files and pass the module path settings to JS
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * 
 * @ingroup tripal_map_includes
 */
function tripal_map_load_correspondence_matrix_js($form, &$form_state) {
    
  drupal_add_js(drupal_get_path('module', 'tripal_map') . '/theme/js/tripal_map_utils.js');
  drupal_add_js(drupal_get_path('module', 'tripal_map') . '/theme/js/draw_correspondence_matrix.js'); 
  drupal_add_js(drupal_get_path('module', 'tripal_map') . '/theme/js/tripal_correspondence_matrix.js');
  drupal_add_js(array( 'tripal_map' => array('modulePath' => drupal_get_path( 'module', 'tripal_map' )) ),'setting');
  
  return $form;
}


/**
 * function: tripal_map_submit_cm_rebuild_form_state
 *	 Rebuild the form state, create features from newly selected linkage groups and pass them to JS
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_submit_cm_rebuild_form_state($form, &$form_state) {

  $form_state['rebuild'] = TRUE;
  tripal_map_update_storage($form_state, 'flag_update_js_maps', TRUE, TRUE);

}

