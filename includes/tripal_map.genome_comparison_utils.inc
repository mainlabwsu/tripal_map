<?php

/**
 * @file
 *	 Create genome comparison toolbar for selecting analysis and scaffold for the sequence view display in MapViewer forms.
 *
 * @ingroup tripal_map
 */


/**
 * 
 * function: tripal_map_init_genome_comparison_selectors_to_storage
 *	 Initialize form state storage variables for comparison selectors
 *
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options_genome_corres 
 * @param $analysis_id 
 * @param $scaffold_id
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_init_genome_comparison_selectors_to_storage(&$form_state, $select_options_genome_corres, $analysis_id, $scaffold_id) {

  $analysis_comp = $select_options_genome_corres['options_analysis'];
  $scaffold_comp = $select_options_genome_corres['options_scaffold'];
  
  $fs_storage =& $form_state['storage'];
  if (!(array_key_exists('storage', $form_state))) {
    // the storage key does not exist in form state
    $fs_storage = array();
  }

  if (!is_array($fs_storage)) {
    // the form state is initialized to null, by Drupal. Assign it to type array.
    $fs_storage = array();
  }
  
  $submit = FALSE;
  if ((tripal_map_get_form_trigger($form_state) == 'Submit_Comparison') || 
      (tripal_map_get_form_trigger($form_state) == 'Submit_Genome_Comparison') ||
      (tripal_map_get_form_trigger($form_state) == 'Submit_Reference')) {
    $submit = TRUE;
  }

  // by default, do not show genome comparison or invert scaffold. set to false if flag does not exist.
  tripal_map_update_storage($form_state, 'show_genome_comp', FALSE, FALSE);
  tripal_map_update_storage($form_state, 'invert_genome_scaffold', FALSE, FALSE);
  
  if (!$submit) {
    if (is_array($analysis_comp)) {
      // take the first analysis as default and write it to storage
      tripal_map_update_storage($form_state, 'analysis_compkey', array_key_first($analysis_comp), TRUE);
    }
    if (isset($analysis_id) && strlen($analysis_id)) {
        // analysis_id value is previously set in form state storage, defer to analysis_id value passed in by the caller 
        tripal_map_update_storage($form_state, 'analysis_compkey', $analysis_id, TRUE);  
    }
  }

  // use the first scaffold as default
  if (!$submit) {
    // the analysis_comp array might be empty ie. [0,"None"]. check for this and assign scaffold key accordingly
    if (array_key_exists('analysis_compkey', $form_state['storage']) && 
      ($analysis_comp[$fs_storage['analysis_compkey']] == "None")) {
      tripal_map_update_storage($form_state, 'scaffold_compkey', "0", TRUE);
    }
    else {
      // take the first scaffold as default and write to storage 
      if ($scaffold_comp && array_key_exists('analysis_compkey', $form_state['storage'])) {
          tripal_map_update_storage($form_state, 'scaffold_compkey', array_key_first($scaffold_comp[$fs_storage['analysis_compkey']]), TRUE);
      }
      if ((isset($analysis_id) && strlen($analysis_id)) &&
        (isset($scaffold_id) && strlen($scaffold_id))) {
        // unless the scaffold_id value is previously set in form state storage, then defer to analysis_id value provided by caller  
        tripal_map_update_storage($form_state, 'scaffold_compkey', $scaffold_id, TRUE);
      }
    }
  }
    
}


/**
 * function: tripal_map_form_genome_comparison_bar_init
 *	 Initialize data structures for the genome comparison toolbar
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options_genome_corres
 * @param $linkage_group_id
 * @param $analysis_id
 * @param $scaffold_name 
 * @param $features_genome 
 * @param $show_genome_checked 
 * @param $show_genome_enabled 
 *
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
*/
function tripal_map_form_genome_comparison_bar_init( &$form, &$form_state, $select_options_genome_corres, $linkage_group_id,
    &$analysis_id, &$scaffold_name, &$features_genome, $show_genome_checked, $show_genome_enabled ) {

  $fs_storage =& $form_state['storage'];
  
  $analysis_comp = $select_options_genome_corres['options_analysis'];
  $scaffold_comp = $select_options_genome_corres['options_scaffold'];

  // check if there was a trigger in the reference toolbar, as this will reflect in update of the genome comparison bar
  $ref_trigger = FALSE; 
  if ((tripal_map_get_form_trigger($form_state) == 'select_organism') || 
    (tripal_map_get_form_trigger($form_state) == 'select_map') ||
    (tripal_map_get_form_trigger($form_state) == 'select_chromosome')) {
    $ref_trigger = TRUE;
  }

  $submit = FALSE;
  if ((tripal_map_get_form_trigger($form_state) == 'Submit_Comparison') && 
    (tripal_map_get_form_trigger($form_state) == 'Submit_Reference') &&
    (tripal_map_get_form_trigger($form_state) == 'Submit_Genome_Comparison')) {
    $submit = TRUE;
  }
  
  // by default pick the first element of each select list
  $analysis_id = "";
  if (!$ref_trigger) {
    if (array_key_exists('analysis_compkey', $form_state['storage'])) {
      $analysis_id = $fs_storage['analysis_compkey'];
    }
  }
  
  $scaffold_id = "";
  if(!$ref_trigger && !$submit) {
    if (array_key_exists('scaffold_compkey', $form_state['storage'])) {
      $scaffold_id = $fs_storage['scaffold_compkey'];
    }
  }
  
  // initialize the values for select lists for analysis and scaffolds, if none are
  // currently set. None are set if form_state storage for these is uninitialized.
  tripal_map_init_genome_comparison_selectors_to_storage($form_state, $select_options_genome_corres, $analysis_id, $scaffold_id);

  $analysis_compkey =& $fs_storage['analysis_compkey'];
  $scaffold_compkey =& $fs_storage['scaffold_compkey'];
  $analysis_comp = $select_options_genome_corres['options_analysis'];
  $scaffold_comp = $select_options_genome_corres['options_scaffold'];
    
  if (!$submit) {
    // if the reference map is updated, then set the genome map to new defaults 
    tripal_map_update_storage($form_state, 'analysis_comp', $analysis_comp, TRUE);
    tripal_map_update_storage($form_state, 'scaffold_comp', $scaffold_comp[$analysis_compkey], TRUE);
  }

  if (tripal_map_get_form_trigger($form_state) == 'select_analysis_comparison') {
    // the organism comparison selection item has changed
    tripal_map_update_storage($form_state, 'analysis_compkey', $form_state['triggering_element']['#value'], TRUE);
    tripal_map_update_storage($form_state, 'scaffold_comp', $scaffold_comp[$analysis_compkey], TRUE);
    tripal_map_update_storage($form_state, 'scaffold_compkey', key($scaffold_comp[$analysis_compkey]), TRUE);
    
  }
  
  if (tripal_map_get_form_trigger($form_state) == 'select_scaffold_comparison') {
    // the map comparison selection item has changed
    tripal_map_update_storage($form_state, 'scaffold_compkey', $form_state['triggering_element']['#value'], TRUE);
  }
  
  tripal_map_update_storage($form_state, 'show_genome_enabled', $show_genome_enabled, TRUE);
  if (tripal_map_get_form_trigger($form_state) == 'show_sequence_view') {
    //save the show comparison setting in form state storage, so it is accessible to JS
    //tripal_map_update_storage($form_state, 'show_genome_comp', $form_state['values']['show_sequence_view'], TRUE);
    tripal_map_update_storage($form_state, 'show_genome_comp', $show_genome_checked, TRUE);
  }

  if (tripal_map_get_form_trigger($form_state) == 'invert_genome_scaffold') {
      //save the show comparison setting in form state storage, so it is accessible to JS
      tripal_map_update_storage($form_state, 'invert_genome_scaffold', $form_state['values']['invert_genome_scaffold'], TRUE);
  }

  $form['analysis_comparison']['select_analysis_comparison']['#default_value'] = $analysis_compkey;
  $form['analysis_comparison']['scaffold_comparison']['select_scaffold_comparison']['#options'] = $fs_storage['scaffold_comp'];
  $form['analysis_comparison']['scaffold_comparison']['select_scaffold_comparison']['#default_value'] = $scaffold_compkey;
  $form['analysis_comparison']['#disabled'] = !$show_genome_checked;
  
  $analysis_id = $analysis_compkey;  
  $scaffold_name = "";
  if (array_key_exists('scaffold_comp', $fs_storage) && array_key_exists($scaffold_compkey, $fs_storage['scaffold_comp'])) {
  	$scaffold_name = $fs_storage['scaffold_comp'][$scaffold_compkey];
    tripal_map_update_storage($form_state, 'scaffold_comparison', $scaffold_name, TRUE);
    $scaffold_id = $scaffold_compkey;
  }

  if ((isset($scaffold_id) && ($scaffold_id != 0)) &&
    (isset($linkage_group_id) && (gettype($linkage_group_id) == 'string') && (strlen($linkage_group_id) != 0))) {
    // populate the features genome object, if scaffold exists for reference linkage group
    $features_genome = tripal_map_create_genes($scaffold_id, $linkage_group_id); 
  }
  
  return $form;
}


/**
 * function: tripal_map_form_genome_table
 *	 Create the downloadable table for marker map correspondences
 *
 * @param $ref_map_name
 * @param $ref_chr_name
 * @param $analysis_name 
 * @param $scaffold_name 
 * @param $ref_chr 
 * @param $ref_map 
 * @param $scaffold_key
 * @param $analysis_key
 *
 * @return - Redirect to CSV table file location
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_genome_table( $ref_map_name, $ref_chr_name, $analysis_name, $scaffold_name, $ref_chr, $ref_map, 
    $scaffold_key, $analysis_key) {
    
  $map_params = array("ref_map_name" => $ref_map_name, "ref_chr_name" => $ref_chr_name, "analysis_name" => $analysis_name, 
    "scaffold_name" => $scaffold_name, "ref_chr" => $ref_chr, "ref_map" => $ref_map, "scaffold_key" => $scaffold_key, "analysis_key" => $analysis_key);

  $table_url = tripal_map_create_marker_genome_corres_table($map_params);
  
  drupal_goto($table_url);
    
}


/**
 * function: tripal_map_form_add_genome_comparison_bar
 *	 Add the genome comparison toolbar to the MapViewer form
 *
 * @param $form - The form to be initialized
 * @param $form_state - The current state of the form. The values and storage that were submitted
 * @param $select_options_genome_comparison 
 * @param $show_genome_checked
 *
 * @return - The form array for the MapViewer form.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_form_add_genome_comparison_bar(&$form, &$form_state, $select_options_genome_comparison, $show_genome_checked) {

  $fs_storage =& $form_state['storage'];
  
  global $base_url;
  
  $options_analysis = tripal_map_get_elem($fs_storage, 'analysis_comp');
  $analysis_key = tripal_map_get_elem($fs_storage, 'analysis_compkey');
  $analysis_name = $options_analysis[$analysis_key];
  
  $options_scaffold = tripal_map_get_elem($fs_storage, 'scaffold_comp');
  $scaffold_key = tripal_map_get_elem($fs_storage, 'scaffold_compkey');
  $scaffold_name = $options_scaffold[$scaffold_key];
  
  $ref_map = tripal_map_get_elem($fs_storage, 'maps_key');
  $ref_map_name = $fs_storage['maps'][$ref_map];
  
  $ref_chr = tripal_map_get_elem($fs_storage, 'chr_key');
  $ref_chr_name = $fs_storage['chr'][$ref_chr];
  
  // Draw the Dot plot icon with genome specific link if genome view enabled
  if ($show_genome_checked) {
    $dotplot_icon = $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/dotplot.png';
    $dotplot_url = $base_url . "/dotplot/" . $ref_map . "/" . $ref_chr . "/" . $scaffold_key;
    $form['dotplot'] = array(
      '#type' => 'container',
      '#prefix' =>'<div id="show_dot_plot" class="TripalMap">
        <label class="dotplot">Dot Plot  <a href= ' . $dotplot_url . '
  	    target="_blank" title="Dot Plot"><img id="dotplot_img" src="' . $dotplot_icon .
        '" align="top"></a></label>',
      '#suffix' => '</div>',
      '#collapsible' => FALSE,
    );
  }

  //  Draw the Correspondence matrix link with genome specific path data if genome view enabled
  if ($show_genome_checked) {
      $correspondence_matrix_url = $base_url . "/correspondence_matrix_genomes/" . $ref_map . "/" . $analysis_key . "/#" . $ref_map;
    $form['main_comparison']['#prefix'] = '<div id="show_main_comparison" class="TripalMap">' .
      '<label class="reference">Select Comparison Map: </label>' .
      '<a id="correspondence_matrix_link" href= ' . $correspondence_matrix_url . ' target="_blank"
       title="Correspondence Matrix">(View Correspondence Matrix)</a></label>';
  }
  
  $form['genome_comparison']['gc_frame'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="show_gc_frame" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['genome_comparison']['gc_frame']['sequence_view'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="show_sequence_view" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
  );
  
  $form['genome_comparison']['gc_frame']['sequence_view']['show_sequence_view'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show genome view'),
    '#default_value' => tripal_map_get_elem($fs_storage, 'show_genome_comp'), 
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#disabled' => !tripal_map_get_elem($fs_storage, 'show_genome_enabled')
      
  );

  $form['genome_comparison']['gc_frame']['sequence_view']['invert_genome_scaffold'] = array(
      '#type' => 'checkbox',
      '#title' => t('Invert scaffold'),
      '#default_value' => tripal_map_get_elem($fs_storage, 'invert_genome_scaffold'),
      '#ajax' => array(
          'wrapper' => 'show-map-chr-form',
          'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
          'method' => 'replace',
          'progress' => array('type' => 'throbber'),
      ),
      '#disabled' => !tripal_map_get_elem($fs_storage, 'show_genome_comp')
      
  );

  // create download correspondence table icon
  $download_table_icon = $base_url . '/' . drupal_get_path('module', 'tripal_map') . '/theme/images/download_table.png';
  
  $map_params = array("ref_map_name" => $ref_map_name, "ref_chr_name" => $ref_chr_name, "analysis_name" => $analysis_name, "scaffold_name" => $scaffold_name, 
      "ref_chr" => $ref_chr, "ref_map" => $ref_map, "scaffold_key" => $scaffold_key, "analysis_key" => $analysis_key);
  
  $download_table_url = $base_url . "/download_genome_table/". rawurlencode(tripal_map_encode_reserved_symbols($ref_map_name)) . "/" 
      . rawurlencode(tripal_map_encode_reserved_symbols($ref_chr_name)) . "/" . rawurlencode(tripal_map_encode_reserved_symbols($analysis_name)) . "/" 
          . rawurlencode(tripal_map_encode_reserved_symbols($scaffold_name)) ."/" . $ref_chr ."/" . $ref_map . "/" . $scaffold_key . "/" . $analysis_key;
  
  $link_to_table = '<a id="download_table_link" href= ' . $download_table_url . '
      target="_blank" title="Download Table"><img id="download_table_img" src="' . $download_table_icon .
      '"></a>Download Table ';

  if (($scaffold_key == 0) || ($analysis_key == 0)) {
      // there is no genome marker correspondence, omit the link to the table
      $link_to_table = '<img id="download_table_img" src="' . $download_table_icon .
      '">Download Table ';
  }

  $form['genome_comparison']['gc_frame']['sequence_view']['download_table'] = array(
      '#type' => 'container',
      '#prefix' => '<div id="download_table" class="TripalMap">' . $link_to_table,
      '#suffix' => '</div>',
      
  );  
  
  $form['genome_comparison']['gc_frame']['analysis_comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="select_analysis_comparison" class="TripalMap">',
    '#suffix' => '</div>',
    '#collapsible' => FALSE,
    '#disabled' => !tripal_map_get_elem($fs_storage, 'show_genome_comp')
  );
  
  $form['genome_comparison']['gc_frame']['analysis_comparison']['select_analysis_comparison'] = array(
    '#type' => 'select',
    '#title' => t('Whole Genome'),
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'method' => 'replace',
      'progress' => array('type' => 'throbber'),
    ),
    '#options' => $options_analysis,
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#default_value' => $analysis_key,
  );
  
  // If #empty_value or #empty_option is set and #required is FALSE (default), an empty option is added to the select control, allowing the user to choose nothing.
  $form['genome_comparison']['gc_frame']['analysis_comparison']['scaffold_comparison'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="scaffold_comparison' . '" class="form-item">',
    '#suffix' => '</div>',
    '#validated' => TRUE,
  );

  $form['genome_comparison']['gc_frame']['analysis_comparison']['scaffold_comparison']['select_scaffold_comparison'] = array(
    '#type' => 'select',
    '#title' => t('Chromosome/Scaffold'),
    '#prefix' => '<div id="select_scaffold_comparison' . '" class="form-item">',
    '#suffix' => '</div>',
    '#ajax' => array(
      'wrapper' => 'show-map-chr-form', 
      'callback' => 'tripal_map_show_map_chr_form_ajax_callback',
      'progress' => array('type' => 'throbber'),
      'method' => 'replace',
    ),
    '#options' => $options_scaffold,
    '#multiple' => FALSE,
    '#required' => FALSE,
    '#default_value' => $scaffold_key,
    '#needs_validation' => FALSE,
  );

  $form['genome_comparison']['gc_frame']['analysis_comparison']['button_scaffold_comparison_mv-submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
    '#name' => ('Submit_Genome_Comparison'),
    '#attributes' => array(
      'id' => 'scaffold_comparison_mv-submit'),
    '#prefix' => '<div class="scaffold_comparison_mv-submit-button">',
    '#suffix' => '</div>',
    '#submit' => array('tripal_map_submit_rebuild_form_state'),
    '#after_build' => array('tripal_map_draw_mapViewer'),
  );
  
  return $form;
}


/**
 * function: tripal_map_create_marker_genome_corres_table
 *	 Create the downloadable table for the genetic marker correspondences between
 *   the reference genetic map and comparison genome
 *
 * @param $map_params - The name and identifiers for the map and genome 
 *
 * @return - The download table file name string.
 *
 * @ingroup tripal_map_includes
 */
function tripal_map_create_marker_genome_corres_table($map_params) {
  
  // create the path
  $dir = 'sites/default/files/tripal/tripal_mapviewer';
  if (!file_exists($dir)) {
    $res = mkdir($dir, 0777);
  }
  else {
    // remove all files in this directory, to prevent a build up of cached table files
    if (file_exists($dir)) {
      $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
      $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
      foreach ( $ri as $file ) {
        $file->isDir() ?  rmdir($file) : unlink($file);
      }
    }
  }
  
  $ref_map_name = tripal_map_encode_special_char_filename($map_params["ref_map_name"]);
  $ref_chr_name = tripal_map_encode_special_char_filename($map_params["ref_chr_name"]);
  $ref_chr_name = tripal_map_lg_name_reduction($ref_chr_name, $ref_map_name);
  $analysis_name = tripal_map_encode_special_char_filename($map_params["analysis_name"]);
  $scaffold_name = tripal_map_encode_special_char_filename($map_params["scaffold_name"]);
  $scaffold_name = tripal_map_lg_name_reduction($scaffold_name, $analysis_name);
  
  $file_name = "Genome_Marker_Correspondences_" . $ref_map_name . "_" . $ref_chr_name .
  "_vs_" .  $analysis_name . "_" . $scaffold_name;

  $file_name = tripal_map_decode_reserved_symbols($file_name);
  $file_name_char_limit = 251; // 255 linux file name character limit, substracting the extension
  if (strlen($file_name) > $file_name_char_limit) {
    // truncate the file name, removing the end to fit the character limit
    $file_name = substr($file_name, 0, $file_name_char_limit);
  }
  
  $download_table_file = $dir . "/" . $file_name . ".csv";
    
  // open the file with the generated file name
  $handle = fopen($download_table_file, "w");
  fwrite($handle, "Correspondence between markers in the genetic map and genome " . "\n" . 
    "(some markers are aligned to genome using sequences around " . "\n" . 
    "the markers so SNPs can span longer than 1bp in the genome)" . "\n");

  // provide the column names
  fwrite($handle, 'Map Name,Linkage Group,Marker Locus,Marker Type,Genetic Position (cM),' .
    'Genome Name,Chromosome,Marker Name,Genome Start (bp),Genome Stop (bp)' ."\n");
    
  // create the query to obtain the marker correspondence data
  $sql = "SELECT G.map_name, G.map_id, G.linkage_group, G.linkage_group_id,  G.marker_locus_name, G.feature_id as genetic_marker_id, G.marker_type, G.marker_pos, G.marker_pos_type,
  GSV.analysis_name, GSV.analysis_id, GSV.analysis_srcfeature_name, GSV.analysis_srcfeature_id, GSV.analysis_feature_name, GSV.analysis_feature_type, GSV.analysis_feature_min,
  GSV.analysis_feature_max, GSV.analysis_feature_strand
  FROM (
      SELECT GMV.map_name, GMV.map_id, GMV.linkage_group, GMV.linkage_group_id, GMV.marker_locus_name,  GMV.genetic_marker_id as feature_id,
      GMV.marker_type, GMV.marker_pos, GMV.marker_pos_type
      FROM {tripal_map_genetic_markers_mview} GMV
      WHERE (( GMV.linkage_group_id = :linkage_group_id) AND (GMV.map_id = :map_id))
      UNION
      SELECT QMV.map_name, QMV.map_id, QMV.linkage_group, QMV.linkage_group_id,  QMV.marker_locus_name, QMV.feature_id,
      QMV.marker_type, QMV.marker_pos, QMV.marker_pos_type
      FROM {tripal_map_qtl_and_mtl_mview} QMV
      WHERE (( QMV.linkage_group_id = :linkage_group_id) AND (QMV.map_id = :map_id))
      ) G
      INNER JOIN {tripal_map_gene_sequence_mview} GSV ON GSV.analysis_feature_id = G.feature_id
      AND ((GSV.analysis_srcfeature_id= :scaffold_id) AND (GSV.analysis_id = :analysis_id))";
    
  $args = array();
  $args[':linkage_group_id'] =  $map_params["ref_chr"];
  $args[':map_id'] =            $map_params["ref_map"];
  $args[':scaffold_id'] =       $map_params["scaffold_key"];
  $args[':analysis_id'] =       $map_params["analysis_key"];

  $results = chado_query($sql, $args);
  $numrec = $results->rowCount();
  while ($results && ($results_obj = $results->fetchObject())) {
    if (is_object($results_obj)) {
      $marker_str = "";
      $array = array('map_name', 'linkage_group', 'marker_locus_name', 'marker_type', 'marker_pos',
      'analysis_name', 'analysis_srcfeature_name', 'analysis_feature_name', 'analysis_feature_min', 'analysis_feature_max');
      foreach ($array as $elem) {
        if (property_exists($results_obj, $elem)) {
          $marker_str .= '"' . $results_obj->$elem . '",';
        }
      }
      fwrite($handle, $marker_str . "\n");
    }
  }
  fclose($handle);
  
  return $download_table_file;

}


/**
 * function: tripal_map_update_drupal_add_js_show_comparison_params
 *	 Pass the show comparison settings to JS
 *
 * @param $show_genome_comparison
 * @param $invert_genome_scaffold
 * 
 * @ingroup tripal_map_includes
 */
function tripal_map_update_drupal_add_js_show_genome_comparison_params($show_genome_comparison, $invert_genome_scaffold) {

  // pass flag for whether to display comparison linkage group or not
  $js_setting = "mapViewerDisplayGenomeJS";
  drupal_add_js(array($js_setting => array('show_genome_comparison' => $show_genome_comparison)), 'setting');
  drupal_add_js(array($js_setting => array('invert_genome_scaffold' => $invert_genome_scaffold)), 'setting');
  
}

